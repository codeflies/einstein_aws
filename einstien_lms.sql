-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: einstien_lms
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `educator_id` int DEFAULT NULL,
  `action_code` varchar(50) DEFAULT NULL,
  `action_desc` varchar(50) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `email_verified_at` varchar(50) NOT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','admin@gmail.com','','81dc9bdb52d04dc20036dbd8313ed055','2021-07-23 07:25:35','2021-07-23 07:25:35'),(2,'test','test@gmail.com','','cc03e747a6afbbcbf8be7668acfebee5','2021-07-23 10:05:24','2021-07-23 10:05:24');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branch` (
  `id` int NOT NULL AUTO_INCREMENT,
  `educator_id` int NOT NULL,
  `name` varchar(30) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `default_branch_theme` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `language` varchar(150) DEFAULT NULL,
  `timezone` int DEFAULT NULL,
  `internal` longtext,
  `external` longtext,
  `default_user_type` varchar(50) DEFAULT NULL,
  `sigup` varchar(50) DEFAULT NULL,
  `specific_domain` varchar(50) DEFAULT NULL,
  `register_total_user` int DEFAULT NULL,
  `disallow` enum('Y','N') DEFAULT NULL,
  `domain_url` varchar(50) DEFAULT NULL,
  `terms_of_services` enum('Y','N') DEFAULT NULL,
  `e_commerce_processor` varchar(50) DEFAULT NULL,
  `badget_set` varchar(50) DEFAULT NULL,
  `paypal_email_address` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `gamification` varchar(255) NOT NULL,
  `count_of` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,3,'test','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',NULL,'2021-08-11 10:41:51','2021-08-11 10:41:51'),(2,3,'test1','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',1,'2021-08-11 10:41:56','2021-08-11 10:41:56'),(3,3,'test2','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',1,'2021-08-11 10:42:03','2021-08-11 10:42:03'),(4,3,'test11','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',2,'2021-08-11 10:42:05','2021-08-11 10:42:05'),(5,3,'test21','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',3,'2021-08-11 10:42:08','2021-08-11 10:42:08'),(6,3,'test3','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',1,'2021-08-11 11:54:04','2021-08-11 11:54:04'),(7,3,'test12','','','Default',NULL,NULL,'english',1,'','','Learn Type',NULL,NULL,0,NULL,NULL,NULL,'Paypal',NULL,'','US Dollar','Old School',2,'2021-08-11 11:55:33','2021-08-11 11:55:33');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `educator_id` int NOT NULL,
  `course_name` varchar(100) NOT NULL,
  `category` int DEFAULT NULL,
  `description` longtext NOT NULL,
  `image` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `course_code` varchar(40) NOT NULL,
  `price` int NOT NULL,
  `Capacity` varchar(100) NOT NULL,
  `You_Tube` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Use_Video` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `course_expiration` varchar(50) NOT NULL,
  `certification` int DEFAULT NULL,
  `level` int DEFAULT NULL,
  `expiration_date` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,3,'test23',2,'test','1_course.png','w232323',23,'8',NULL,NULL,'4',NULL,NULL,'08/25/2021 5:10 PM','2021-08-11 11:35:03','2021-08-11 11:35:03'),(2,3,'testing',1,'test','2_course.png','sqsssq',22,'sfsff',NULL,NULL,'',NULL,NULL,'','2021-08-11 11:45:19','2021-08-11 11:45:19'),(3,3,'tarun-Course',NULL,'test  for course','3_course.png','text12345',89,'text',NULL,NULL,'test',NULL,NULL,'08/13/2021 4:20 PM','2021-08-12 07:20:36','2021-08-12 07:20:36');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_category`
--

DROP TABLE IF EXISTS `course_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent_categories` int NOT NULL,
  `price` float NOT NULL,
  `educator_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_category`
--

LOCK TABLES `course_category` WRITE;
/*!40000 ALTER TABLE `course_category` DISABLE KEYS */;
INSERT INTO `course_category` VALUES (2,'dcdvfvvgb',0,111,3,'2021-08-11 12:07:20','2021-08-11 12:07:20'),(3,'TESTING',0,33,3,'2021-08-12 12:39:55','2021-08-12 12:39:55'),(4,'test',0,22,3,'2021-08-13 11:07:29','2021-08-13 11:07:29'),(5,'1test',0,22,3,'2021-08-13 11:28:22','2021-08-13 11:28:22'),(6,'test',0,22,3,'2021-08-13 11:30:00','2021-08-13 11:30:00'),(7,'testing',0,222,3,'2021-08-16 04:49:31','2021-08-16 04:49:31');
/*!40000 ALTER TABLE `course_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educator`
--

DROP TABLE IF EXISTS `educator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `educator` (
  `id` int NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(30) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `department` varchar(30) DEFAULT NULL,
  `what_company_do` varchar(150) DEFAULT NULL,
  `no_of_people` int DEFAULT NULL,
  `goal` varchar(150) DEFAULT NULL,
  `welcome_info` varchar(10) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educator`
--

LOCK TABLES `educator` WRITE;
/*!40000 ALTER TABLE `educator` DISABLE KEYS */;
INSERT INTO `educator` VALUES (1,'testdomain',2,'Consultant','Computer Software',100,'Training Employees,Training Partners,Providing Training Services','false'),(2,'mydomain',4,'Accounting','Agriculture',1,'Training Employees,Training Partners,Providing Training Services','true'),(3,'testmail',6,'Accounting','Agriculture',1,'Training Employees,Training Partners,Providing Training Services','true'),(4,'codebuddy',7,'Accounting','Agriculture',1,'Training Employees,Training Partners,Providing Training Services','true');
/*!40000 ALTER TABLE `educator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `price` float NOT NULL,
  `group_key` varchar(100) NOT NULL,
  `educator_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_type`
--

DROP TABLE IF EXISTS `module_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `module_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `is_active` enum('Y','N') NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_type`
--

LOCK TABLES `module_type` WRITE;
/*!40000 ALTER TABLE `module_type` DISABLE KEYS */;
INSERT INTO `module_type` VALUES (1,'Admin','Y','2021-07-27 09:02:43','2021-07-27 09:02:43'),(2,'Instructor','Y','2021-07-27 09:03:02','2021-07-27 09:03:02'),(3,'Learner','Y','2021-07-27 09:03:20','2021-07-27 09:03:20'),(4,'General','Y','2021-07-27 09:03:47','2021-07-27 09:03:47');
/*!40000 ALTER TABLE `module_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `my_view`
--

DROP TABLE IF EXISTS `my_view`;
/*!50001 DROP VIEW IF EXISTS `my_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `my_view` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `is_active`,
 1 AS `created_at`,
 1 AS `updated_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `perm_categories`
--

DROP TABLE IF EXISTS `perm_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perm_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_category` int DEFAULT '0',
  `module_id` int NOT NULL,
  `is_active` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perm_categories`
--

LOCK TABLES `perm_categories` WRITE;
/*!40000 ALTER TABLE `perm_categories` DISABLE KEYS */;
INSERT INTO `perm_categories` VALUES (1,'Users',NULL,NULL,NULL,0,'Y'),(2,'Courses',NULL,NULL,NULL,0,'Y'),(3,'Categories',NULL,NULL,NULL,0,'Y'),(4,'Branches',NULL,NULL,NULL,0,'Y'),(5,'Events engine',NULL,NULL,NULL,0,'Y'),(6,'Import - Export',NULL,NULL,NULL,0,'Y'),(7,'User types',NULL,NULL,NULL,0,'Y'),(8,'Account & Settings',NULL,NULL,NULL,0,'Y'),(9,'Reports',NULL,NULL,NULL,0,'Y'),(10,'Files',NULL,NULL,NULL,0,'Y');
/*!40000 ALTER TABLE `perm_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_type_id` int DEFAULT NULL,
  `module_type_id` int NOT NULL,
  `action_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `module_type_key` (`module_type_id`),
  KEY `action_key` (`action_id`),
  CONSTRAINT `module_type_key` FOREIGN KEY (`module_type_id`) REFERENCES `module_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_action`
--

DROP TABLE IF EXISTS `permission_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_action` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `action_code` varchar(100) DEFAULT NULL,
  `action_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_key` (`category_id`),
  CONSTRAINT `category_key` FOREIGN KEY (`category_id`) REFERENCES `permission_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_action`
--

LOCK TABLES `permission_action` WRITE;
/*!40000 ALTER TABLE `permission_action` DISABLE KEYS */;
INSERT INTO `permission_action` VALUES (1,1,'user_view','View','2021-07-28 06:21:34','2021-07-28 06:21:34'),(2,1,'user_create','Create','2021-07-28 06:22:06','2021-07-28 06:22:06'),(3,1,'user_update','Update','2021-07-28 06:22:25','2021-07-28 06:22:25'),(4,1,'user_delete','Delete','2021-07-28 06:22:41','2021-07-28 06:22:41'),(5,2,'courses_view','View','2021-07-28 06:25:21','2021-07-28 06:25:21'),(6,2,'courses_create','Create','2021-07-28 06:27:34','2021-07-28 06:27:34'),(7,2,'courses_update','Update','2021-07-28 06:27:55','2021-07-28 06:27:55'),(8,2,'courses_delete','Delete','2021-07-28 06:28:32','2021-07-28 06:28:32'),(9,3,'groups_view','View','2021-07-28 06:31:13','2021-07-28 06:31:13'),(10,3,'groups_create','Create','2021-07-28 06:44:09','2021-07-28 06:44:09'),(11,3,'groups_update','Update','2021-07-28 06:45:04','2021-07-28 06:45:04'),(12,3,'groups_delete','Delete','2021-07-28 06:45:36','2021-07-28 06:45:36'),(13,4,'categories_view','View','2021-07-28 06:46:30','2021-07-28 06:46:30'),(14,4,'categories_create','Create','2021-07-28 06:46:54','2021-07-28 06:46:54'),(15,4,'categories_update','Update','2021-07-28 06:47:15','2021-07-28 06:47:15'),(16,4,'categories_delete','Delete','2021-07-28 06:47:32','2021-07-28 06:47:32'),(17,5,'branches_view','View','2021-07-28 06:48:34','2021-07-28 06:48:34'),(18,5,'branches_create','Create','2021-07-28 06:48:53','2021-07-28 06:48:53'),(19,5,'branches_update','Update','2021-07-28 06:49:13','2021-07-28 06:49:13'),(20,5,'branches_delete','Delete','2021-07-28 06:49:28','2021-07-28 06:49:28'),(21,12,'notifications_view','View','2021-07-28 06:55:10','2021-07-28 06:55:10'),(22,12,'notification_create','Create','2021-07-28 06:55:30','2021-07-28 06:55:30'),(23,12,'notification_update','Update','2021-07-28 06:55:51','2021-07-28 06:55:51'),(24,12,'notification_delete','Delete','2021-07-28 06:56:16','2021-07-28 06:56:16'),(25,13,'automations_view','View','2021-07-28 06:56:51','2021-07-28 06:56:51'),(26,13,'automations_create','Create','2021-07-28 06:57:47','2021-07-28 06:57:47'),(27,13,'automations_update','Update','2021-07-28 07:01:42','2021-07-28 07:01:42'),(28,13,'automations_delete','Delete','2021-07-28 07:02:48','2021-07-28 07:02:48'),(29,7,'import&export_import','Import','2021-07-28 07:03:42','2021-07-28 07:03:42'),(30,7,'import&export_export','Export','2021-07-28 07:03:55','2021-07-28 07:03:55'),(31,8,'usertyes_view','View','2021-07-28 07:04:39','2021-07-28 07:04:39'),(32,8,'usertyes_create','Create','2021-07-28 07:04:55','2021-07-28 07:04:55'),(33,8,'usertyes_update','Update','2021-07-28 07:05:21','2021-07-28 07:05:21'),(34,8,'usertyes_delete','Delete','2021-07-28 07:05:36','2021-07-28 07:05:36'),(35,9,'account&setting_view','View','2021-07-28 07:06:59','2021-07-28 07:06:59'),(36,9,'account&setting_update','Update','2021-07-28 07:07:28','2021-07-28 07:07:28'),(37,9,'account&setting_gamification','Gamification','2021-07-28 07:07:50','2021-07-28 07:07:50'),(38,9,'account&setting_e-commerce','E-Commerce','2021-07-28 07:10:00','2021-07-28 07:10:00'),(39,9,'account&setting_domain','Domain','2021-07-28 07:10:37','2021-07-28 07:10:37'),(40,9,'account&setting_subscription','Subscription','2021-07-28 07:11:33','2021-07-28 07:11:33'),(41,10,'reports_user','User','2021-07-28 07:12:18','2021-07-28 07:12:18'),(42,10,'reports_courses','Courses','2021-07-28 07:12:44','2021-07-28 07:12:44'),(43,10,'reports_groups','groups','2021-07-28 07:13:11','2021-07-28 07:13:11'),(44,10,'reports_branches','Branches','2021-07-28 07:13:30','2021-07-28 07:13:30'),(45,10,'reports_tests','Tests','2021-07-28 07:13:50','2021-07-28 07:13:50'),(46,10,'reports_survey','Survey','2021-07-28 07:14:12','2021-07-28 07:14:12'),(47,10,'report_assignments','Assignments','2021-07-28 07:14:42','2021-07-28 07:14:42'),(48,10,'reports_itls','ITls','2021-07-28 07:15:40','2021-07-28 07:15:40'),(49,10,'reports_custom','Custom','2021-07-28 07:15:56','2021-07-28 07:15:56'),(50,11,'files_view','View','2021-07-28 07:17:43','2021-07-28 07:17:43'),(51,11,'files_create','Create','2021-07-28 07:18:00','2021-07-28 07:18:00'),(52,11,'files_update','Update','2021-07-28 07:18:28','2021-07-28 07:18:28'),(53,11,'files_delete','Delete','2021-07-28 07:18:42','2021-07-28 07:18:42'),(54,14,'courses_view','View','2021-08-03 11:27:55','2021-08-03 11:27:55'),(55,14,'courses_create','Create','2021-08-03 11:28:22','2021-08-03 11:28:22'),(56,14,'courses_update','Update','2021-08-03 11:28:51','2021-08-03 11:28:51'),(57,14,'courses_share','Share','2021-08-03 11:29:16','2021-08-03 11:29:16'),(58,14,'courses_delete','Delete','2021-08-03 11:29:55','2021-08-03 11:29:55'),(59,15,'users_view','View','2021-08-03 11:59:51','2021-08-03 11:59:51'),(60,15,'users_add','Add','2021-08-03 12:01:01','2021-08-03 12:01:01'),(61,15,'users_reset','Reset','2021-08-03 12:01:24','2021-08-03 12:01:24'),(62,15,'users_remove','Remove','2021-08-03 12:01:47','2021-08-03 12:01:47'),(63,16,'units_view','View','2021-08-03 12:02:21','2021-08-03 12:02:21'),(64,16,'units_create','Create','2021-08-03 12:02:43','2021-08-03 12:02:43'),(65,16,'units_update','Update','2021-08-03 12:03:04','2021-08-03 12:03:04'),(66,16,'units_delete','Delete','2021-08-03 12:03:25','2021-08-03 12:03:25'),(67,17,'rules_view','View','2021-08-03 12:04:00','2021-08-03 12:04:00'),(68,17,'rules_update','Update','2021-08-03 12:04:17','2021-08-03 12:04:17'),(69,11,'files_view','View','2021-08-03 12:04:42','2021-08-03 12:04:42'),(70,18,'files_add','Add','2021-08-03 12:05:06','2021-08-03 12:05:06'),(71,18,'files_update','Update','2021-08-03 12:05:39','2021-08-03 12:05:39'),(72,18,'files_delete','Delete','2021-08-03 12:06:27','2021-08-03 12:06:27'),(73,19,'groups_view','View','2021-08-03 12:07:09','2021-08-03 12:07:09'),(74,19,'groups_create','Create','2021-08-03 12:07:32','2021-08-03 12:07:32'),(75,19,'groups_update','Update','2021-08-03 12:07:48','2021-08-03 12:07:48'),(76,19,'groups_delete','Delete','2021-08-03 12:08:12','2021-08-03 12:08:12'),(77,20,'reports_courses','Courses','2021-08-03 12:09:01','2021-08-03 12:09:01'),(78,20,'reports_tests','Tests','2021-08-03 12:09:21','2021-08-03 12:09:21'),(79,20,'reports_surveys','Surveys','2021-08-03 12:09:52','2021-08-03 12:09:52'),(80,20,'reports_assignments','Assignments','2021-08-03 12:10:25','2021-08-03 12:10:25'),(81,20,'reports_ilts','ILTs','2021-08-03 12:10:46','2021-08-03 12:10:46'),(82,21,'conferences_view','View','2021-08-03 12:11:30','2021-08-03 12:11:30'),(83,21,'conferences_create','Create','2021-08-03 12:11:45','2021-08-03 12:11:45'),(84,21,'conferences_update','Update','2021-08-03 12:12:02','2021-08-03 12:12:02'),(85,21,'conferences_delete','Delete','2021-08-03 12:12:43','2021-08-03 12:12:43'),(86,22,'users_view','View','2021-08-03 12:13:37','2021-08-03 12:13:37'),(87,22,'users_add','Add','2021-08-03 12:13:53','2021-08-03 12:13:53'),(88,23,'discussions_view','View','2021-08-03 12:14:47','2021-08-03 12:14:47'),(89,23,'discussions_create','Create','2021-08-03 12:15:05','2021-08-03 12:15:05'),(90,23,'discussions_reply/comment','Reply/Comment','2021-08-03 12:15:40','2021-08-03 12:15:40'),(91,23,'discussions_moderate','Moderate','2021-08-03 12:16:36','2021-08-03 12:16:36'),(92,24,'calendar_view','View','2021-08-03 12:17:18','2021-08-03 12:17:18'),(93,24,'calendar_create','Create','2021-08-03 12:17:43','2021-08-03 12:17:43'),(94,24,'calendar_moderate','Moderate','2021-08-03 12:18:13','2021-08-03 12:18:13'),(95,30,'other_progress','Progress','2021-08-06 08:56:14','2021-08-06 08:56:14'),(96,30,'other_gamification','Gamification','2021-08-06 08:57:09','2021-08-06 08:57:09'),(97,31,'messages_view','View','2021-08-06 09:00:41','2021-08-06 09:00:41'),(98,31,'messages_send_to_system_administrators','Send to system administrators','2021-08-06 09:02:07','2021-08-06 09:02:07'),(99,31,'messages_and_to_course_instructor','... and to course instructor','2021-08-06 09:04:25','2021-08-06 09:04:25'),(100,31,'message_and_to_course_branch_group_members','... and to course, branch, group members','2021-08-06 09:08:28','2021-08-06 09:08:28'),(101,31,'messages_and_to_individuals_in_courses_branches_groups','... and to individuals in courses, branches, groups','2021-08-06 09:11:04','2021-08-06 09:11:04'),(102,31,'message_and_to_anyone_in_the_system','... and to anyone in the system','2021-08-06 09:12:13','2021-08-06 09:12:13'),(103,32,'profile_read','Read','2021-08-06 09:24:09','2021-08-06 09:24:09'),(104,32,'profile_Update','Update','2021-08-06 09:25:35','2021-08-06 09:25:35'),(105,32,'profile_reset_password','Reset password','2021-08-06 09:26:33','2021-08-06 09:26:33'),(106,33,'other_help','Help','2021-08-06 09:43:02','2021-08-06 09:43:02'),(107,33,'other_timeline','Timeline','2021-08-06 09:43:57','2021-08-06 09:43:57');
/*!40000 ALTER TABLE `permission_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_category`
--

DROP TABLE IF EXISTS `permission_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `module_type_id` int NOT NULL,
  `category_code` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `parent_category_id` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_category`
--

LOCK TABLES `permission_category` WRITE;
/*!40000 ALTER TABLE `permission_category` DISABLE KEYS */;
INSERT INTO `permission_category` VALUES (1,1,'administrator_user','User',0,'2021-07-28 05:54:53','2021-07-28 05:54:53'),(2,1,'administrator_courses','Courses',0,'2021-07-28 05:55:36','2021-07-28 05:55:36'),(3,1,'administrator_groups','Groups',0,'2021-07-28 05:56:59','2021-07-28 05:56:59'),(4,1,'administrator_categories','Categories',0,'2021-07-28 05:58:15','2021-07-28 05:58:15'),(5,1,'administrator_branches','Branches',0,'2021-07-28 05:58:33','2021-07-28 05:58:33'),(6,1,'administrator_event_engine','Event engine',0,'2021-07-28 05:59:08','2021-07-28 05:59:08'),(7,1,'administrator_import_export','Import  - Export',0,'2021-07-28 05:59:47','2021-07-28 05:59:47'),(8,1,'administrator_user_types','User Types',0,'2021-07-28 06:00:10','2021-07-28 06:00:10'),(9,1,'administrator_account$setting','Account & Settings',0,'2021-07-28 06:00:31','2021-07-28 06:00:31'),(10,1,'administrator_reports','Reports',0,'2021-07-28 06:00:47','2021-07-28 06:00:47'),(11,1,'administrator_files','Files',0,'2021-07-28 06:01:06','2021-07-28 06:01:06'),(12,1,'administrator_notification','Notifications',6,'2021-07-28 06:02:38','2021-07-28 06:02:38'),(13,1,'administrator_automations','Automations',6,'2021-07-28 06:03:08','2021-07-28 06:03:08'),(14,2,'instructor_courses','Courses',0,'2021-08-03 11:05:43','2021-08-03 11:05:43'),(15,2,'instructor_Users','Users',14,'2021-08-03 11:06:59','2021-08-03 11:06:59'),(16,2,'instructor_Units','Units',14,'2021-08-03 11:07:43','2021-08-03 11:07:43'),(17,2,'instructor_rules','Rules',14,'2021-08-03 11:12:48','2021-08-03 11:12:48'),(18,2,'instructor_files','Files',14,'2021-08-03 11:14:09','2021-08-03 11:14:09'),(19,2,'instructor_groups','Groups',0,'2021-08-03 11:14:55','2021-08-03 11:14:55'),(20,2,'instructor_reports','Reports',0,'2021-08-03 11:16:07','2021-08-03 11:16:07'),(21,2,'instructor_conferences','Conferences',0,'2021-08-03 11:18:37','2021-08-03 11:18:37'),(22,2,'instructor_users','Users',21,'2021-08-03 11:21:41','2021-08-03 11:21:41'),(23,2,'instructor_discussions','Discussions',0,'2021-08-03 11:23:23','2021-08-03 11:23:23'),(24,2,'instructor_calendar','Calendar',0,'2021-08-03 11:23:54','2021-08-03 11:23:54'),(25,3,'learner_courses','Courses',0,'2021-08-04 04:59:55','2021-08-04 04:59:55'),(26,3,'learner_course_catalog','Course Catalog',0,'2021-08-04 04:59:55','2021-08-04 04:59:55'),(27,3,'learner_conferences','Conferences',0,'2021-08-04 04:59:55','2021-08-04 04:59:55'),(28,3,'learner_discussions','Discussions',0,'2021-08-04 04:59:55','2021-08-04 04:59:55'),(29,3,'learner_calendar','Calendar',0,'2021-08-04 04:59:55','2021-08-04 04:59:55'),(30,3,'learner_other','Other',0,'2021-08-06 08:53:27','2021-08-06 08:53:27'),(31,4,'general_messages','Messages',0,'2021-08-06 08:59:29','2021-08-06 08:59:29'),(32,4,'general_profile','Profile',0,'2021-08-06 09:16:00','2021-08-06 09:16:00'),(33,4,'general_other','Other',0,'2021-08-06 09:41:05','2021-08-06 09:41:05');
/*!40000 ALTER TABLE `permission_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_type`
--

DROP TABLE IF EXISTS `role_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_type`
--

LOCK TABLES `role_type` WRITE;
/*!40000 ALTER TABLE `role_type` DISABLE KEYS */;
INSERT INTO `role_type` VALUES (1,'super admin','2021-07-22 07:34:22','2021-07-22 07:34:22'),(2,'administrator','2021-07-22 07:34:58','2021-07-22 07:34:58'),(3,'instructor','2021-07-22 07:35:25','2021-07-22 07:35:25'),(4,'learner','2021-07-22 07:46:30','2021-07-22 07:46:30');
/*!40000 ALTER TABLE `role_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timezone`
--

DROP TABLE IF EXISTS `timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `timezone` (
  `id` int NOT NULL AUTO_INCREMENT,
  `utc_offset` varchar(30) NOT NULL,
  `is_currently_dst` int NOT NULL,
  `datediff` varchar(30) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timezone`
--

LOCK TABLES `timezone` WRITE;
/*!40000 ALTER TABLE `timezone` DISABLE KEYS */;
INSERT INTO `timezone` VALUES (1,'GMT-12:00',0,'-12','International Date Line West','2021-07-29 12:28:36','2021-07-29 12:28:36'),(2,'GMT-11:00',0,'-11','Midway Island, Samoa','2021-07-29 12:35:23','2021-07-29 12:35:23'),(3,'GMT-10:00',0,'-10','Hawaii','2021-07-29 12:35:23','2021-07-29 12:35:23'),(4,'GMT-09:00',1,'-9','Alaska','2021-07-29 12:35:23','2021-07-29 12:35:23'),(5,'GMT-08:00',1,'-8','Pacific Time (US & Canada)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(6,'GMT-08:00',1,'-8','Tijuana, Baja California','2021-07-29 12:35:23','2021-07-29 12:35:23'),(7,'GMT-07:00',0,'-7','Arizona','2021-07-29 12:35:23','2021-07-29 12:35:23'),(8,'GMT-07:00',1,'-7','Chihuahua, La Paz, Mazatlan','2021-07-29 12:35:23','2021-07-29 12:35:23'),(9,'GMT-07:00',1,'-7','Mountain Time (US & Canada)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(10,'GMT-06:00',0,'-6','Central America','2021-07-29 12:35:23','2021-07-29 12:35:23'),(11,'GMT-06:00',1,'-6','Central Time (US & Canada)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(12,'GMT-06:00',1,'-6','Guadalajara, Mexico City, Monterrey','2021-07-29 12:35:23','2021-07-29 12:35:23'),(13,'GMT-06:00',0,'-6','Saskatchewan','2021-07-29 12:35:23','2021-07-29 12:35:23'),(14,'GMT-05:00',0,'-5','Bogota, Lima, Quito, Rio Branco','2021-07-29 12:35:23','2021-07-29 12:35:23'),(15,'GMT-05:00',1,'-5','Eastern Time (US & Canada)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(16,'GMT-05:00',1,'-5','Indiana (East)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(17,'GMT-04:00',1,'-4','Atlantic Time (Canada)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(18,'GMT-04:00',0,'-4','Caracas, La Paz','2021-07-29 12:35:23','2021-07-29 12:35:23'),(19,'GMT-04:00',0,'-4','Manaus','2021-07-29 12:35:23','2021-07-29 12:35:23'),(20,'GMT-04:00',1,'-4','Santiago','2021-07-29 12:35:23','2021-07-29 12:35:23'),(21,'GMT-03:30',1,'-3.5','Newfoundland','2021-07-29 12:35:23','2021-07-29 12:35:23'),(22,'GMT-03:00',1,'-3','Brasilia','2021-07-29 12:35:23','2021-07-29 12:35:23'),(23,'GMT-03:00',0,'-3','Buenos Aires, Georgetown','2021-07-29 12:35:23','2021-07-29 12:35:23'),(24,'GMT-03:00',1,'-3','Greenland','2021-07-29 12:35:23','2021-07-29 12:35:23'),(25,'GMT-03:00',1,'-3','Montevideo','2021-07-29 12:35:23','2021-07-29 12:35:23'),(26,'GMT-02:00',1,'-2','Mid-Atlantic','2021-07-29 12:35:23','2021-07-29 12:35:23'),(27,'GMT-01:00',0,'-1','Cape Verde Is.','2021-07-29 12:35:23','2021-07-29 12:35:23'),(28,'GMT-01:00',1,'-1','Azores','2021-07-29 12:35:23','2021-07-29 12:35:23'),(29,'GMT+00:00',0,'0','Casablanca, Monrovia, Reykjavik','2021-07-29 12:35:23','2021-07-29 12:35:23'),(30,'GMT+00:00',1,'0','Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London','2021-07-29 12:35:23','2021-07-29 12:35:23'),(31,'GMT+01:00',1,'1','Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna','2021-07-29 12:35:23','2021-07-29 12:35:23'),(32,'GMT+01:00',1,'1','Belgrade, Bratislava, Budapest, Ljubljana, Prague','2021-07-29 12:35:23','2021-07-29 12:35:23'),(33,'GMT+01:00',1,'1','Brussels, Copenhagen, Madrid, Paris','2021-07-29 12:35:23','2021-07-29 12:35:23'),(34,'GMT+01:00',1,'1','Sarajevo, Skopje, Warsaw, Zagreb','2021-07-29 12:35:23','2021-07-29 12:35:23'),(35,'GMT+01:00',1,'1','West Central Africa','2021-07-29 12:35:23','2021-07-29 12:35:23'),(36,'GMT+02:00',1,'2','Amman','2021-07-29 12:35:23','2021-07-29 12:35:23'),(37,'GMT+02:00',1,'2','Athens, Bucharest, Istanbul','2021-07-29 12:35:23','2021-07-29 12:35:23'),(38,'GMT+02:00',1,'2','Beirut','2021-07-29 12:35:23','2021-07-29 12:35:23'),(39,'GMT+02:00',1,'2','Cairo','2021-07-29 12:35:23','2021-07-29 12:35:23'),(40,'GMT+02:00',0,'2','Harare, Pretoria','2021-07-29 12:35:23','2021-07-29 12:35:23'),(41,'GMT+02:00',1,'2','Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius','2021-07-29 12:35:23','2021-07-29 12:35:23'),(42,'GMT+02:00',1,'2','Jerusalem','2021-07-29 12:35:23','2021-07-29 12:35:23'),(43,'GMT+02:00',1,'2','Minsk','2021-07-29 12:35:23','2021-07-29 12:35:23'),(44,'GMT+02:00',1,'2','Windhoek','2021-07-29 12:35:23','2021-07-29 12:35:23'),(45,'GMT+03:00',0,'3','Kuwait, Riyadh, Baghdad','2021-07-29 12:35:23','2021-07-29 12:35:23'),(46,'GMT+03:00',1,'3','Moscow, St. Petersburg, Volgograd','2021-07-29 12:35:23','2021-07-29 12:35:23'),(47,'GMT+03:00',0,'3','Nairobi','2021-07-29 12:35:23','2021-07-29 12:35:23'),(48,'GMT+03:00',0,'3','Tbilisi','2021-07-29 12:35:23','2021-07-29 12:35:23'),(49,'GMT+03:30',1,'3.5','Tehran','2021-07-29 12:35:23','2021-07-29 12:35:23'),(50,'GMT+04:00',0,'4','Abu Dhabi, Muscat','2021-07-29 12:35:23','2021-07-29 12:35:23'),(51,'GMT+04:00',1,'4','Baku','2021-07-29 12:35:23','2021-07-29 12:35:23'),(52,'GMT+04:00',1,'4','Yerevan','2021-07-29 12:35:23','2021-07-29 12:35:23'),(53,'GMT+04:30',0,'4.5','Kabul','2021-07-29 12:35:23','2021-07-29 12:35:23'),(54,'GMT+05:00',1,'5','Yekaterinburg','2021-07-29 12:35:23','2021-07-29 12:35:23'),(55,'GMT+05:00',0,'5','Islamabad, Karachi, Tashkent','2021-07-29 12:35:23','2021-07-29 12:35:23'),(56,'GMT+05:30',0,'5.5','Sri Jayawardenapura','2021-07-29 12:35:23','2021-07-29 12:35:23'),(57,'GMT+05:30',0,'5.5','Chennai, Kolkata, Mumbai, New Delhi','2021-07-29 12:35:23','2021-07-29 12:35:23'),(58,'GMT+05:45',0,'5.75','Kathmandu','2021-07-29 12:35:23','2021-07-29 12:35:23'),(59,'GMT+06:00',1,'6','Almaty, Novosibirsk','2021-07-29 12:35:23','2021-07-29 12:35:23'),(60,'GMT+06:00',0,'6','Astana, Dhaka','2021-07-29 12:35:23','2021-07-29 12:35:23'),(61,'GMT+06:30',0,'6.5','Yangon (Rangoon)','2021-07-29 12:35:23','2021-07-29 12:35:23'),(62,'GMT+07:00',0,'7','Bangkok, Hanoi, Jakarta','2021-07-29 12:35:23','2021-07-29 12:35:23'),(63,'GMT+07:00',1,'7','Krasnoyarsk','2021-07-29 12:35:23','2021-07-29 12:35:23'),(64,'GMT+08:00',0,'8','Beijing, Chongqing, Hong Kong, Urumqi','2021-07-29 12:35:23','2021-07-29 12:35:23'),(65,'GMT+08:00',0,'8','Kuala Lumpur, Singapore','2021-07-29 12:35:23','2021-07-29 12:35:23'),(66,'GMT+08:00',0,'8','Irkutsk, Ulaan Bataar','2021-07-29 12:35:23','2021-07-29 12:35:23'),(67,'GMT+08:00',0,'8','Perth','2021-07-29 12:35:23','2021-07-29 12:35:23'),(68,'GMT+08:00',0,'8','Taipei','2021-07-29 12:35:23','2021-07-29 12:35:23'),(69,'GMT+09:00',0,'9','Osaka, Sapporo, Tokyo','2021-07-29 12:35:23','2021-07-29 12:35:23'),(70,'GMT+09:00',0,'9','Seoul','2021-07-29 12:35:23','2021-07-29 12:35:23'),(71,'GMT+09:00',1,'9','Yakutsk','2021-07-29 12:35:23','2021-07-29 12:35:23'),(72,'GMT+09:30',0,'9.5','Adelaide','2021-07-29 12:35:23','2021-07-29 12:35:23'),(73,'GMT+09:30',0,'9.5','Darwin','2021-07-29 12:35:23','2021-07-29 12:35:23'),(74,'GMT+10:00',0,'10','Brisbane','2021-07-29 12:35:23','2021-07-29 12:35:23'),(75,'GMT+10:00',1,'10','Canberra, Melbourne, Sydney','2021-07-29 12:35:23','2021-07-29 12:35:23'),(76,'GMT+10:00',1,'10','Hobart','2021-07-29 12:35:23','2021-07-29 12:35:23'),(77,'GMT+10:00',0,'10','Guam, Port Moresby','2021-07-29 12:35:23','2021-07-29 12:35:23'),(78,'GMT+10:00',1,'10','Vladivostok','2021-07-29 12:35:23','2021-07-29 12:35:23'),(79,'GMT+11:00',1,'11','Magadan, Solomon Is., New Caledonia','2021-07-29 12:35:23','2021-07-29 12:35:23'),(80,'GMT+12:00',1,'12','Auckland, Wellington','2021-07-29 12:35:23','2021-07-29 12:35:23'),(81,'GMT+12:00',0,'12','Fiji, Kamchatka, Marshall Is.','2021-07-29 12:35:23','2021-07-29 12:35:23'),(82,'GMT+13:00',0,'13','Nuku alofa','2021-07-29 12:35:23','2021-07-29 12:35:23');
/*!40000 ALTER TABLE `timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_type_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` VALUES (1,'Super Admin',1),(2,'Manager',2);
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `educator_id` int DEFAULT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `full_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `first_name` varchar(22) DEFAULT NULL,
  `last_name` varchar(22) DEFAULT NULL,
  `bio` varchar(150) DEFAULT NULL,
  `user_type` varchar(20) DEFAULT NULL,
  `timezone` varchar(60) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `active` varchar(10) DEFAULT 'false',
  `deactivated_at` date DEFAULT NULL,
  `email_exclude` varchar(10) DEFAULT 'false',
  `profile_pic` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Test','e10adc3949ba59abbe56e057f20f883e','test@testmail.com','usertest',NULL,NULL,NULL,'1',NULL,NULL,'true',NULL,'false',NULL,'2021-07-30 12:32:27',NULL),(2,1,'test1','e10adc3949ba59abbe56e057f20f883e','test1@testmail.com','User test',NULL,NULL,NULL,'1',NULL,NULL,'true',NULL,'false',NULL,'2021-07-30 12:37:23',NULL),(3,1,'rocky','e10adc3949ba59abbe56e057f20f883e','rocky@testmail.com','Rocky kumar','Rocky','kumar','cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum','2','-4',NULL,'true','0000-00-00','0','3.png','2021-07-30 12:40:22',NULL),(4,2,'mydomain','e10adc3949ba59abbe56e057f20f883e','me@testmail.com','my domain',NULL,NULL,NULL,'1',NULL,NULL,'true',NULL,'false',NULL,'2021-07-30 12:51:33',NULL),(5,2,'user12','e10adc3949ba59abbe56e057f20f883e','user1@testmail.com','User first','User','first','cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum','2','-12',NULL,'true','0000-00-00','0','5.png','2021-07-30 12:53:57',NULL),(6,3,'testmail','$2y$10$Z9OtnKGK5VbvbrPt9xpgz.DRYiS7qhDq5TuIy3Pmw59n17PURT9GG','testmail@test.co','test mail',NULL,NULL,NULL,'1',NULL,NULL,'true',NULL,'false',NULL,'2021-08-02 09:04:27',NULL),(7,4,'codebuddy','$2y$10$JfSZJZVYVPHtW3Y6XdI./.mzoclyRJyBh8d1lt0Ni6c1rjiiP.gSy','code@code.co','code buddy',NULL,NULL,NULL,'1',NULL,NULL,'true',NULL,'false',NULL,'2021-08-02 10:18:50',NULL),(8,3,'test@testmail.com','cfd2cb8a9bb128603eb44b182db014fc','test@testmail.com','tarun varshney','tarun','varshney','test@testmail.comtest@testmail.comtest@testmail.comtest@testmail.comtest@testmail.comtest@testmail.comtest@testmail.comtest@testmail.com','1','5.5',NULL,'true','0000-00-00','0','8.png','2021-08-03 05:08:16',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `viewadmin`
--

DROP TABLE IF EXISTS `viewadmin`;
/*!50001 DROP VIEW IF EXISTS `viewadmin`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `viewadmin` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `email`,
 1 AS `email_verified_at`,
 1 AS `password`,
 1 AS `created_at`,
 1 AS `update_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `my_view`
--

/*!50001 DROP VIEW IF EXISTS `my_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`codeflies`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `my_view` AS select `module_type`.`id` AS `id`,`module_type`.`name` AS `name`,`module_type`.`is_active` AS `is_active`,`module_type`.`created_at` AS `created_at`,`module_type`.`updated_at` AS `updated_at` from `module_type` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `viewadmin`
--

/*!50001 DROP VIEW IF EXISTS `viewadmin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`codeflies`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `viewadmin` AS select `admin`.`id` AS `id`,`admin`.`name` AS `name`,`admin`.`email` AS `email`,`admin`.`email_verified_at` AS `email_verified_at`,`admin`.`password` AS `password`,`admin`.`created_at` AS `created_at`,`admin`.`update_at` AS `update_at` from `admin` where (0 <> 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-16  5:03:06
