<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
	<footer class="footer">
		<div class="container">
			<div class="footer-logo">
				<?php dynamic_sidebar('sidebar-4'); ?>
				</div>
			<ul class="footer-menu">
				<?php dynamic_sidebar('sidebar-5'); ?>
				<!-- <li>
					<a href="https://wp.localserverweb.com/EinsteinsMind/contact-us/">Contact Us</a>
				</li>
				<li><a href="javascript:void(0)">Help &amp; Support</a></li>
				<li><a href="https://wp.localserverweb.com/EinsteinsMind/faqs">FAQs</a></li>
				<li><a href="javascript:void(0)">Terms of service</a></li>
				<li><a href="javascript:void(0)">Privacy</a></li> -->
			</ul>
			<div><?php dynamic_sidebar('sidebar-6'); ?></div>
			<!-- <ul class="footer-bottom-social"> -->
				
				<!-- <li><a href="https://www.facebook.com/Einsteins-Mind-108457918045055"><i class="fab fa-facebook-f"></i></a></li>
				<li><a href="https://www.linkedin.com/company/einsteinsmind/about/?viewAsMember=true"><i class="fab fa-linkedin-in"></i></a></li>
				<li><a href="https://twitter.com/mind_einstein"><i class="fab fa-twitter"></i></a></li> -->
			<!-- </ul> -->
		</div>
	</footer>
	<div class="copyright">
		<?php dynamic_sidebar('sidebar-7'); ?>
		<!-- &copy; Einstein’s Mind -->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

<div id="sidebar" class="sidebar-offcanvas-menu">
	<div id="sidebar-wrapper" class="sidebar-wrapper">
		<div class="outer">
			<div class="sidebar-top-header">
				<div class="sidebar-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php header_image(); ?>" class="header-image" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" /></a>
				</div>
				<div class="sidebar-close-btn">
					<a class="close-sidebar">✕</a>
				</div>
			</div>
			<div class="inner">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'menu_class'     => 'sidebar-menu',
						)
					);
				?> 
			</div>
		</div>
	</div>
</div>


<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.2.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>


</body>
</html>
