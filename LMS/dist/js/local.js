jQuery(document).ready(function(){
	stopLoader();
    // loader function
		$(".main-header .nav-item > a").click(function () {
		  if ($(this).hasClass("active")) {
		    $(".nav-item-help > a").removeClass("active");
		  }
		   else {
		    $(".nav-item-help > a").removeClass("active");
		    $(this).addClass("active");
		  }
		});


	jQuery("#togglemenu").click(function(e){
		jQuery(this).addClass('active');
		jQuery(this).next('.main-header ul.navbar-nav').slideToggle();
	});

	 $('.next-section-btn').click(function () {
            $(this).parents('.section.active').next().addClass('active');
            $(this).parents('.section.active').removeClass('active');
       });
});


function startLoader() {
	$(".loading_wrapper").show();
}

function stopLoader() {
	$(".loading_wrapper").fadeOut("slow");
}


$(window).load(function() {
     stopLoader();
});

