var snackbar =  (message) => {
    var x = document.getElementById("snackbar");
    x.innerHTML = message;
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4500);
}

// Common Ajax function
var CommonAjax = (url, method, FormData) => {
	var data;
	$.ajax({
		url: url,
		type: method,
		dataType: "JSON",
		data: FormData,
		contentType: false,
		processData: false,
		async : false,
		beforeSend : function () {
			startLoader();
		}
	})
	.done(function(response) {
		data = response;
	})
	.fail(function() {
		data = false;
	})
	.always(function() {
		stopLoader();
	});
	return data;
}

// CommonWithOut File Ajax function
var CommonAjaxForObjData = (url, method, FormData) => {
	var data;
	$.ajax({
		url: url,
		type: method,
		dataType: "JSON",
		data: FormData,
		async : false,
		beforeSend : function () {
			startLoader();
		}
	})
	.done(function(response) {
		data = response;
	})
	.fail(function() {
		data = false;
	})
	.always(function() {
		stopLoader();
	});
	return data;
}


// Common image upload function 
var ImageUpload = (url, file) => {
	var formData = new FormData();
	formData.append('formData', file);
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'JSON',
		data: formData,
		contentType: false,
		processData: false
	})
	.done(function(response) {
		alert("hi");
		return response;
		
	})
	.fail(function(response) {
		alert("Some error occured.");
	})
	.always(function() {
		console.log("done.")
	});	
}

// image preview function 
var imagesPreview = (input, placeToInsertImagePreview) => {
	$(placeToInsertImagePreview).html("");
	$(".previous_image").remove();
    if (input.files) {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function(event) {
                $($.parseHTML(`<img class='${i}' width='50%'>`)).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
				if(placeToInsertImagePreview == "div.gallery_1"){
					$('.favicon').remove();
				}
				else if(placeToInsertImagePreview == "div.gallery"){
					$('.image').remove();
				}
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};

// Hide all form errors on load
$('.invalid-feedback').css({
	display: 'none'
});
// Add LMS USERS FUNCTION
$('#LMS_AddUser').submit(function(event) {
	event.preventDefault();
	// Form Data
	let myForm = document.getElementById('LMS_AddUser');
	let formData = new FormData(myForm);
	// URL
	var url = $("#LMS_AddUser").attr("action");
	// Ajax request
	var response = CommonAjax(url, 'POST', formData);
	if (response !== false) {
		// Displaying validation errors
		var err = response.error;	
		for (var key in err) {
		    if (err.hasOwnProperty(key)) {
		        $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
		        $('.invalid-feedback').css({
					display: 'block'
				});
		    }
		}
		// Handling response
		if (response.success === true) {
			window.location = response.target;
		}
		snackbar(response.message);
	} else {
		snackbar("Some error occured.");
	}
});

// Category function
// Add Category  FUNCTION
$('#CourseCategoryAdd').submit(function(event) {
	event.preventDefault();
	// // Form Data
	let myForm = document.getElementById('CourseCategoryAdd');
	let formData = new FormData(myForm);
	// URL
	var url = $("#CourseCategoryAdd").attr("action");
	// Ajax request
	var response = CommonAjax(url, 'POST', formData);
	if (response !== false) {
		// Displaying validation errors
		var err = response.error;	
		for (var key in err) {
		    if (err.hasOwnProperty(key)) {
		        $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
		        $('.invalid-feedback').css({
					display: 'block'
				});
		    }
		}
		// Handling response
		if (response.success === true) {
			window.location = response.target;
		}
		snackbar(response.message);
	} else {
		snackbar("Some error occured.");
	}
});

// End Category function 

 // Delete Category
function DeleteCategory(id) {

    if(confirm("Are you sure you want to delete this record?")){
        startLoader();
    // Form Data
    

    // var formdata = JSON.stringify(data); 

    // URL
    var url = 'delete';
    // Ajax request
    var response = CommonAjaxForObjData(url, 'POST', {category_id : id});
    // Handling response
    if (response.success === true) {
            $categoryListTable.bootstrapTable('refresh');
    }
    snackbar(response.message);
  } else {
    confirm("Category is not delete!");
  }
}
// End Function

// Group function
// Add Group FUNCTION
$('#GroupAdd').submit(function(event) {
	event.preventDefault();
	
	// // Form Data
	let myForm = document.getElementById('GroupAdd');
	let formData = new FormData(myForm);
	// URL
	var url = $("#GroupAdd").attr("action");
	// Ajax request
	var response = CommonAjax(url, 'POST', formData);
	if (response !== false) {
		// Displaying validation errors
		var err = response.error;	
		for (var key in err) {
		    if (err.hasOwnProperty(key)) {
		        $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
		        $('.invalid-feedback').css({
					display: 'block'
				});
		    }
		}
		// Handling response
		if (response.success === true) {
			setTimeout(
			function() 
			{
				window.location = response.target;
			}, 3000);
		}
		snackbar(response.message);
	} else {
		snackbar("Some error occured.");
	}
});

// End Group function 


// End Category function 

 // Delete Group
function DeleteGroup(id) {

    if(confirm("Are you sure you want to delete this record?")){
        startLoader();
    // Form Data
    

    // var formdata = JSON.stringify(data); 

    // URL
    var url = 'delete';
    // Ajax request
    var response = CommonAjaxForObjData(url, 'POST', {group_id : id});
    // Handling response
    if (response.success === true) {
            $groupListTable.bootstrapTable('refresh');
    }
    snackbar(response.message);
  } else {
    confirm("Group is not delete!");
  }
}
// End Function

// Add Branch

// Add LMS USERS FUNCTION
$('#LMS_AddBranches').submit(function(event) {
	event.preventDefault();
	startLoader();
	// Form Data
	let myForm = document.getElementById('LMS_AddBranches');
	let formData = new FormData(myForm);
	// URL
	var url = $("#LMS_AddBranches").attr("action");
	// Ajax request
	var response = CommonAjax(url, 'POST', formData);
	if (response !== false) {
		// Displaying validation errors
		var err = response.error;	
		for (var key in err) {
		    if (err.hasOwnProperty(key)) {
		        $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
		        $('.invalid-feedback').css({
					display: 'block'
				});
		    }
		}
		snackbar(response.message);
		// Handling response
		if (response.success === true) {
			setTimeout(
			function() 
			{
				window.location = response.target;
			}, 5000);
		}
	} else {
		snackbar("Some error occured.");
	}
});

// Delete Branch
function DeleteBranch(id) {

    if(confirm("Are you sure you want to delete this record?")){
        startLoader();
		// Form Data
		

		// var formdata = JSON.stringify(data); 

		// URL
		var url = 'delete';
		// Ajax request
		var response = CommonAjaxForObjData(url, 'POST', {branch_id : id});
		// Handling response
		if (response.success === true) {
          	$branchListTable.bootstrapTable('refresh');
		}
		snackbar(response.message);
	} else {
		confirm("Branch is not delete!");
	}
}

// Courses Function

$('#CourseADD').submit(function(event) {
	event.preventDefault();
	
	// // Form Data
	let myForm = document.getElementById('CourseADD');
	let formData = new FormData(myForm);
	// URL
	var url = $("#CourseADD").attr("action");
	// Ajax request
	var response = CommonAjax(url, 'POST', formData);
	if (response !== false) {
		// Displaying validation errors
		var err = response.error;	
		for (var key in err) {
		    if (err.hasOwnProperty(key)) {
		        $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
		        $('.invalid-feedback').css({
					display: 'block'
				});
		    }
		}
		// Handling response
		if (response.success === true) {
			window.location = response.target;
		}
		snackbar(response.message);
	} else {
		snackbar("Some error occured.");
	}
});


// Delete Course
function DeleteCourse(id) {

    if(confirm("Are you sure you want to delete this record?")){
        startLoader();
		// Form Data
		

		// var formdata = JSON.stringify(data); 

		// URL
		var url = 'delete';
		// Ajax request
		var response = CommonAjaxForObjData(url, 'POST', {course_id : id});
		// Handling response
		if (response.success === true) {
          	$courseListTable.bootstrapTable('refresh');
		}
		snackbar(response.message);
	} else {
		confirm("Cource is not delete!");
	}
}