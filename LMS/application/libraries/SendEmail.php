<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendEmail {

	protected $CI;
	// Invitation mail for user

	public function __construct()
	{
		// Codeigniter Super object
		$this->CI =& get_instance();
		// Loading email library
		$this->CI->load->library('email');
	}
	public function Send($from, $from_name, $email_to, $subject, $message)
	{
		$config['protocol']    = 'smtp';
        $config['smtp_host']    = 'smtp.mailtrap.io';
        $config['smtp_port']    = '587';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'cf7da1e913f2cf';
        $config['smtp_pass']    = 'd60454dd56510b';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->CI->email->initialize($config);

		$this->CI->email->from($from, $from_name);
		$this->CI->email->to($email_to);
		$this->CI->email->subject($subject);
		$this->CI->email->message(
			$message
		);
		return $this->CI->email->send();
	}

}

/* End of file SendEmail.php */
