<?php 
$this->load->view('admin/layouts/header');
$this->load->view('admin/layouts/sidebar');

?>

<div class="container">
  	<h2><b>Add Category</b></h2>
  	<form action="<?php echo base_url('admin/category/create');?>" method="post">
		<div class="row">
		    <div class="form-group col-sm-6" >
		      	<label for="name">Select Module Type</label>
		      	<select id="module_type" class="form-control" name="module_type">
				    <option>Please select</option>
			  		<?php if (isset($moduledata)) { ?>
			      		<?php foreach ($moduledata as $module_data){ ?>
					    <option value="<?php echo $module_data->id; ?>"><?php echo $module_data->name; ?></option>
						<?php } ?>
					<?php } ?>
			  	</select>
		    </div>

		    <div class="form-group col-sm-6" >
		      	<label for="name">Category</label>
		      	<input type="text" class="form-control category" name="category" placeholder="Please Enter Category" />
		    </div>

		    <div class="form-group col-sm-6" >
		      	<label for="is_active">Active</label>
		      	<select id="is_active" class="form-control" name="is_active">
				    <option>Please select</option>
				    <option value="Y">Active</option>
				    <option value="N">Inactive</option>
			  	</select>
		    </div>
		</div>
		    <button type="submit" class="btn btn-success">Submit</button>
  	</form>
</div>

<?php 

$this->load->view('admin/layouts/footer');
$this->load->view('admin/layouts/footer_end');

?> 