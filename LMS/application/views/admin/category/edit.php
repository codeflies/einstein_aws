<?php 
$this->load->view('admin/layouts/header');
$this->load->view('admin/layouts/sidebar');

?>

<div class="container">
  	<h2><b>Edit Role</b></h2>
  	<form action="<?php echo base_url('admin/category/update/'.$result->id);?>" method="post">
		<div class="row">
		    <div class="form-group col-sm-6" >
		      <label for="name">Name:</label>
			  <input type="text" class="form-control" name="name" value="<?php echo $result->name;  ?>" placeholder="Please Enter The Name">
		    </div>
		    <div class="form-group col-sm-6" >
		      <label for="is_active">Active</label>

		      <select id="is_active" class="form-control" name="is_active">
				    <option>Please select</option>
				    <option value="Y" <?php if($result->is_active=='Y') echo 'selected="selected"'; ?>>Active</option>
				    <option value="N" <?php if($result->is_active=='N') echo 'selected="selected"'; ?>>Inactive</option>
			  </select>
		    </div>
		</div>
		    <button type="submit" class="btn btn-success">Submit</button>
  	</form>
</div>

<?php

$this->load->view('admin/layouts/footer');
$this->load->view('admin/layouts/footer_end');

?> 