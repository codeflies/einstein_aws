<?php 
$this->load->view('admin/layouts/header');
$this->load->view('admin/layouts/sidebar');?>


<section class="content user-content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="card ">
          <div class="card-header ">
            <h3 class="card-title">User</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="card-tbl-head">
              <div class="row">
                <div class="col-sm-6">
                  <div class="btn-group">
                    <a href="<?php echo base_url('admin/module-user/create');?>" class="btn btn-primary">Add User</a>
                    <!-- <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu" style="">
                      <a class="dropdown-item" href="#">Import User(s)</a>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="dropdown-divider"></div>
            <div class="table-responsive">
              <table id="example1" class="table table-striped table-hover user-table datatable">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                	<?php if (isset($result)) { ?>
                		<?php foreach ($result as $value) { ?>
                		<tr>
		                  <td><?php echo $value->name ?></td>
                      <td><?php echo ($value->is_active=='Y')?"Active":"Inactive"?></td>
		                  <td>
		                    <i class="fa fa-ellipsis-h"></i>
		                    <div class="hover-tbl-btn">
		                      <!-- <a href="#" class="tbl-btn tbl-signal" title="Reports"><i class="fas fa-signal"></i></a> -->
		                      <!-- <a href="#" class="tbl-btn tbl-logout" title="Log into account"><i class="fas fa-sign-out-alt"></i></a> -->
		                      <a href="<?php echo base_url('admin/module-user/edit/'.$value->id);?>" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
		                      <a href="<?php echo base_url('admin/module-user/delete/'.$value->id);?>" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
		                    </div>
		                  </td>
		                </tr>
						<?php }?>
                		

					<?php }?>
                
                </tfoot>
              </table>
            </tbody>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>

<?php 

$this->load->view('admin/layouts/footer');
$this->load->view('admin/layouts/footer_end');

?> 