
<?php 
$this->load->view('admin/layouts/header');
$this->load->view('admin/layouts/sidebar');
?>

<div class="container">
  	<h2><b>LMS Role- Role Based Access Control</b></h2>
  	<form action="/action_page.php">
		<div class="row">
		    <div class="form-group col-sm-6" >
		      <label for="role">Module Role:</label>
			  <?php if (isset($result)) { ?>

		      <select id="role" class="form-control" name="role">
		      	<option>Please Select Module</option>
				<?php foreach ($result as $item) { ?> 
				    <option value="<?php echo $item->id;?>"><?php echo $item->name; ?></option>
			    <?php } ?>
			  </select>
			<?php }?>
		    </div>
		    <div class="form-group col-sm-6">

		      <label for="category">Please Select category</label>
			  	<?php if (isset($category)) { ?>
		      	<select id="role" class="form-control select_category" name="role">
			  		<option>Please Select</option>
					<?php foreach ($category as $cate_item) { ?> 
					    <option value="<?php echo $cate_item->id;?>" data-name="<?php echo $cate_item->name; ?>"><?php echo $cate_item->name; ?></option>
				    <?php } ?>
			  	</select>
			    <?php } ?>
		    </div>
		    <div class="category_html form-group col-sm-6">
		    	
		    </div>
		    
		    
		</div>
		<div class="row">
	    	<button type="submit" class="btn btn-success">Submit</button>
	    </div>
  	</form>
</div>

<?php 

$this->load->view('admin/layouts/footer');
?>
	
<?php
$this->load->view('admin/layouts/footer_end');

?>
<script type="text/javascript">
		$(document).ready(function() {
		    $('.select_category').on('change',function(){
				var category_val = $(this).find("option:selected").data("name").toLowerCase();
				$('.category_html').append('<input type="text" class="form-control col-sm-6" value="'+category_val+"_"+'" name="cat_value" Placeholder="category name_action"/> &ensp;<button type="button" class="btn btn-danger remove_category_html">-</button><br><br>');

				$('.remove_category_html').on('click',function(){
					$(this).parent().remove();
				});

			});

		});
		

</script>