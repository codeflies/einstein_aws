<?php $this->load->view('admin/layouts/header');?>

<div class="container">
  	<h2><b>Add Role</b></h2>
  	<form action="<?php echo base_url('admin/role/create');?>" method="post">
		<div class="row">
		    <div class="form-group col-sm-6" >
		      <label for="name">Name:</label>
			  <input type="text" class="form-control" name="name" value="<?php $data->name;?>" placeholder="Please Enter The Name">
		    </div>
		    
		</div>
		    <button type="submit" class="btn btn-success">Submit</button>
  	</form>
</div>

<?php $this->load->view('admin/layouts/footer');?> 