<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login Registration</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/auth_login.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/fontawesome-free/css/all.min.css">


</head>
<body>
    <div class="container"><!-- container class is used to centered  the body of the browser with some decent width-->
        <div class="row"><!-- row class is used for grid system in Bootstrap-->
            <div class="login-form mx-auto">
                <div class="login-logo">
                    <a href="index.html">
                        <img src="http://54.172.16.189/LMS/dist/img/em-logo.png" alt="Logo" class="brand-image">
                    </a>
                </div>
                <div class="login-panel">
                    <div class="login-form-box">
                        <div class="panel-heading">
                            <h3 class="panel-title">Admin Registration</h3>
                        </div>
                        <div class="panel-body">

                            <?php
                                $error_msg=$this->session->flashdata('error_msg');
                                if($error_msg){
                                    echo $error_msg;
                                }
                            ?>

                            <form role="form" method="post" action="<?php echo base_url('admin/register'); ?>">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Please Enter Name" name="name" type="text">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Please Enter E-mail" name="email" type="email">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-envelope"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Enter Password" name="password" type="password" value="">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <input class="btn btn-lg btn-info btn-block" type="submit" value="Register" name="register" >
                                </fieldset>
                            </form>
                            <center><b>Already registered?</b> <a href="<?php echo base_url('admin/login'); ?>"> Please Login</a></center><!--for centered text-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</body>
</html>