<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Login-CI Login Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/css/auth_login.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/fontawesome-free/css/all.min.css">
</head>
<body class="login-register-page">

    <div class="container">
        <div class="row">
            <div class="login-form mx-auto">
                <div class="login-logo">
                    <a href="index.html">
                        <img src="http://54.172.16.189/LMS/dist/img/em-logo.png" alt="Logo" class="brand-image">
                    </a>
                </div>
                <div class="login-panel">
                    <div class="login-form-box">
                        <div class="panel-heading">
                            <h3 class="panel-title">Admin Login</h3>
                        </div>
                        <?php
                            $success_msg= $this->session->flashdata('success_msg');
                            $error_msg= $this->session->flashdata('error_msg');

                            if($success_msg){
                        ?>
                        <div class="alert alert-success">
                            <?php echo $success_msg; ?>
                        </div>
                        <?php
                            }
                            if($error_msg){
                        ?>
                        <div class="alert alert-danger">
                            <?php echo $error_msg; ?>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="panel-body">
                            <form role="form" method="post" action="<?php echo base_url('admin/login'); ?>">
                                <fieldset>
                                    <div class="form-group"  >
                                        <input class="form-control" placeholder="Enter E-mail" name="email" type="email">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-envelope"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Enter Password" name="password" type="password" value="">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-lock"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="icheck-primary">
                                                <input type="checkbox" name="remember_me" value="true" id="remember">
                                                <label for="remember">
                                                    Remember Me
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="mb-1 text-right">
                                                <a href="http://54.172.16.189/LMS/user/forgot_password">Forgot Password?</a>
                                            </p>
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                    </div>

                                    <input class="btn btn-lg btn-info btn-block" type="submit" value="Login" name="login" >
                                </fieldset>
                            </form>
                            <!-- <center><b>You are not registered ?</b> <br></b><a href="<?php echo base_url(); ?>admin/register">Register here</a></center> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>