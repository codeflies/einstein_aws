<?php

$this->load->view('admin/layouts/header');
$this->load->view('admin/layouts/sidebar');

?>

<div class="container">
  	<h2><b>Add Role</b></h2>
  	<form action="<?php echo base_url('admin/role/create');?>" method="post">
		<div class="row">
		    <div class="form-group col-sm-6" >
		      <label for="name">Name:</label>
			  <input type="text" class="form-control" name="name" placeholder="Please Enter The Name">
		    </div>
		    <div class="form-group col-sm-6" >
		      <label for="is_active">Active</label>

		      <select id="is_active" class="form-control" name="is_active">
				    <option>Please select</option>
				    <option value="Y">Active</option>
				    <option value="N">Inactive</option>
			  </select>
		    </div>
		</div>
		    <button type="submit" class="btn btn-success">Submit</button>
  	</form>
</div>

<?php 

$this->load->view('admin/layouts/footer');

$this->load->view('admin/layouts/footer_end');

?> 