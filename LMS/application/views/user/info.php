<?php 
  $this->load->view('include/header2');
 ?>  
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Users Info</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content userinfo-content">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $userData[0]->full_name; ?></h3>
          </div>
          <!-- /.card-header -->
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('user/info/'.$user_id) ?>">Info</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('user/course/'.$user_id); ?>">Courses</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('user/group/'.$user_id); ?>">Groups</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('user/branches/'.$user_id); ?>">Branches</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('user/files/'.$user_id); ?>">Files</a></li>
                </ul>
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="#" class="btn btn-primary">Profile</a>
                      <a href="#" class="btn btn-default">Progress</a>
                      <a href="#" class="btn btn-default">Infographic</a>
                    </div>
                  </li>
                </ul>
              </nav>
             </div> 
          <?php 
                $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_AddUser');
                echo form_open(base_url().'user/update', $attributes); 
              ?>
                <input type="hidden" name="update_user" value="UpdateUser">
                <input type="hidden" name="user_id" value="<?php echo $userData[0]->id; ?>">
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="fp-name">First Name</label>
                          <input type="text" class="form-control" name="first_name" value="<?php echo $userData[0]->first_name; ?>" placeholder="First Name" />
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('first_name'); ?>  
                        </div>    
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="lp-name">Last Name</label>
                          <input type="text" class="form-control" value="<?php echo $userData[0]->last_name; ?>" name="last_name" placeholder="Last Name" />
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('last_name'); ?>  
                        </div>    
                      </div>
                      <!-- /.form-group -->
                    </div>

                    <div class="form-group">
                      <label for="">Email address</label>
                      <input type="email" class="form-control" value="<?php echo $userData[0]->email; ?>" name="email"  placeholder="Enter email">
                    </div>
                    <div class="invalid-feedback">
                        <?php echo form_error('email'); ?>  
                    </div>    
                    <!-- /.form-group -->
                    <div class="form-group">
                      <label for="">Profile Pic</label>
                      <?php if(isset($userData[0]->profile_pic)){ ?>
                        <div class="image">
                            <img src="<?php echo base_url('uploads/users/profile/'.$userData[0]->profile_pic);?>"
                                class="img-responsive zoom-img" alt="" width="50%">
                        </div>
                      <?php } ?>
                      <input type="file" class="form-control" onchange="imagesPreview(this, 'div.gallery')"   name="profile_pic"  placeholder="Choose Image" accept="image/*">
                      <input type="hidden" name="edit_image" value="<?php echo $userData[0]->profile_pic;?>">
                    </div>
                    <div class="invalid-feedback">
                        <?php echo form_error('profile_pic'); ?>  
                    </div>
                    <div class="col-6 gallery">

                    </div>
                    <!-- <div id="actions" class="row">
                      <div class="col-lg-6">
                        <label for="exampleInputFile">Upload Profile Pic</label>
                        <div class="btn-group w-100">
                          <span class="btn btn-success col fileinput-button">
                            <i class="fas fa-plus"></i>
                            <span>Add files</span>
                          </span>
                          <button type="submit" class="btn btn-primary col start">
                            <i class="fas fa-upload"></i>
                            <span>Start upload</span>
                          </button>
                          <button type="reset" class="btn btn-warning col cancel">
                            <i class="fas fa-times-circle"></i>
                            <span>Cancel upload</span>
                          </button>
                        </div>
                      </div>
                      <div class="col-lg-6 d-flex align-items-center">
                        <div class="fileupload-process w-100">
                          <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="table table-striped files" id="previews">
                      <div id="template" class="row mt-2">
                        <div class="col-auto">
                            <span class="preview"><img src="data:," alt="" data-dz-thumbnail /></span>
                        </div>
                        <div class="col d-flex align-items-center">
                            <p class="mb-0">
                              <span class="lead" data-dz-name></span>
                              (<span data-dz-size></span>)
                            </p>
                            <strong class="error text-danger" data-dz-errormessage></strong>
                        </div>
                        <div class="col-4 d-flex align-items-center">
                            <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                          <div class="btn-group">
                            <button class="btn btn-primary start">
                              <i class="fas fa-upload"></i>
                              <span>Start</span>
                            </button>
                            <button data-dz-remove class="btn btn-warning cancel">
                              <i class="fas fa-times-circle"></i>
                              <span>Cancel</span>
                            </button>
                            <button data-dz-remove class="btn btn-danger delete">
                              <i class="fas fa-trash"></i>
                              <span>Delete</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <!-- /.form-group -->

                    <div class="dropdown-divider mt-2"></div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="user-name">Username</label>
                          <input type="text" class="form-control" value="<?php echo $userData[0]->username; ?>" name="username"   placeholder="User Name" />
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('username'); ?>  
                        </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="">Password</label>
                          <input type="password"   name="password" class="form-control"  placeholder="Blank to leave unchanged">
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('password'); ?>  
                        </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    
                    <div class="dropdown-divider pd-5"></div>
                    <div class="form-group">
                      <label for="Bio">Bio</label>
                      <textarea class="form-control" name="bio"><?php echo $userData[0]->bio; ?></textarea>
                    </div>
                    <div class="invalid-feedback">
                      <?php echo form_error('bio'); ?>  
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                      <label>User type</label>
                      <select class="form-control select2bs4" name="user_type" style="width: 100%;">
                        <option selected="selected">User Type</option>
                        <?php
                        if(!empty($usertype)){ 
                          foreach ($usertype as $key => $type_value) { 
                            ?>
                            <option value="<?php echo $type_value->id; ?>" <?php if($type_value->id==$userData[0]->user_type){echo "Selected";}?>><?php echo $type_value->name; ?></option>
                          <?php }
                         }
                        ?>
                      </select>
                    </div>
                    <div class="invalid-feedback">
                      <?php echo form_error('user_type'); ?>  
                    </div>
                    <!-- /.form-group -->
                    <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label>Time Zone</label>
                        <div class="input-group date" id="timepicker" data-target-input="nearest">
                              <select name="timezone" value="<?php echo $userData[0]->timezone; ?>" class="form-control select2bs4"   style="width: 100%;" id="">
                                <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-12">(GMT-12:00) International Date Line West</option>
                                <option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>
                                <option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="-10">(GMT-10:00) Hawaii</option>
                                <option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="-9">(GMT-09:00) Alaska</option>
                                <option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Pacific Time (US & Canada)</option>
                                <option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>
                                <option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="-7">(GMT-07:00) Arizona</option>
                                <option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                <option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Mountain Time (US & Canada)</option>
                                <option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Central America</option>
                                <option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Central Time (US & Canada)</option>
                                <option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                <option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Saskatchewan</option>
                                <option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                <option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Eastern Time (US & Canada)</option>
                                <option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Indiana (East)</option>
                                <option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
                                <option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>
                                <option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Manaus</option>
                                <option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Santiago</option>
                                <option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="-3.5">(GMT-03:30) Newfoundland</option>
                                <option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Brasilia</option>
                                <option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
                                <option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Greenland</option>
                                <option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Montevideo</option>
                                <option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>
                                <option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>
                                <option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="-1">(GMT-01:00) Azores</option>
                                <option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                <option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                <option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                <option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                <option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                <option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                <option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) West Central Africa</option>
                                <option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Amman</option>
                                <option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                <option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Beirut</option>
                                <option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Cairo</option>
                                <option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="2">(GMT+02:00) Harare, Pretoria</option>
                                <option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                <option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Jerusalem</option>
                                <option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Minsk</option>
                                <option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Windhoek</option>
                                <option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                <option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                <option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Nairobi</option>
                                <option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Tbilisi</option>
                                <option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="3.5">(GMT+03:30) Tehran</option>
                                <option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
                                <option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Baku</option>
                                <option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Yerevan</option>
                                <option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="4.5">(GMT+04:30) Kabul</option>
                                <option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="5">(GMT+05:00) Yekaterinburg</option>
                                <option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                <option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
                                <option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                <option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="5.75">(GMT+05:45) Kathmandu</option>
                                <option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>
                                <option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="6">(GMT+06:00) Astana, Dhaka</option>
                                <option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
                                <option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                <option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="7">(GMT+07:00) Krasnoyarsk</option>
                                <option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                <option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                <option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                <option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Perth</option>
                                <option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Taipei</option>
                                <option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                <option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Seoul</option>
                                <option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="9">(GMT+09:00) Yakutsk</option>
                                <option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Adelaide</option>
                                <option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Darwin</option>
                                <option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Brisbane</option>
                                <option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                <option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Hobart</option>
                                <option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>
                                <option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Vladivostok</option>
                                <option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                <option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="12">(GMT+12:00) Auckland, Wellington</option>
                                <option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                <option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="13">(GMT+13:00) Nuku'alofa</option>
                              </select>
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('timezone'); ?>  
                        </div>
                      </div>
                    </div>
                    <!-- /.form-group -->
                    <div class="invalid-feedback">
                      <?php echo form_error('language'); ?>  
                    </div>
                    <!-- /.form-group -->
                    <div class="form-check">
                      <input type="checkbox" name="active" <?php
                        if ( $userData[0]->active == 'true') {
                          echo "checked";
                        }
                       ?> value="true" class="form-check-input" id="user-active">
                      <label class="form-check-label" for="user-active">Active</label>
                    </div>
                    <div class="invalid-feedback">
                      <?php echo form_error('active'); ?>  
                    </div>
                    <!-- /.form-group -->
                    <!-- <div class="form-check">
                      <input type="checkbox" name="deactivated" class="form-check-input" id="user-deactive">
                      <label class="form-check-label" for="user-deactive">Deactivate at ...</label>
                    </div> -->
                    <div class="form-check DateCheckbox">
                      <input type="checkbox" name="deactivated" class="form-check-input" id="user-deactive" onclick="checkbox()">
                      <label class="form-check-label" for="user-deactive">Deactivate at ...</label>
                      <div class="form-group col-sm-3 mt-2"style="display: none;" id="Datebox">
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                          <input type="text" name="deactivated_date" class="form-control datetimepicker-input" data-target="#reservationdate" />
                          <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-check">
                      <input type="checkbox" name="exclude_mail" value="true" class="form-check-input" id="exclude-mail">
                      <label class="form-check-label" for="exclude-mail">Exclude from emails</label>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update User</button> <span>or</span> <a href="<?php echo base_url(); ?>user/list">Cancel</a>
                  </div>
              <?php echo form_close(); ?>
        </div>

            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php 
  $this->load->view('include/footer2');
 ?>  