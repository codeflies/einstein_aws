<?php $this->load->view('include/header2');?> 

<style type="text/css">
  thead tr{border-bottom: 2px solid #ccc;}
  .fixed-table-pagination .float-left{float: right !important;}
  .refresh-btn{padding: .375rem .75rem;font-size: 1rem;}
  .refresh-btn i{margin-left: 10px;}
</style>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">User Listing</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content user-content">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header ">
                <h3 class="card-title">User</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-tbl-head">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="btn-group">
                        <a href="create" class="btn btn-primary">Add User</a>
                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu" style="">
                          <a class="dropdown-item" href="#">Import User(s)</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 text-right">
                      <div id="userListToolbar">
                        <button  type="button" class="btn btn-primary btn-sm refresh-btn" >Refresh <i class="fas fa-sync"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="dropdown-divider"></div>
                <div class="table-responsive">
                <!-- Refresh button -->
               
                <div>
                  <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search" id="userListSearch" autocomplete="off">
                </div>
                <table
                    id="userListTable"
                    data-search="true"
                    data-show-export="true"
                    data-visible-search="true"
                    data-ajax-options="userListAjax"
                    data-show-columns="true"
                    data-show-columns-search="true"
                    data-search-selector="#userListSearch"
                    data-buttons-prefix="btn-sm btn btn-success"
                    data-pagination="true"
                    data-side-pagination="server"
                    data-server-sort="true"
                    data-show-toggle="true"
                    class="table-borderless user-table table-hover fonts_size font_family"
                    >
                    <thead>
                    </thead>
                </table>
                </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php $this->load->view('include/footer2');?> 
<!-- page specific js -->
<script>
  var $userListTable = $('#userListTable');

   // Row action function
  function rowAction(value, row, index) {
    var html = `<i class="fa fa-ellipsis-h"></i>
                <div class="hover-tbl-btn">
                    <a href="#" class="tbl-btn tbl-signal" title="Reports"><i class="fas fa-signal"></i></a>
                    <a href="" class="tbl-btn tbl-logout" title="Log into account"><i class="fas fa-sign-out-alt"></i></a>
                    <a href="info/${row.id}" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                    <a href="delete/${row.id}" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                </div>`;         
    return[
      html
    ].join("");
  }


  // EXPORT TO CSV OR PDF CODE BEGINS HERE
  $(function() {
      $userListTable.bootstrapTable('destroy').bootstrapTable({
              url:'datalist',
              showFullscreen: true,
              exportDataType: $(this).val(),
              exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
                  columns: [
                      {
                          field: 'state',
                          checkbox: true,
                          visible: $(this).val() === 'selected'
                      },
                      {
                          field: 'full_name',
                          title: 'USER',
                          sortable:true
                      },
                      {
                          field: 'email',
                          title: 'EMAIL',
                          sortable: true
                      },
                      {
                          field: 'user_type',
                          title: 'USER TYPE',
                          sortable: true
                      },
                      {
                          field: 'registration_date',
                          title: 'REGISTRATION',
                          sortable: true
                      },
                      {
                          field: 'last_login',
                          title: 'LAST LOGIN',
                          sortable: true
                      },
                      {
                          field: 'option',
                          title: 'OPTIONS',
                          formatter: rowAction
                      }
                  ]
          })
      userListToolbar.onclick = () => {
          // startLoader();
          $userListTable.bootstrapTable('refresh');
      }
  })
  window.userListAjax = {
      complete: function (xhr) {
        // stopLoader();
      }
  }
</script>