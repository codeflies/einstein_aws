<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/local.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/snackbar.css">
</head>
<body class="hold-transition login-page">
    <div id="snackbar"></div>
    <div class="login-box step-form">
        <div class="login-logo">
            <a href="index.html">
                <img src="<?php echo base_url(); ?>dist/img/em-logo.png" alt="Logo" class="brand-image">
            </a>
        </div>

       <?php 
        $attributes = array('id' => 'welcome_form');
        echo form_open(base_url()."user/register", $attributes); ?>
        <input type="hidden" name="welcome_form" value="welcomeForm">
        <div class="card section card-sec-1 active">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Welcome, <?php echo $this->session->userdata["domain_name"]; ?></p>
                    <h5>What's your main goal with TalentLMS?</h5>
                    <div class="row">
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Training Employees" id="employees">
                                <label for="employees">
                                    Training Employees
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Training Customers" id="customers">
                                <label for="customers">
                                    Training Customers
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Training Partners" id="partners">
                                <label for="partners">
                                    Training Partners
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Selling Courses" id="courses">
                                <label for="courses">
                                    Selling Courses
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Providing Training Services" id="services">
                                <label for="services">
                                    Providing training services
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Teaching Students" id="students">
                                <label for="students">
                                    Teaching students
                                </label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" name="goal[]" value="Other" id="other">
                                <label for="other">
                                    Other
                                </label>
                            </div>
                        </div>
                    </div>
                    <button type="button" class=" next-section-btn btn btn-primary mt-2 mb-3">Next</button>

            </div>
            <!-- /.login-card-body -->
        </div>

        <div class="card section card-sec-2">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Welcome, <?php echo $this->session->userdata["domain_name"]; ?></p>
                    <div class="form-group">
                        <label>How many people do you want to train?</label>
                        <select class="custom-select" name="no_of_people" required>
                            <option value="1-10">1 - 10</option>
                            <option value="10-50">10 - 50</option>
                            <option value="50-100">50 - 100</option>
                            <option value="100-500">100 - 500</option>
                            <option value="500-1000">500 - 1000</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>What does your company do?</label>
                        <select class="custom-select" required name="what_company_do">
                            <option value="Agriculture">Agriculture</option>
                            <option value="Automotive">Automotive </option>
                            <option value="Aviation">Aviation</option>
                            <option value="Computer Software">Computer Software</option>
                            <option value="Construction">Construction</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Select your department</label>
                        <select class="custom-select" name="department">
                            <option value="Accounting">Accounting</option>
                            <option value="Administrative">Administrative </option>
                            <option value="Business Development">Business Development</option>
                            <option value="Consultant">Consultant</option>
                            <option value="Design">Design</option>
                        </select>
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-primary mt-2 mb-3">Submit</button>
                    </p>
                
            </div>
            <!-- /.login-card-body -->
        </div>
        <?php echo form_close(); ?> 
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>dist/js/adminlte.min.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/local.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/main.js"></script>

    <script>
        
            $('.next-section-btn').click(function () {
                $(this).parents('.section.active').next().addClass('active');
                $(this).parents('.section.active').removeClass('active');
            });
          

            $("#welcome_form").submit(function(event) {
                event.preventDefault();
                // Form Data
                let myForm = document.getElementById('welcome_form');
                let formData = new FormData(myForm);
                // // URL
                var url = $(this).attr("action");
                // validation
                var goal_value = $('[name="goal[]"]:checked').length;
                if (goal_value < 3) {
                    snackbar("Please select atleast three main goals.");
                } else {
                    // Ajax request
                    var response = CommonAjax(url, 'POST', formData);
                    if (response !== false) {
                        // Displaying validation errors
                        var err = response.error;   
                        for (var key in err) {
                            if (err.hasOwnProperty(key)) {
                                $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
                                $('.invalid-feedback').css({
                                    display: 'block'
                                });
                            }
                        }
                        // Handling response
                        if(response.success === true) {
                           document.location.href = response.target;
                        }
                        snackbar(response.message);
                    } else {
                        snackbar("Some error occured.");
                    }
                }
            });
    </script>


</body>
</html> 
