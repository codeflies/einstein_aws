 
<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Course Store</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Course Store</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content library-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Course Store</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>marketplace/List">Einstein's Mind Library</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>marketplace/Create">Other course providers</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="modal" data-target="#modal-marketplace"><i class="fa fa-info-circle"></i>&nbsp; How the Course Store works</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="tab-content">
                <div class="tab-pane fade show active">
                  <div class="library-content">
                    <div class="row">
                      <div class="col-sm-9">
                        <div class="library-head">
                          <div class="row">
                            <div class="col-6 col-sm-4">
                              <div class="form-group">
                                 <input type="search" name="" placeholder="Search courses" class="form-control">
                              </div> 
                            </div>
                            <div class="col-6 col-sm-8 text-right">
                              <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <span class=""><i class="fa fa-bars"></i> Name</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                  <a class="dropdown-item" href="#">Order</a>
                                  <a class="dropdown-item" href="#">Name</a>
                                  <a class="dropdown-item" href="#">Date</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="library-body">
                          <div class="library-box">
                            <div class="col-lft">
                              <div class="library-box-img">
                                <a href="#"><img class="tl-marketplace-course-avatar-image avatar-square tl-course-avatar" src="https://d3j0t7vrtr92dk.cloudfront.net/iamtalentlms/1600090969_The%207%20Skills%20of%20Critical%20Thinking%20-%20LMS%20Thumb.png?" alt="The" title="The"></a>
                              </div>
                              <button class="btn btn-default btn-sm disabled">Access TalentLibrary</button>
                            </div>
                            <div class="col-rgt">
                              <div class="library-categories"><span>Business Innovation</span></div>
                              <h4 class="library-name"><a href="#">The 7 Skills of Critical Thinking <small class="course-code">&nbsp;(BI001)</small></a></h4>
                              <p class="library-description">The philosophies of history’s great thinkers have been the starting point for much of the innovation we take for granted now. Without them, we might never have had cell phones, computers, or even electricity! See, critical thinking is the...</p>
                            </div>
                          </div>
                          <!-- end -->
                          <div class="library-box">
                            <div class="col-lft">
                              <div class="library-box-img">
                                <a href="#"><img class="tl-marketplace-course-avatar-image avatar-square tl-course-avatar" src="https://d3j0t7vrtr92dk.cloudfront.net/iamtalentlms/1600090969_The%207%20Skills%20of%20Critical%20Thinking%20-%20LMS%20Thumb.png?" alt="The" title="The"></a>
                              </div>
                              <button class="btn btn-default btn-sm disabled">Access TalentLibrary</button>
                            </div>
                            <div class="col-rgt">
                              <div class="library-categories"><span>Business Innovation</span></div>
                              <h4 class="library-name"><a href="#">The 7 Skills of Critical Thinking <small class="course-code">&nbsp;(BI001)</small></a></h4>
                              <p class="library-description">The philosophies of history’s great thinkers have been the starting point for much of the innovation we take for granted now. Without them, we might never have had cell phones, computers, or even electricity! See, critical thinking is the...</p>
                            </div>
                          </div>
                          <!-- end -->
                        </div>
                        <div class="library-foot"></div>
                      </div>
                      <!-- left end -->
                      <div class="col-sm-3">
                        <div class="sidebar-collection">
                          <h4>COLLECTIONS</h4>
                          <ul class="collection-list">
                            <li>
                              <div class="icheck-primary d-inline">
                                <input type="checkbox" id="check1">
                                <label for="check1">Business Innovation  <span class="courses-count">(10)</span></label>
                              </div>
                            </li>
                            <li>
                              <div class="icheck-primary d-inline">
                                <input type="checkbox" id="check2">
                                <label for="check2">Business Innovation  <span class="courses-count">(10)</span></label>
                              </div>
                            </li>
                            <li>
                              <div class="icheck-primary d-inline">
                                <input type="checkbox" id="check3">
                                <label for="check3">Business Innovation  <span class="courses-count">(10)</span></label>
                              </div>
                            </li>
                            <li>
                              <div class="icheck-primary d-inline">
                                <input type="checkbox" id="check4">
                                <label for="check4">Business Innovation  <span class="courses-count">(10)</span></label>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- right end -->
                    </div>
                  </div>
                </div>
                <!-- // Tab 1 End -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->

          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>