<?php $this->load->view('include/header2');?>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Category Listing</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content category-content">
      <div class="container">
        <div class="row">
          <div class="col-12">

            <div class="card card-primary">
              <div class="card-header ">
                <h3 class="card-title">Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <a href="<?php echo base_url(); ?>Category/create" class="btn btn-primary">Add Category</a>
                  </div>
                  <div class="col-6 text-right">
                    <a href="#"><i class="fa fa-share"></i> View course catalog</a>
                  </div>
                </div>
                <div class="dropdown-divider"></div>


                <div class="table-responsive">
                    <!-- Refresh button -->
                   <div>
                  <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search" id="categoryListSearch" autocomplete="off">
                </div>
                   <table
                    id="categoryListTable"
                    data-search="true"
                    data-visible-search="true"
                    data-ajax-options="categoryListAjax"
                    data-show-columns="true"
                    data-show-columns-search="true"
                    data-show-export="true"
                    data-search-selector="#categoryListSearch"
                    data-buttons-prefix="btn-sm btn btn-success"
                    data-pagination="true"
                    data-side-pagination="client"
                    data-server-sort="true"
                    class="table-borderless user-table table-hover fonts_size font_family"
                    >
                    <thead>
                    </thead>
                </table>
                </tbody>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<!-- Modal -->
<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this record?
      </div>
      <div class="modal-footer">
        <?php 
          $attributes = array('enctype' => 'multipart/form-data', 'id' => 'DeleteCategory');
          echo form_open(base_url().'category/delete', $attributes); 
        ?>

          <input type="hidden" name="deleteid" id="delbtn" />
          <button type="submit" class="btn btn-danger" onclick="ColseModal()">Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div> 
</div>
  <?php $this->load->view('include/footer2');?>
<script>
  var $categoryListTable = $('#categoryListTable');

   // Row action function
  function rowAction(value, row, index) {
    var html = `<i class="fa fa-ellipsis-h"></i>
                <div class="hover-tbl-btn">
                <a href="edit/${row.id}" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                <a  onclick="DeleteCategory(${row.id})" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
            </div>`;         
    return[
      html
    ].join("");
  }


  // EXPORT TO CSV OR PDF CODE BEGINS HERE
  $(function() {
      $categoryListTable.bootstrapTable('destroy').bootstrapTable({
              url:'datalist',
              showFullscreen: true,
              exportDataType: $(this).val(),
              exportTypes: [ 'csv', 'txt','excel', 'pdf'],
                  columns: [
                      {
                          field: 'state',
                          checkbox: true,
                          visible: $(this).val() === 'selected'
                      },
                      {
                          field: 'name',
                          title: 'Name',
                          sortable:true
                      },
                      {
                          field: 'option',
                          title: 'OPTIONS',
                          formatter: rowAction
                      }
                  ]
          })
      categoryListTable.onclick = () => {
          // startLoader();
          $categoryListTable.bootstrapTable('refresh');
      }
  })
  window.categoryListAjax = {
      complete: function (xhr) {
        // stopLoader();
      }
  }

   // code to read selected table row cell data (values).
  $("#categoryListTable").on('click','.btnSelect',function(){

    var currentRow=$(this).closest("tr");
    var col=currentRow.find("td:eq(0)").text(); 

    var course_id = $(this).data('id');
    var count = $(this).attr('data-count_'+category_id);
    if(count == 0){
      var one_include = parseInt(count)+1;
      $(this).attr('data-count'+'_'+course_id,one_include);
      var clone_name = col+one_include;
      AjaxClone(category_id, col, clone_name);
    }
    else{
      var last_value = parseInt(col.slice(-1))+parseInt(count);
      var one_include = parseInt(count)+1;
      $(this).attr('data-count'+'_'+category_id,one_include);
      var clone_name = col+last_value;
      AjaxClone(category_id, col, clone_name);
    } 
  });
</script>
