<?php
  function fetch_menu($parent_category,$parent_id){
    foreach($parent_category as $menu){
    if($menu->id == $parent_id){
        $selected = "selected";
      }
      else{
        $selected = "";
      }
    echo "<option value='".$menu->id."' ".$selected .">".$menu->name."</option>";
      if(!empty($menu->sub)){
        $space="&nbsp;&nbsp;&nbsp;&nbsp;";
        $menu_id = $menu->id;
        fetch_sub_menu($menu->sub,$space,$parent_id);
      }
    }
  }

  function fetch_sub_menu($sub_menu,$space,$parent_id){
    
    foreach($sub_menu as $menu1){
      if($menu1->id == $parent_id){
        $selected = "selected";
      }
      else{
        $selected = "";
      }
      echo "<option value='".$menu1->id."' ".$selected .">".$space.$menu1->name."</option>";
      if(!empty($menu1->sub)){
        $space.="&nbsp;&nbsp;&nbsp;&nbsp";
        $menu_id = $menu1->id;
        fetch_sub_menu($menu1->sub,$space,$parent_id);
      }
    }
  }
  ?>

<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Category</h3>
              </div>
            
                 <?php 
                  $attributes = array('enctype' => 'multipart/form-data', 'id' => 'CourseCategoryAdd');
               echo form_open(base_url().'category/update', $attributes); 
              ?>
          
              <input type="hidden" name="update_category" value="UpdateCategory" >
               <input type="hidden" name="user_id" value="<?php echo $CategoryData[0]->id; ?>">
                <div class="card-body">

                  <div class="invalid-feedback">
                      <p><?php echo $this->session->flashdata('message'); ?></p>
                    </div>
                  <div class="">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="ct-name">Name</label>

                          
                          <input type="text" class="form-control" name="name" id="" value="<?php echo $CategoryData[0]->name; ?>"  placeholder="e.g Accounting" />
                        </div>

                        <div class="invalid-feedback">
                          <?php echo form_error('name'); ?>  
                        </div> 
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Parent Category</label>
                          <?php 
                          $parent_id = $CategoryData[0]->parent_categories;
                          ?>
                          <?php 
                               echo '<select class="form-control select2bs4" name="parent_categories">';
                               echo '<option>Select a Parent Category</option>';
                               echo fetch_menu($parent_category,$parent_id);
                               echo '</select>';
                          ?>
                          <!-- <select class="form-control select2bs4" name="parent_categories" >
                            <option selected="selected" >Select a Parent Category</option>
                              <?php //foreach($parent_category as $row):?>
                                
                                <option value="<?php //echo $row->name;?>" >
                                    <?php //echo $row->name;?>
                                </option>
                              <?php //endforeach;?>
                          </select> -->
                        </div>
                        
                        <div class="invalid-feedback">
                      <?php echo form_error('parent_categories'); ?>  
                    </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="ct-price">Price</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fas fa-dollar-sign"></i>
                                </span>
                              </div>
                              <input type="text"  name="price" id="" class="form-control" value="<?php echo $CategoryData[0]->price; ?>" >
                            </div>
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('price'); ?>  
                    </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary">Update Category</button>
                </div>
           
            <?php echo form_close(); ?>

            </div>
            
            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
    <?php $this->load->view('include/footer2');?>