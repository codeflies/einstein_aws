<?php
function fetch_menu($category){
  foreach($category as $menu){
  echo "<option value=".$menu->id.">".$menu->name."</option>";
    if(!empty($menu->sub)){
      $space="&nbsp;&nbsp;&nbsp;&nbsp;";
      fetch_sub_menu($menu->sub,$space);
    }
  }
}

function fetch_sub_menu($sub_menu,$space){
   foreach($sub_menu as $menu1){
      echo "<option value=".$menu1->id.">".$space.$menu1->name."</option>";
    if(!empty($menu1->sub)){
      $space.="&nbsp;&nbsp;&nbsp;&nbsp";
      fetch_sub_menu($menu1->sub,$space);
    }
  }  
}

$this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <!-- <form> -->
                 <?php 
                  $attributes = array('id' => 'CourseCategoryAdd');
                  echo form_open(base_url().'category/create', $attributes); 
              ?>
          
              <input type="hidden" name="add_category" value="AddCategory" >
                <div class="card-body">

                  <div class="invalid-feedback">
                      <p><?php echo $this->session->flashdata('message'); ?></p>
                    </div>
                  <div class="">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="ct-name">Name</label>
                          <input type="text" class="form-control" name="name" id=""  placeholder="e.g Accounting" />
                        </div>

                        <div class="invalid-feedback">
                          <?php echo form_error('name'); ?>  
                        </div> 
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Parent Category</label>
                          <?php 
                            echo '<select class="form-control select2bs4" name="parent_categories">';
                            echo '<option selected="selected">Select a Parent Category</option>';
                            echo fetch_menu($category);
                            echo '</select>';
                          ?>
                          <!-- <select class="form-control select2bs4" name="parent_categories">
                          <option selected="selected">Select a Parent Category</option>
                              <?php //foreach($category as $row):?>
                                <option value="<?php //echo $row->id;?>"><?php //echo $row->name;?></option>
                                <?php //foreach($row->sub as $category):?>
                                  <option value="<?php //echo $category->id;?>"><?php //echo ">>  :".$category->name;?></option>
                                <?php //endforeach;?>
                              <?php //endforeach;?>
                          </select> -->
                        </div>

                        <div class="invalid-feedback">
                      <?php echo form_error('parent_categories'); ?>  
                    </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="ct-price">Price</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fas fa-dollar-sign"></i>
                                </span>
                              </div>
                              <input type="text"  name="price" id="" class="form-control">
                            </div>
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('price'); ?>  
                    </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add Category</button> <span>or</span> <a href="<?php echo base_url(); ?>/Categorylist">Cancel</a>
                </div>
                 
            <!--   </form> -->
           
            <?php echo form_close(); ?>

            </div>
            
            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   
    <?php $this->load->view('include/footer2');?>