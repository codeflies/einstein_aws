<?php $this->load->view('include/header2');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Group</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Groups</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Group</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form> -->
                <?php 
                  $attributes = array('enctype' => 'multipart/form-data', 'id' => 'GroupAdd');
                  echo form_open(base_url().'group/create', $attributes); 
                ?>
                <input type="hidden" name="add_group" value="add_group">
                <div class="card-body">
                  <div class="">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="ct-name">Name</label>
                          <input type="text" class="form-control" name="name"  placeholder="e.g. Startup courses" />
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('name'); ?>  
                        </div> 
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="gp-price">Price</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fas fa-dollar-sign"></i>
                                </span>
                              </div>
                              <input type="text"  name="price" class="form-control">
                            </div>
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('price'); ?>  
                        </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="gp-key"><i class="fa fa-lock"></i> Group Key</label>
                          <input type="text" readonly class="form-control" 
                             name="group_key"  value="<?php echo $key ?>" />
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('group_key'); ?>  
                    </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="description">Description</label>
                          <textarea class="form-control"  name="description" placeholder="Short description up to 500 characters"></textarea>
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('description'); ?>  
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add Group</button> <span>or</span> <a href="<?php echo base_url(); ?>/Grouplist">Cancel</a>
                </div>
              <!-- </form> -->
                <?php echo form_close(); ?>
            </div>
            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>  
<?php $this->load->view('include/footer2');?>
