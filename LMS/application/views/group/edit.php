<?php $this->load->view('include/header2');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Group</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Groups</li>
            </ol>
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Group</h3>
              </div>
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('group/edit/'.$group_id) ?>">Info</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/users/'.$group_id) ?>">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/course/'.$group_id) ?>">Courses</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/files/'.$group_id) ?>">Files</a></li>
                </ul>
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="#" class="btn btn-primary">Profile</a>
                      <a href="#" class="btn btn-default">Progress</a>
                      <a href="#" class="btn btn-default">Infographic</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <!-- /.card-header -->
              <!-- form start -->
             <!--  <form> -->
                <?php 
                  $attributes = array('enctype' => 'multipart/form-data', 'id' => 'GroupAdd');
                  echo form_open(base_url().'group/update', $attributes); 
                ?>
                <input type="hidden" name="update_group" value="update_group">
                 <input type="hidden" name="user_id" value="<?php echo $GroupData[0]->id; ?>" >
                <div class="card-body">
                  <div class="">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="ct-name">Name</label>
                          <input type="text" class="form-control" name="name" value="<?php echo $GroupData[0]->name; ?>" id="" placeholder="e.g. Startup courses" />
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('name'); ?>  
                        </div> 
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="gp-price">Price</label>
                          <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fas fa-dollar-sign"></i>
                                </span>
                              </div>
                              <input type="text" value="<?php echo $GroupData[0]->price; ?>" name="price" class="form-control">
                            </div>
                        </div>
                        <div class="invalid-feedback">
                          <?php echo form_error('price'); ?>  
                        </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="gp-key"><i class="fa fa-lock"></i> Group Key</label>
                          <input type="text" readonly class="form-control" value="<?php echo $GroupData[0]->group_key; ?>"  name="group_key" id="group_key"/>
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('group_key'); ?>  
                    </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="description">Description</label>
                          <textarea class="form-control"  name="description" id="" placeholder="Short description up to 500 characters"><?php echo $GroupData[0]->description; ?></textarea>
                        </div>
                        <div class="invalid-feedback">
                      <?php echo form_error('description'); ?>  
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update Group</button>
                </div>
              <!-- </form> -->
                <?php echo form_close(); ?>
            </div>
            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>  
<?php $this->load->view('include/footer2');?>
