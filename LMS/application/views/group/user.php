<?php $this->load->view('include/header2');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!-- <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Group</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Group Listing</li>
                    </ol>
                </div>
            </div>
        </div> -->
    </section>

    <!-- Main content -->
    <section class="content groups-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Group</h3>
                        </div>
                        <nav class="navbar navbar-expand p-0">
                            <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/edit/'.$group_id) ?>">Info</a></li>
                            <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('group/users/'.$group_id) ?>">Users</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/course/'.$group_id) ?>">Courses</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/files/'.$group_id) ?>">Files</a></li>
                            </ul>
                        </nav>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive mt-2">
                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="groupListSearch" autocomplete="off">
                                </div>
                                <table id="groupListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="groupListAjax" data-show-columns="true"
                                    data-show-columns-search="true" data-show-export="true"
                                    data-search-selector="#groupListSearch" data-buttons-prefix="btn-sm btn btn-success"
                                    data-pagination="true" data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('include/footer2');?>

<script>
var $groupListTable = $('#groupListTable');

// Row action function
function rowAction(value, row, index) {
    var html = `<i class="fa fa-ellipsis-h"></i>
                <div class="hover-tbl-btn">
                <a class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-plus"></i></a>
            </div>`;
    return [
        html
    ].join("");
}


// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
    $groupListTable.bootstrapTable('destroy').bootstrapTable({
        url: '<?php echo base_url('group/groupuserslist/'.$educator_id) ?>',
        showFullscreen: true,
        exportDataType: $(this).val(),
        exportTypes: ['csv', 'txt', 'excel', 'pdf'],
        columns: [{
                field: 'state',
                checkbox: true,
                visible: $(this).val() === 'selected'
            },
            {
                field: 'username',
                title: 'USER',
                sortable: true
            },
            {
                field: '',
                title: 'Synchronize users with courses',
                sortable: true
            },

            {
                field: 'option',
                title: 'OPTIONS',
                formatter: rowAction
            }
        ]
    })
    groupListTable.onclick = () => {
        // startLoader();
        $groupListTable.bootstrapTable('refresh');
    }
})
window.groupListAjax = {
    complete: function(xhr) {
        // stopLoader();
    }
}
</script>