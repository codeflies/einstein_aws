<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"></section>

    <!-- Main content -->
    <section class="content branches-content pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header ">
                            <h3 class="card-title"><a href="<?php echo base_url(); ?>">Home / <a
                                        href="<?php echo base_url('/branches/list'); ?>">Branches</a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <nav class="navbar navbar-expand p-0">
                                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/edit/'.$group_id) ?>">Info</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/users/'.$group_id) ?>">Users</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('group/course/'.$group_id) ?>">Courses</a></li>
                                <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('group/files/'.$group_id) ?>">Files</a></li></ul>
                            </nav>
                            <!-- branch not found end -->

                            <div class="table-responsive">
                                <div class="tab-pane fade show active">
                                    <div class="dropdown-divider mt-4 mb-4"></div>
                                    <!-- form start -->

                                    <form id="File_upload" enctype="multipart/form-data">
                                        <label>Choose File:</label>
                                        <input type="file" class="file-upload" name="image" id="fileInput" />
                                        <input type="hidden" name="group_id" value="<?php echo $group_id; ?>" />
                                    </form>
                                    <!-- Progress bar -->
                                    <div class="">
                                        <div id="progress-bar" class="progress-bar"></div>
                                    </div>
                                    <!-- Display upload status -->
                                    <div id="uploadStatus"></div>

                                </div>
                                <br>

                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="branchFileListSearch" autocomplete="off">
                                </div>
                                <table id="branchFileListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="branchFileAjax" data-show-columns="true" data-show-export="true"
                                    data-search-selector="#branchFileListSearch" data-checkbox-header="false"
                                    data-click-to-select="true" data-checkbox="true"
                                    data-buttons-prefix="btn-sm btn btn-success" data-pagination="true"
                                    data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>


                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- /.content -->
</div>

<!-- Modal of Preview file -->
<div id="show_preview_id" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-title" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="file_review" class="modal-body">
            </div>
            <div id="modal-footer" class="modal-footer">

            </div>
        </div>
    </div>
</div>

<!-- Modal of rename -->

<!-- Modal -->
<div id="rename_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Rename</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="renameSubmit">
                <div id="file_review" class="modal-body">
                    <div class="form-group">
                        <input type="text" id="rename_input" class="form-control" name="rename" />
                        <input type="hidden" id="file_id" class="form-control" value="" name="name_id" />
                    </div>

                </div>
                <div id="modal-footer" class="modal-footer">
                    <button id="Rename_submit" type="submit" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal of delete file -->
<div id="file-confirm" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="tl-modal-header">Delete file?</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Delete_file">
                <div class="modal-body">
                    <p>Are you sure you want to delete the file <b class="get_file_name"></b>?</p>
                    </p>
                    <input type="hidden" id="get_id" value="" name="file_id"/>
                    <input type="hidden" id="pre_name" value="" name="pre_name"/>
                </div>

                <div class="modal-footer">
                    <a id="tl-confirm-submit" class="btn btn-danger"><i class="icon-trash"></i>&nbsp;Delete</a>
                    <a class="btn" data-dismiss="modal">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('include/footer2');?>


<script>
var $branchFileListTable = $('#branchFileListTable');

// Row action function
function rowAction(value, row, index) {
    var image_url = "<?php echo base_url() ?>"+'uploads/group/files/'+row.pre_name.split('.')[0]+row.type;
    var html = `<i class="fa fa-ellipsis-h"></i>
                <div class="hover-tbl-btn">
                   <a class="tbl-btn tbl-preview" onclick="preview('${row.name}','${row.pre_name}','${row.type}')" title="Preview"><i class="fas fa-file-alt"></i></i></a>
                   <a href="${image_url}" download class="tbl-btn tbl-download" title="Download"><i class="fas fa-download"></i></a>
                   <a class="tbl-btn tbl-pen btnSelect" onclick="renamefunction('${row.name}','${row.id}')"><i class="fas fa-pen-square"></i></a>
                   <a onclick="DeleteFile('${row.id}','${row.name}','${row.pre_name}')" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times" ></i></a>
                </div>`;
    return [
        html
    ].join("");
}


$(function() {
    var segment_str = window.location.pathname;
    var segment_array = segment_str.split('/');
    var last_segment = segment_array.pop();
    $branchFileListTable.bootstrapTable('destroy').bootstrapTable({
        url: '<?php echo base_url('group/fileslist/') ;?>' + last_segment,
        showFullscreen: true,
        exportDataType: $(this).val(),
        exportTypes: ['excel'],
        columns: [{
                field: 'state',
                checkbox: true,
                visible: $(this).val() === 'selected'
            },
            {
                field: 'name',
                title: 'NAME',
                sortable: true
            },
            {
                field: 'visibility',
                title: 'Visibility',
                sortable: true
            },
            {
                field: 'type',
                title: 'TYPE',
                formatter: true
            },
            {
                field: 'size',
                title: 'SIZE',
                formatter: true
            },
            {
                field: 'option',
                title: 'OPTIONS',
                formatter: rowAction
            }
        ]

    })
})

window.branchFileAjax = {
    complete: function(xhr) {
        stopLoader();
    }
}

$(document).ready(function() {
    $('.tbl-preview').on("click", function() {
        console.log("hI");
    });
    $('.file-upload').on('change', function(e) {
        console.log("hi");
        var fileName = e.target.files[0].name;
        const Myfile = this.files[0];
        const fileType = Myfile['type'];
        const validImageTypes = ['image/jpg', 'image/jpeg', 'image/png', 'video/mp4',
            'application/pdf'
        ];
        if (validImageTypes.includes(fileType)) {
            $("#progress-bar").addClass("progress-bar");
            let myForm = document.getElementById('File_upload');
            let formData = new FormData(myForm);
            formData.append('formData', Myfile);
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                            $(".progress-bar").html(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: 'POST',
                url: '<?php echo base_url();?>group/file/uploadimage',
                dataType: "JSON",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    // $('#uploadStatus').html('<img src="images/loading.gif"/>');
                },
                error: function() {
                    $('#uploadStatus').html(
                        '<p style="color:#EA4335;">File upload failed, please try again.</p>'
                    );
                },
                success: function(response) {
                    if (response["success"] === true) {
                        $('.file-upload').val('');
                        $branchFileListTable.bootstrapTable('refresh');
                        snackbar(response.message);
                        $("#progress-bar").removeClass("progress-bar");
                    } else {
                        $('#uploadStatus').html(
                            '<p style="color:#EA4335;">Please select a valid file to upload.</p>'
                        );
                    }
                }
            });
        } else {
            console.log(fileName + " File type not allowed, ");
        }
    });
});

function preview(name,pre_name,file_type) {
    $('#modal-title').text(name);
    var base_url = "<?php echo base_url(); ?>";
    var file_name = pre_name.split('.')[0]; 
    $("#file_review").empty();
    $("#modal-footer").empty();
    switch (file_type) {
        case ".jpg":
            $("#file_review").append("<img class='image_preview' src=''>");
            $('.image_preview').attr('src', base_url + 'uploads/group/files/' +file_name+file_type);
            $("#modal-footer").append("<a href='' class='dwnload_file' download><i class='fas fa-download'></i></a>");
            $(".dwnload_file").attr('href', base_url + 'uploads/group/files/' + file_name+file_type);
            break;
        case '.pdf':
            $("#file_review").append("<iframe id='frame' src=''></iframe>");
            $('#frame').attr('src', base_url + 'uploads/group/files/' +file_name+file_type);
            $("#modal-footer").append("<a class='btn btn-danger' data-dismiss='modal'>Cancel</a>");
            /* implement the statement(s) to be executed when
          expression = value2 */
            break;
        case value3:
            /* implement the statement(s) to be executed when
          expression = value3 */
            break;
    }
    $("#show_preview_id").modal("show");
}

function renamefunction(name, id) {
    // $('form :input').val('');
    $("#file_id").val(id);
    $('#rename_modal').modal("show");
    $("#rename_input").val(name);
    $('#Rename_submit').on('click', function(e) {
        let myForm = document.getElementById('renameSubmit');
        let formData = new FormData(myForm);
        console.log(formData);
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url('group/RenameFile');?>',
            dataType: "JSON",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function(response) {
                $('#rename_modal').modal('toggle');
                $branchFileListTable.bootstrapTable('refresh');
                snackbar(response.message);
            },
            error: function() {
                alert('Error');
            }
        });
        return false;
    });
}

function DeleteFile(id, name,pre_name) {
    $(".get_file_name").text(name);
    $('#pre_name').val(pre_name);
    $("#get_id").val(id);
    $("#file-confirm").modal("show");
    $('#tl-confirm-submit').on('click', function(e) {
        let myForm = document.getElementById('Delete_file');
        let formData = new FormData(myForm);
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>group/DeleteUserFile',
            dataType: "JSON",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function(response) {
                $('#file-confirm').modal('toggle');
                $branchFileListTable.bootstrapTable('refresh');
                snackbar(response.message);
            },
            error: function() {
                alert('Error');
            }
        });
        return false;
    });
}
</script>