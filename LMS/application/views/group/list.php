<?php $this->load->view('include/header2');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!-- <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Group</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Group Listing</li>
                    </ol>
                </div>
            </div>
        </div> -->
    </section>

    <!-- Main content -->
    <section class="content groups-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header ">
                            <h3 class="card-title">Groups</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <?php if(!empty($result)){ ?>
                            <!-- Not found deta end -->
                            <div class="card-tbl-head">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <a href="<?php echo base_url(); ?>group/create" class="btn btn-primary">Add
                                                Group</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-right mobile-none">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-default">Mass Actions</a>
                                            <button type="button" class="btn btn-default dropdown-toggle dropdown-icon"
                                                data-toggle="dropdown" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu" role="menu" style="">
                                                <a class="dropdown-item" onclick="add_group()">Add a course
                                                    to all Group</a>
                                                <a class="dropdown-item" onclick="delete_group()">Remove a course from
                                                    all Groups</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="table-responsive mt-2">
                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="groupListSearch" autocomplete="off">
                                </div>
                                <table id="groupListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="groupListAjax" data-show-columns="true"
                                    data-show-columns-search="true" data-show-export="true"
                                    data-search-selector="#groupListSearch" data-buttons-prefix="btn-sm btn btn-success"
                                    data-pagination="true" data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                                <?php  } else{ ?>
                                <div class="groups-content-not-found">
                                    <img src="<?php echo base_url(); ?>/dist/img/groups.svg">
                                    <h3>Groups</h3>
                                    <p>Groups allow you to assign sets of courses to several users at once</p>
                                    <a href="<?php echo base_url(); ?>group/create" class="btn btn-primary">Add your
                                        first Group</a>
                                </div>
                                <?php }?>


                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- Modal -->
<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this record?
            </div>
            <div class="modal-footer">
                <?php 
          $attributes = array('enctype' => 'multipart/form-data', 'id' => 'DeleteGroup');
          echo form_open(base_url().'group/delete', $attributes); 
        ?>

                <input type="hidden" name="deleteid" id="delbtn" />
                <button type="submit" class="btn btn-danger" onclick="ColseModal()">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<!-- Add course to all groups -->
<div class="modal fade" id="add_courses_to_group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">add a course from all groups</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
         $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_AddCoursesToAllGroups');
         echo form_open(base_url().'group/AddCoursesToAllGroup', $attributes); 

       ?>
            <div class="modal-body ">
                <select name="course_id" id="deleteCourse">
                    <?php
                    if(isset($category) && isset($course_category)) { ?>
                    <option value="">Please select course</option>
                    <?php
                    foreach($category as $category_value){?>
                    <optgroup label="<?php echo $category_value->name; ?>">
                        <?php 
                            foreach($course_category as $course_category_value){ ?>
                        <?php 
                                if($category_value->id == $course_category_value->category){ ?>
                        <option value="<?php echo $course_category_value->id;?>">
                            <?php echo $course_category_value->course_name;?></option>
                        <?php }
                                ?>
                        <?php }
                            ?>
                    </optgroup>
                    <?php }
                    ?>
                    <?php } ?>
                </select><br>
                <span class="error_deletecourse"></span>
            </div>

            <div class="modal-footer">
                <!-- <input type="hidden" name="deleteid" id="delbtn" /> -->
                <button type="submit" class="btn btn-danger">Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<!-- Delete course to all groups -->
<div class="modal fade" id="delete_courses_to_group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remove a course from all groups</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
         $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_DeleteCoursesToAllGroups');
         echo form_open(base_url().'group/DeleteCoursesToGroup', $attributes); 

       ?>
            <div class="modal-body ">
                <select name="course_id" id="deleteCourse">
                    <?php
                    if(isset($category) && isset($course_category)) { ?>
                    <option value="">Please select course</option>
                    <?php
                    foreach($category as $category_value){?>
                    <optgroup label="<?php echo $category_value->name; ?>">
                        <?php 
                            foreach($course_category as $course_category_value){ ?>
                        <?php 
                                if($category_value->id == $course_category_value->category){ ?>
                        <option value="<?php echo $course_category_value->id;?>">
                            <?php echo $course_category_value->course_name;?></option>
                        <?php }
                                ?>
                        <?php }
                            ?>
                    </optgroup>
                    <?php }
                    ?>
                    <?php } ?>
                </select><br>
                <span class="error_deletecourse"></span>
            </div>

            <div class="modal-footer">
                <!-- <input type="hidden" name="deleteid" id="delbtn" /> -->
                <button type="submit" class="btn btn-danger">Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<?php $this->load->view('include/footer2');?>

<script>
var $groupListTable = $('#groupListTable');

// Row action function
function rowAction(value, row, index) {
    var html = `<i class="fa fa-ellipsis-h"></i>
                <div class="hover-tbl-btn">
                <a href="edit/${row.id}" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                <a onclick="DeleteGroup(${row.id})" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
            </div>`;
    return [
        html
    ].join("");
}


// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
    $groupListTable.bootstrapTable('destroy').bootstrapTable({
        url: 'datalist',
        showFullscreen: true,
        exportDataType: $(this).val(),
        exportTypes: ['csv', 'txt', 'excel', 'pdf'],
        columns: [{
                field: 'state',
                checkbox: true,
                visible: $(this).val() === 'selected'
            },
            {
                field: 'name',
                title: 'Name',
                sortable: true
            },
            {
                field: 'description',
                title: 'Description',
                sortable: true
            },

            {
                field: 'option',
                title: 'OPTIONS',
                formatter: rowAction
            }
        ]
    })
    groupListTable.onclick = () => {
        // startLoader();
        $groupListTable.bootstrapTable('refresh');
    }
})
window.groupListAjax = {
    complete: function(xhr) {
        // stopLoader();
    }
}

// code to read selected table row cell data (values).
$("#groupListTable").on('click', '.btnSelect', function() {
    var currentRow = $(this).closest("tr");
    var col = currentRow.find("td:eq(0)").text();
    var group_id = $(this).data('id');
    var count = $(this).attr('data-count_' + group_id);
    if (count == 0) {
        var one_include = parseInt(count) + 1;
        $(this).attr('data-count' + '_' + group_id, one_include);
        var clone_name = col + one_include;
        AjaxClone(group_id, col, clone_name);
    } else {
        var last_value = parseInt(col.slice(-1)) + parseInt(count);
        var one_include = parseInt(count) + 1;
        $(this).attr('data-count' + '_' + group_id, one_include);
        var clone_name = col + last_value;
        AjaxClone(group_id, col, clone_name);
    }
});

function add_group() {
    $("#add_courses_to_group").modal('show');
}

function delete_group() {
    $("#delete_courses_to_group").modal('show');
}
</script>