 
<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Sync with an FTP server</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">FTP</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content ftp-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Sync with an FTP server</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/Import">Import</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/Export">Export</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>ImportExport/Ftp">Sync with an FTP server</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="p-3">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="host">Host</label>
                      <input type="text" class="form-control" id="host" placeholder="ftp.domain.com" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="port">Port</label>
                      <input type="number" class="form-control" id="port" placeholder="22" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="user-name">Username</label>
                      <input type="text" class="form-control" id="user-name" placeholder="Username" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="bt-pass">Password</label>
                      <input type="password" class="form-control" id="bt-pass" placeholder="Password" />
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <button type="button" class="btn btn-primary">Test Connection</button>
                  </div>
                  <div class="col-sm-12 pt-3">
                    <div class="form-group user-export pl-4">
                      <input type="checkbox" class="form-check-input" id="e-export">
                      <label class="form-check-label" for="e-export">Enable export</label>
                      <div class="col-sm-12 mt-2 bg-grey p-3"style="display: none;" id="exportbox">
                        <div class="form-group">
                          <label for="export-directory">Export directory</label>
                          <input type="text" class="form-control" id="export-directory" placeholder="" />
                        </div>
                        <div class="form-group">
                          <label for="export-type">Export type</label>
                          <input type="text" class="form-control" id="export-type" placeholder="Export type" />
                        </div>
                        <div class="form-group">
                          <label>Date format</label>
                          <select class="form-control select2">
                            <option selected="selected">Default</option>
                            <option>Use dashes insted of slashes</option>
                            <option>Timestamp</option>
                          </select>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="missing">
                          <label class="form-check-label" for="missing">Export only recent course completions</label>
                        </div>
                        <div class="col-sm-12 pt-3">
                          <button type="button" class="btn btn-primary">Export Now</button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group user-import pl-4">
                      <input type="checkbox" class="form-check-input" id="e-import">
                      <label class="form-check-label" for="e-import">Enable import</label>
                      <div class="col-sm-12 mt-2 bg-grey p-3"style="display: none;" id="importbox">
                        <div class="form-group">
                          <label for="import-directory">Import directory</label>
                          <input type="text" class="form-control" id="import-directory" placeholder="" />
                        </div>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="missing">
                          <label class="form-check-label" for="missing">Deactivate users and courses missing from import file</label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="after-import">
                          <label class="form-check-label" for="after-import">Delete file after import</label>
                        </div>
                        <div class="col-sm-12 pt-3">
                          <button type="button" class="btn btn-primary">Import Now</button>
                        </div>
                      </div>
                    </div>
                    <!-- end -->
                  </div>
                </div>
                <div class="dropdown-divider mt-0 mb-3"></div>
                <div class="form-group mb-0">
                  <div class="row">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Save</button> <span>or</span> <a href="#">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php $this->load->view('include/footer2');?>