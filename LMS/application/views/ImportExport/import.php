
<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Import</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Import</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content content-import pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Import</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>ImportExport/Import">Import</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/Export">Export</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/Ftp">Sync with an FTP server</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="import-content p-3">
                <div class="row">
                  <div class="col-sm-9 left-col">
                    <div class="form-group">
                      <div id="actions" class="row">
                        <div class="col-lg-12">
                          <label><i class="fa fa-upload"></i> Drop a csv or excel file here or browse</label>
                          <div class="btn-group w-100">
                            <span class="btn btn-success col fileinput-button">
                              <i class="fas fa-plus"></i>
                              <span>Add files</span>
                            </span>
                            <button type="submit" class="btn btn-primary col start">
                              <i class="fas fa-upload"></i>
                              <span>Start upload</span>
                            </button>
                            <button type="reset" class="btn btn-warning col cancel">
                              <i class="fas fa-times-circle"></i>
                              <span>Cancel upload</span>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div class="table table-striped files" id="previews">
                        <div id="template" class="row mt-2">
                          <div class="col-auto">
                              <span class="preview"><img src="data:," alt="" data-dz-thumbnail /></span>
                          </div>
                          <div class="col d-flex align-items-center">
                              <p class="mb-0">
                                <span class="lead" data-dz-name></span>
                                (<span data-dz-size></span>)
                              </p>
                              <strong class="error text-danger" data-dz-errormessage></strong>
                          </div>
                          <div class="col-auto d-flex align-items-center">
                            <div class="btn-group">
                              <button class="btn btn-primary start">
                                <i class="fas fa-upload"></i>
                                <span>Start</span>
                              </button>
                              <button data-dz-remove class="btn btn-warning cancel">
                                <i class="fas fa-times-circle"></i>
                                <span>Cancel</span>
                              </button>
                              <button data-dz-remove class="btn btn-danger delete">
                                <i class="fas fa-trash"></i>
                                <span>Delete</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Import description. (Note that you can copy/paste data from EXCEL directly)</label>
                      <textarea id="codeMirrorDemo" class="p-3">
         
                      </textarea>
                    </div>
                    <div class="dropdown-divider mt-0 mb-3"></div>
                    <div class="form-group mb-0">
                      <div class="row">
                        <div class="col-sm-6">
                          <button type="button" class="btn btn-default">Import</button> <span>or</span> <a href="#">Cancel</a>
                        </div>
                      </div>
                    </div>

                    <div class="import-deta">
                      <div class="import-deta-item">
                        <h3>USERS</h3>
                        <hr/>
                        <p> <b>Format:</b> <i>Login; Firstname; Lastname; Email; Password; User-type; Bio; Active; Deactivation-date; Exclude-from-emails; [Course]; [Group]; [Branch]</i> </p>
                        <p>Required fields are Login, Firstname, Lastname & Email. You may add as many Course, Group, Branch columns as you like. When the login of a user already exists we update his info. Make sure that login is the first column. For a new user that we do not have a password we use his login as password. Active field gets as value YES or NO. Default is YES. Custom field name must have the prefix 'custom_field: ', e.g. 'custom_field: middle name'. Custom fields of type checkbox get as value on or off.</p>
                        <p><strong>Example:</strong></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="user-import-example">Login; Firstname; Lastname; Email
                          dummy_login; dummy_firstname; dummy_lastname; dummy_email@mydomain.com</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>COURSES</h3>
                        <hr/>
                        <p><b>Format:</b> <i>Course; Code; Description; Category; Duration; Price; Active; Catalog; Expiration-date</i></p>
                        <p>Required field is the Course. Active and Catalog fields get as value, YES and NO. Default for both is YES. Custom field name must have the prefix 'custom_field: ', e.g. 'custom_field: middle name'. Custom fields of type checkbox get as value on or off.</p>
                        <p><strong>Example:</strong></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="course-import-example">Course; Code; Description
                            European History; HIST01; An introductory course to European history</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>BRANCHES</h3>
                        <hr/>
                        <p><b>Format:</b> <i>Branch; Description</i></p>
                        <p>Required field is the Branch. Branch names are also used as URLs, your branch name should follow a proper URL syntax. Branch names should not contain white spaces and should be all lower case.</p>
                        <p><strong>Example:</strong></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="user-branch-import-example">Branch; Description
                            sales; All sales people belong to this branch</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>GROUPS</h3>
                        <hr/>
                        <p><b>Format:</b> <i>Group; Description; Key</i></p>
                        <p>Required field is the Group.</p>
                        <p><strong>Example:</strong></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="group-import-example">Group; Description; Key
                            New employees; All new employees should be members of this group; xyznatt</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>CATEGORIES</h3>
                        <hr/>
                        <p><b>Format:</b> <i>Category</i></p>
                        <p>You may describe a category as CategoryA > CategoryB. This will add CategoryB as child of CategoryA. All needed categories will be created along the path if they do not exist.</p>
                        <p><strong>Example:</strong></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="category-import-example">Category
                          Cat level 1 &gt; Cat level 2 &gt; Cat level 3</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>ADDITIONAL OBJECTS</h3>
                        <hr/>
                        <p>You can import existings users directly to courses, groups and branches as well as courses to groups and branches with the following simplified formats:</p>
                        <p><strong>User-to-courses:</strong> <i>usertocourses; course</i></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="usertocourse-import-example">Usertocourses; course
                          dummy_login; European History (HIST01)</pre>
                        </div>
                        <p><strong>User-to-groups:</strong> <i>usertogroups; group</i></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="usertogroup-import-example">Usertogroups; group
                          dummy_login; New employees</pre>
                        </div>
                        <p><strong>User-to-branches:</strong> <i>usertobranches; branch</i></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="usertobranch-import-example">Usertobranches; branch
                          dummy_login; sales</pre>
                        </div>
                        <p><strong>Course-to-groups:</strong> <i>coursetogroups; group</i></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="coursetogroup-import-example">Coursetogroups; group
                          European History (HIST01); New employees</pre>
                        </div>
                        <p><strong>Course-to-branches:</strong> <i>coursetobranches; branch</i></p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="coursetobranch-import-example">Coursetobranches; branch
                            European History (HIST01); sales</pre>
                        </div>
                      </div>
                      <div class="import-deta-item">
                        <h3>CONCRETE EXAMPLE</h3>
                        <hr/>
                        <p>Copy/paste the following commands to the import box. Those commands will create several system objects and assign a dummy user to them.</p>
                        <div class="import-deta-pre">
                          <a class="example-paste-code" data-id="user-import">
                            <i class="fa fa-paste"></i>&nbsp;Use this example
                          </a>
                          <pre id="concrete-import-example">## IMPORT A USER
                            Login; Firstname; Lastname; Email
                            dummy_login; dummy_firstname; dummy_lastname; dummy_email@mydomain.com

                            ## IMPORT A COURSE
                            Course; Code; Description
                            European History; HIST01; An introductory course to European history

                            ## IMPORT A GROUP
                            Group; Description; Key
                            New employees; All new employees should be members of this group; xyzhna

                            ## IMPORT A BRANCH
                            Branch; Description
                            sales; All sales people belong to this branch

                            ## IMPORT A CATEGORY
                            Category
                            Cat level 1 &gt; Cat level 2 &gt; Cat level 3

                            ## ENROLL A USER TO A COURSE
                            Usertocourses; course
                            dummy_login; European History (HIST01)

                            ## ADD A USER TO A GROUP
                            Usertogroups; group
                            dummy_login; New employees

                            ## ADD A USER TO A BRANCH
                            Usertobranches; branch
                            dummy_login; sales

                            ## ADD A COURSE TO A GROUP
                            Coursetogroups; group
                            European History (HIST01); New employees

                            ## ADD A COURSE TO A BRANCH
                            Coursetobranches; branch
                            European History (HIST01); sales</pre>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- Left End -->
                  <div class="col-sm-3 right-col">
                    <div class="admin-icons-block" onclick="location='#'">
                      <span class="admin-icon-stack">
                        <i class="fa fa-download tl-icon-stack-content"></i>
                      </span>
                      <div class="admin-icon-label">
                        <div class="admin-link">
                          <a href="#">SAMPLE EXCEL FILE </a>
                        </div>
                      </div>
                    </div>
                    <div class="admin-icons-block">
                      <span class="admin-icon-stack">
                        <i class="fa fa-book tl-icon-stack-content"></i>
                      </span>
                      <div class="admin-icon-label">
                        <div class="admin-link">
                          <a href="#" class="view-import-deta-btn">VIEW CHEATSHEET</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>