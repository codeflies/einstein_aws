
<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Export</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Export</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content export-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Export</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/import">Import</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>ImportExport/export">Export</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ImportExport/ftp">Sync with an FTP server</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="p-3">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Export type</label>
                      <select class="form-control select2">
                        <option selected="selected">Excel</option>
                        <option>CSV</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="dropdown-divider mt-0 mb-3"></div>
                <div class="form-group mb-0">
                  <div class="row">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Export</button> <span>or</span> <a href="#">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>