

<?php $this->load->view('include/header2');?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Automations</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Automations</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content automations-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Automations</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0 mb-3">
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="<?php echo base_url(); ?>eventsengine/notification" class="btn btn-default ">Notifications</a>
                      <a href="<?php echo base_url(); ?>eventsengine/automatedaction" class="btn btn-primary active">Automations</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <div class="groups-content-not-found">
                <img src="<?php echo base_url(); ?>/dist/img/empty/automations.svg">
                <h3>Automations</h3>
                <p>Automations cut your time in half.</p>
                <p>Upgrade your plan to manage your users and assign courses automatically, without hassle.</p>
              </div>
              <!-- Data not found end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('include/footer2');?>