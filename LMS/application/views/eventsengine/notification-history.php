
<?php $this->load->view('include/header2');?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Notifications</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Notifications</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Notifications</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>eventsengine/notification">Notifications</a>
                  </li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>eventsengine/NotificationHistory">History</a>
                  </li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>eventsengine/NotificationPending">Pending notifications</a></li>
                </ul>
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="<?php echo base_url(); ?>eventsengine/notification" class="btn btn-primary active">Notifications</a>
                      <a href="<?php echo base_url(); ?>eventsengine/automatedaction" class="btn btn-default">Automations</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="tab-content">
                <div class="btn-group mb-3">
                  <a href="#" class="btn btn-danger">Clear notification history</a>
                </div>

                <div class="tab-pane fade show active">
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover course-table">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Subject</th>
                          <th>Date</th>
                          <th>Options</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>jdoe@gmail.com</td>
                          <td>Your account for akankshapaliwal is ready!  </td>
                          <td>13/5/2021</td>
                          <td>
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="hover-tbl-btn">
                              <a data-toggle="modal" data-target="#modal-account" class="tbl-btn tbl-eye" title="Preview"><i class="fas fa-eye"></i></a>
                              <a href="#" class="tbl-btn tbl-send" title="Resend"><i class="fas fa-share"></i></a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>rashmipaliwal14@gmail.com</td>
                          <td>You were assigned to a new course </td>
                          <td>13/5/2021</td>
                          <td>
                            <i class="fa fa-ellipsis-h"></i>
                            <div class="hover-tbl-btn">
                              <a data-toggle="modal" data-target="#modal-account" class="tbl-btn tbl-eye" title="Preview"><i class="fas fa-eye"></i></a>
                              <a href="#" class="tbl-btn tbl-send" title="Resend"><i class="fas fa-share"></i></a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- // Tab 1 End -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>