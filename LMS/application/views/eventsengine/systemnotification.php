 
<?php $this->load->view('include/header2');?>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customize system notifications</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Notifications</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content system-notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Customize system notifications</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <h6 class="mb-2 p-2 bg-grey">FORGOT USERNAME</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">RESET PASSWORD</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">ACCOUNT CONFIRMATION</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">ACCOUNT ACTIVATION</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">EXPORT REPORTS IN EXCEL</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">IMPORT DATA</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->
              <h6 class="mb-2 p-2 bg-grey">REPLY TO DISCUSSION</h6>
              <div class="system-notifications-textarea">
                <div class="form-group">
                  <textarea name="forgot_username" class="form-control" placeholder="The first line of the message will be used as a subject for the email" rows="9" data-maxchars="15000" spellcheck="false" style="border-radius: 4px;">Forgot your username? 
                  Can't remember your username for: &lt;a href="{site_url}"&gt;{site_name}&lt;/a&gt;? Don’t worry, it happens to all of us.
                  Your username is: &lt;strong&gt;{related_user_login}&lt;/strong&gt;

                  &lt;i&gt;If you didn’t make a request for this reminder, you don’t need to do anything, and can safely ignore this email.&lt;/i&gt;</textarea><a href="#" class="color-tooltip" data-toggle="tooltip" title="The first line of the message will be used as a subject for the email"><i class="fa fa-info-circle"></i></a>
                  <div class="system-controls-list">
                    <a href="javascript:void(0)">Related user login</a>
                    <a href="javascript:void(0)">Site name</a>
                    <a href="javascript:void(0)">Site URL</a>
                    <a href="javascript:void(0)">Date</a>
                  </div>
                </div>
              </div>
              <!-- end -->

            </div>
            <!-- /.card-body -->
          </form>
          <div class="card-foot">
            
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>