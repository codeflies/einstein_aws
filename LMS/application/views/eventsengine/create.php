 
<?php $this->load->view('include/header2');?>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add notification</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Add notification</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add notification</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" />
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group">
                        <label for="lp-name">Event</label>
                        <select class="form-control select2bs4">
                          <option selected="selected">Select Event</option>
                          <option>SuperAdmin</option>
                          <option>Admin-Type</option>
                          <option>Trainer-Type</option>
                          <option>Learner-Type</option>
                        </select>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group filter-div">
                        <a href="#" class="show-filter"><i class="fa fa-filter"></i> Filter</a>
                        <div class="" style="display: none;">
                          <label for="branches">Branches</label>
                          <input type="text" class="form-control" id="branches" placeholder="Branches" />
                        </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="form-group">
                        <label for="lp-name">Recipient</label>
                        <select class="form-control select2bs4">
                          <option selected="selected">Select Recipient</option>
                          <option>Classic</option>
                          <option>Fancy</option>
                          <option>Modern</option>
                          <option>Simple</option>
                        </select>
                      </div>
                      <!-- /.form-group -->

                      <div class="form-group">
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="active">
                          <label class="form-check-label" for="active">Active</label>
                        </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <div class="btn-group">
                    <a href="<?php echo base_url(); ?>eventsengine/create" class="btn btn-primary">Create notification</a>
                    <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="#">and add another</a>
                    </div>
                  </div> <span>or</span> <a href=""#>Cancel</a>
                </div>
              </form>
            </div>

            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


     <?php $this->load->view('include/footer2');?>