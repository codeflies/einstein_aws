<?php 
  $this->load->view('include/header2');
 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-12 text-center">
            <h1 class="m-0">Welcome back! Here's what you can do today </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
          <div class="welcome-board">
            <div class="welcome-box">
              <a href="#">
                <div class="welcome-boxin">
                  <div class="welcome-icon">
                    <img src="<?php echo base_url(); ?>dist/img/CreateCourse.svg" />
                  </div>
                  <div class="welcome-text">
                    <p>Jump right in and <b>create a course</b></p>
                  </div>
                </div>
              </a>
            </div>
            <div class="welcome-box">
              <a href="#">
                <div class="welcome-boxin welcome-boxin-border">
                  <div class="welcome-icon">
                    <img src="<?php echo base_url(); ?>dist/img/SampleCourse.svg" />
                  </div>
                  <div class="welcome-text">
                    <p>Learn how EinsteinsMind works by <b>taking a sample course</b></p>
                  </div>
                </div>
              </a>
            </div>
            <div class="welcome-box">
              <a href="#">
                <div class="welcome-boxin">
                  <div class="welcome-icon">
                    <img src="<?php echo base_url(); ?>dist/img/CustomizePortal.svg" />
                  </div>
                  <div class="welcome-text">
                    <p><b>Customize your account</b> by adding your logo, changing the language, themes and colors</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="welcome-box">
              <a href="<?php echo base_url(); ?>user/dashboard">
                <div class="welcome-boxin">
                  <div class="welcome-icon">
                    <img src="<?php echo base_url(); ?>dist/img/Admin.svg" />
                  </div>
                  <div class="welcome-text">
                    <p>Go to the <b>Administrator dashboard</b> and explore all our features</p>
                  </div>
                </div>
              </a>
            </div>
          </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->

<?php 
  $this->load->view('include/footer2');
 ?>