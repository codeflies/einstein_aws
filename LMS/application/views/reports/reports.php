 <?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Overview</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content userinfo-content">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Reports</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>reports/reports">Overview</a></li>
                  <li class="nav-item"><a class="nav-link" href="#">Infographics</a></li>
                  <li class="nav-item"><a class="nav-link" href="#">Training matrix</a></li>
                  <li class="nav-item"><a class="nav-link" href="#">Timeline</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="tab-content">
                <div class="tab-pane fade show active" role="tabpanel">
                  1
                </div>
                <!-- // Tab 1 End -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Update User </button> <span>or</span> <a href=""#>Cancel</a>
            </div>
          </form>
        </div>

            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>