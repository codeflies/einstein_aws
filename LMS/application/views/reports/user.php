 
<?php $this->load->view('include/header2');?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reports Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">User reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content userinfo-content pb-2">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User reports</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="p-3">
              <div class="microstats-section mobile-none">
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="3">3</div>
                    <div class="item-caption">learners</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="1">1</div>
                    <div class="item-caption">course assignments</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">completed courses</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">Courses in progress</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0h 0m">0h 0m</div>
                    <div class="item-caption">Training time</div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- end -->
              <div class="table-responsive">
                  <table id="example1" class="table table-striped table-hover user-table">
                    <thead>
                      <tr>
                        <th>User</th>
                        <th>User Type</th>
                        <th>Last Login</th>
                        <th>Assigned courses</th>
                        <th>Completed courses</th>
                        <th>Points</th>
                        <th>Badges</th>
                        <th>Level</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a href="#">A. Paliwal</a></td>
                        <td>SuperAdmin</td>
                        <td>8 minutes ago </td>
                        <td>1</td>
                        <td>-</td>
                        <td>25</td>
                        <td>0</td>
                        <td>1</td>
                        <td>
                          <a href="#" class="tbl-btn"><i class="fa fa-signal"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td><a href="#">A. Paliwal</a></td>
                        <td>SuperAdmin</td>
                        <td>8 minutes ago </td>
                        <td>1</td>
                        <td>-</td>
                        <td>25</td>
                        <td>0</td>
                        <td>1</td>
                        <td>
                          <a href="#" class="tbl-btn"><i class="fa fa-signal"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <!-- end -->
            </div>
            <!-- end -->
          </div>
          <!-- /.card-body -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>