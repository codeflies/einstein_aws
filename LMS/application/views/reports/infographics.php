<?php $this->load->view('include/header2');?>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Infographic</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Infographic</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content userinfo-content pb-5">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Name Here</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist" id="profile-tabs-tab" role="tablist" aria-orientation="">
                  <li class="nav-item"><a class="btn btn-success" href="javascript:void(0);">Export</a></li>
                </ul>
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="<?php echo base_url(); ?>reports/userinfo" class="btn btn-default">Profile</a>
                      <a href="#" class="btn btn-default">Progress</a>
                      <a href="infographics.html" class="btn btn-primary active">Infographic</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-2 mb-3"></div>
              
              <div class="infographics-user">
                <div class="infographics-section infographics-header">
                  <div class="infographics-user-fullname"><span title="A. Paliwal">A. Paliwal</span></div>
                  <div class="infographics-headtitle">Training infographic</div>
                  <div class="infographics-date"><span>01/06/2021</span></div>
                </div>
                <div class="infographics-section infographics-section-1 clearfix">
                  <div class="item item2">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s1_courses.png">
                    <div class="item-value" id="tl-infographics-s1-v1" data-value="7" title="7">7</div>
                    <div class="item-caption">Courses</div>
                  </div>
                  <div class="tl-infographics-vertical-divider mobile-none"></div>
                  <div class="item item2">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s1_certifications.png">
                    <div class="item-value" id="tl-infographics-s1-v2" data-value="0" title="0">0</div>
                    <div class="item-caption">Certifications</div>
                  </div>
                  <div class="tl-infographics-vertical-divider mobile-none"></div>
                  <div class="item item2">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s1_points.png">
                    <div class="item-value" id="tl-infographics-s1-v3" data-value="25" title="25">25</div>
                    <div class="item-caption">Points</div>
                  </div>
                  <div class="tl-infographics-vertical-divider mobile-none"></div>
                  <div class="item item2">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s1_badges.png">
                    <div class="item-value" id="tl-infographics-s1-v4" data-value="0" title="0">0</div>
                    <div class="item-caption">Badges</div>
                  </div>
                  <div class="tl-infographics-vertical-divider mobile-none"></div>
                  <div class="item item2">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s1_level.png">
                    <div class="item-value" id="tl-infographics-s1-v5" data-value="1" title="1">1st</div>
                    <div class="item-caption">Level</div>
                  </div>
                </div>
                <div class="infographics-section infographics-section-2 clearfix">
                  <div class="infographics-section-headtitle">Training time</div>
                  <div class="item item5">
                    <img class="item-icon" src="<?php echo base_url(); ?>dist/img/infographics/s2_watch.png"><br>
                    <div class="item-value" style="display:inline-block" data-value="0">0<span class="tl-infographics-time-unit">h</span></div>
                    <div class="item-value" style="display:inline-block" data-value="0">0<span class="tl-infographics-time-unit">m</span></div>
                    <div class="item-caption">Training time</div>
                  </div>
                </div>
                <div class="infographics-section infographics-section-3 clearfix">
                  <div class="item item6">
                    <div class="infographics-section-headtitle">Course Completion Rate</div>
                    <div class="mobile-none" data-value="{&quot;course_completed&quot;:0,&quot;course_failed&quot;:0,&quot;course_not_completed&quot;:28.600000000000001,&quot;course_not_started&quot;:71.400000000000006}" data-highcharts-chart="0">
                      <div class="highcharts-container" id="highcharts-0" style="position: relative; overflow: hidden; width: 600px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); display: block;">
                        <svg version="1.1" style="font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="600" height="400"><desc>Created with Highcharts 4.2.3</desc><defs><clipPath id="highcharts-1"><rect x="0" y="0" width="580" height="375"></rect></clipPath></defs><rect x="0" y="0" width="600" height="400" fill="transparent" class=" highcharts-background"></rect><g class="highcharts-series-group" zIndex="3"><g class="highcharts-series highcharts-series-0 highcharts-tracker" zIndex="0.1" transform="translate(10,10) scale(1 1)" style="cursor:pointer;"><path fill="#C5D7E8" d="M 289.9638480063441 10.000003681596212 A 177.5 177.5 0 1 1 117.07730006554121 227.549842039353 L 186.24638003932472 211.5299052236118 A 106.5 106.5 0 1 0 289.97830880380644 81.00000220895774 Z" stroke="#C5D7E8" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path><path fill="#A4C8EA" d="M 117.03733669151961 227.37689934331968 A 177.5 177.5 0 0 1 289.7534560609923 10.000171222375144 L 289.8520736365954 81.00010273342508 A 106.5 106.5 0 0 0 186.22240201491178 211.4261396059918 Z" stroke="#A4C8EA" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path><path fill="#8CBDEA" d="M 289.9309559834586 10.000013428384221 A 177.5 177.5 0 0 1 289.7534560609923 10.000171222375144 L 289.8520736365954 81.00010273342508 A 106.5 106.5 0 0 0 289.95857359007516 81.00000805703053 Z" stroke="#8CBDEA" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path><path fill="#80b7eb" d="M 289.9309559834586 10.000013428384221 A 177.5 177.5 0 0 1 289.7534560609923 10.000171222375144 L 289.8520736365954 81.00010273342508 A 106.5 106.5 0 0 0 289.95857359007516 81.00000805703053 Z" stroke="#80b7eb" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path></g><g class="highcharts-markers highcharts-series-0" zIndex="0.1" transform="translate(10,10) scale(1 1)"></g></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;pointer-events:none;white-space:nowrap;" transform="translate(249,160)" opacity="1" visibility="visible"><path fill="none" d="M 3 0 L 90 0 C 93 0 93 0 93 3 L 93 78 C 93 81 93 81 90 81 L 3 81 C 0 81 0 81 0 78 L 0 3 C 0 0 0 0 3 0"></path></g></svg>
                        <div class="highcharts-tooltip" style="position: absolute; left: 249px; top: 160px; opacity: 1; visibility: visible;">
                          <span style="position: absolute; font-size: 16px; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;" zindex="1">
                            <div style="text-align: center;">
                              <span style="font-size:16px;">Completed</span><br>
                              <span style="font-size:44px; color: #80b7eb; font-weight: bold">0%</span>
                            </div>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="item item6">
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s3_pencil.png">
                      <div class="item-value" data-value="7" title="7">7</div>
                      <div class="item-caption">Courses</div>
                    </div>
                    <div class="tl-infographics-vertical-divider mobile-none"></div>
                    <div class="item item6">
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s3_tick.png">
                      <div class="item-value" data-value="0" title="0">0</div>
                      <div class="item-caption">Completions</div>
                    </div>
                  </div>
                  <div class="tl-infographics-vertical-divider infographics-vertical-divider-large mobile-none"></div>
                  <div class="item item6">
                    <div class="infographics-section-headtitle second-column">Test Pass Rate</div>
                    <div class="mobile-none" data-value="0" data-highcharts-chart="1">
                      <div class="highcharts-container" id="highcharts-2" style="position: relative; overflow: hidden; width: 100%; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); display: block;">
                        <svg version="1.1" style="font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="600" height="400"><desc>Created with Highcharts 4.2.3</desc><defs><clipPath id="highcharts-3"><rect x="0" y="0" width="580" height="375"></rect></clipPath></defs><rect x="0" y="0" width="600" height="400" fill="transparent" class=" highcharts-background"></rect><g class="highcharts-series-group" zIndex="3"><g class="highcharts-series highcharts-series-0 highcharts-tracker" zIndex="0.1" transform="translate(10,10) scale(1 1)" style="cursor:pointer;"><path fill="#80b7eb" d="M 289.9638480063441 10.000003681596212 A 177.5 177.5 0 0 1 289.7863480576851 10.000128583574593 L 289.87180883461104 81.00007715014476 A 106.5 106.5 0 0 0 289.97830880380644 81.00000220895774 Z" stroke="#80b7eb" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path><path fill="#F2C29F" d="M 289.9638480063441 10.000003681596212 A 177.5 177.5 0 1 1 289.7534560609923 10.000171222375144 L 289.8520736365954 81.00010273342508 A 106.5 106.5 0 1 0 289.97830880380644 81.00000220895774 Z" stroke="#F2C29F" stroke-width="1" stroke-linejoin="round" transform="translate(0,0)"></path></g><g class="highcharts-markers highcharts-series-0" zIndex="0.1" transform="translate(10,10) scale(1 1)"></g></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;pointer-events:none;white-space:nowrap;" transform="translate(233,160)" opacity="1" visibility="visible"><path fill="none" d="M 3 0 L 121 0 C 124 0 124 0 124 3 L 124 78 C 124 81 124 81 121 81 L 3 81 C 0 81 0 81 0 78 L 0 3 C 0 0 0 0 3 0"></path></g>
                        </svg>
                        <div class="highcharts-tooltip" style="position: absolute; left: 233px; top: 160px; opacity: 1; visibility: visible;">
                          <span style="position: absolute; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;" zindex="1">
                            <div class="text-center">
                              <span>Test Pass Rate</span><br>
                              <span style="font-size:44px; color: #80b7eb; font-weight: bold">0%</span>
                            </div>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="item item6">
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s3_pages.png">
                      <div class="item-value" data-value="0" title="0">0</div>
                      <div class="item-caption">Executions</div>
                    </div>
                    <div class="tl-infographics-vertical-divider mobile-none"></div>
                    <div class="item item6">
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s3_plot.png">
                      <div class="item-value" data-value="0" title="0">0%</div>
                      <div class="item-caption">Average score</div>
                    </div>
                  </div>
                </div>
                <div class="infographics-section infographics-section-4 clearfix">
                  <div class="infographics-section-headtitle">Badges</div>
                  <div class="item item6 infographics-badge-percentage">
                    <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s4_badges.png">
                    <div class="item-value" data-value="0" title="0">0%</div>
                    <div class="item-caption">Unlocked badges</div>
                  </div>
                  <div class="text-center pull-left hide-on-export">
                    <a class="btn btn-warning" id="go-to-all-badges">All badges</a>
                  </div>
                </div>
                <div class="infographics-section infographics-section-5 clearfix">
                  <div class="infographics-section-headtitle">Compared to others</div>
                    <div class="item item4 cursor-pointer" data-toggle="modal" data-target="#modal-infographic">
                      <div class="bar-value-wrapper">
                        <div class="bar-value" style="display: block;">1st</div>
                      </div>
                      <div class="bar-vertical">
                        <div class="bar bar-points delay0" data-value="100" style="height: 100%;"></div>
                      </div>
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s1_points.png">
                      <div class="item-caption">Points</div>
                    </div>
                    <div class="item item4 cursor-pointer" data-toggle="modal" data-target="#modal-infographic">
                      <div class="bar-value-wrapper">
                        <div class="bar-value" style="display: block;">1st</div>
                      </div>
                      <div class="bar-vertical">
                        <div class="bar bar-badges delay2" data-value="100" style="height: 100%;"></div>
                      </div>
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s1_badges.png">
                      <div class="item-caption">Badges</div>
                    </div>
                    <div class="item item4 cursor-pointer" data-toggle="modal" data-target="#modal-infographic">
                      <div class="bar-value-wrapper">
                        <div class="bar-value" style="display: block;">1st</div>
                      </div>
                      <div class="bar-vertical">
                        <div class="bar bar-level delay4" data-value="100" style="height: 100%;"></div>
                      </div>
                      <img class="item-icon" src="<?php echo base_url(); ?>/dist/img/infographics/s1_level.png">
                      <div class="item-caption">Level</div>
                    </div>
                </div>
              </div>
              <!-- end -->
            </div>
            <!-- /.card-body -->            
          </form>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>