 <?php $this->load->view('include/header2');?>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reports Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Reports Users Info</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content userinfo-content pb-5">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Name Here</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist" id="profile-tabs-tab" role="tablist" aria-orientation="">
                  <li class="nav-item"><a class="nav-link active" href="#custom-overview" aria-controls="custom-overview" id="custom-overview-tab" data-toggle="pill" aria-selected="true">Overview</a></li>
                  <li class="nav-item"><a class="nav-link" href="#custom-courses" aria-controls="custom-courses" id="custom-courses-tab" data-toggle="pill" aria-selected="false">Courses</a></li>
                  <li class="nav-item"><a class="nav-link" href="#custom-certifications" aria-controls="custom-certifications" id="custom-certifications-tab" data-toggle="pill" aria-selected="false">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="#custom-badges" aria-controls="custom-badges" id="custom-badges-tab" data-toggle="pill" aria-selected="false">Badges</a></li>
                  <li class="nav-item"><a class="nav-link" href="#custom-timeline" aria-controls="custom-timeline" id="custom-timeline-tab" data-toggle="pill" aria-selected="false">Timeline</a></li>
                </ul>
                <ul class="navbar-nav ml-auto pb-2 mobile-none">
                  <li>
                    <div class="btn-group">
                      <a href="#" class="btn btn-default">Profile</a>
                      <a href="#" class="btn btn-primary active">Progress</a>
                      <a href="<?php echo base_url(); ?>reports/infographics" class="btn btn-default">Infographic</a>
                    </div>
                  </li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="tab-content"id="profile-tabs-tabContent">
                <div class="tab-pane fade show active" role="tabpanel" id="custom-overview" aria-labelledby="custom-overview-tab">
                  <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                      Digital Strategist
                    </div>
                    <div class="card-body pt-0">
                      <div class="row">
                        <div class="col-7">
                          <h2 class="lead"><b>Nicole Pearson</b></h2>
                          <p class="text-muted text-sm"><b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                          <ul class="ml-4 mb-0 fa-ul text-muted">
                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: + 800 - 12 12 23 52</li>
                          </ul>
                        </div>
                        <div class="col-5 text-center">
                          <img src="<?php echo base_url(); ?>/dist/img/user1-128x128.jpg" alt="user-avatar" class="img-circle img-fluid">
                        </div>
                      </div>
                    </div>
                    <div class="card-footer">
                      <div class="text-right">
                        <a href="#" class="btn btn-sm bg-teal">
                          <i class="fas fa-comments"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-primary">
                          <i class="fas fa-user"></i> View Profile
                        </a>
                      </div>
                    </div>
                  </div>
                  <!-- end -->
                  
                  <div class="microstats-section mobile-none">
                    <div class="item">
                      <div class="item-data">
                        <div class="item-value" title="2">2</div>
                        <div class="item-caption">Courses in progress</div>
                      </div>
                    </div>
                    <!-- end -->
                    <div class="item">
                      <div class="item-data">
                        <div class="item-value" title="0">0</div>
                        <div class="item-caption">Completed courses</div>
                      </div>
                    </div>
                    <!-- end -->
                    <div class="item">
                      <div class="item-data">
                        <div class="item-value" title="5">5</div>
                        <div class="item-caption">Courses not started</div>
                      </div>
                    </div>
                    <!-- end -->
                    <div class="item">
                      <div class="item-data">
                        <div class="item-value" title="0h 0m">0h 0m</div>
                        <div class="item-caption">Training time</div>
                      </div>
                    </div>
                    <!-- end -->
                    <div class="item">
                      <div class="item-data">
                        <div class="item-value" title="25">25</div>
                        <div class="item-caption">Points</div>
                      </div>
                    </div>
                    <!-- end -->
                  </div>
                  <!-- end -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="report-activity">
                        <div class="report-activity-head">
                          <h5>Activity</h5>
                        </div>
                        <div class="report-activity-body">
                          <div class="activity"><span class="ra-report-figure">18</span> logins last week · <span class="ra-report-figure">36</span> logins last month · last login: <span class="ra-report-figure">2 hours ago</span></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="report-progress">
                        <div class="report-progress-head">
                          <h5 class="float-left">Progress overview</h5>
                          <div class="float-right"><a href="https://akankshapaliwal.talentlms.com/reports/userinfographics/id:1">View infographic&nbsp;&gt;</a></div>
                        </div>
                        <div class="report-progress-body">
                          
                        </div>                        
                      </div>
                    </div>
                  </div>
                  <!-- end -->
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="">
                        <div class="empty-state text-center">
                          <div class="empty-state-icon"><img class="item-icon" src="<?php echo base_url(); ?>/dist/img/empty_badges.png"></div>
                          <p class="empty-state-caption empty-state-caption-title">You haven't earned any badges yet.</p>
                          <p class="empty-state-caption">Don't give up!</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class=""></div>
                    </div>
                  </div>
                  <!-- end -->

                </div>
                <!-- // Tab 1 End -->
                <div class="tab-pane fade" role="tabpanel" id="custom-courses" aria-labelledby="custom-courses-tab">
                  <div class="table-responsive">
                    <table id="example1" class="table table-striped table-hover course-table">
                      <thead>
                        <tr>
                          <th>Course</th>
                          <th>Progress</th>
                          <th>Score</th>
                          <th>Enrolled on</th>
                          <th>Completion date</th>
                          <th>Time</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Advanced Features of TalentLMS <small>(002)</small></td>
                          <td>Simple</td>
                          <td>-</td>
                          <td>13/5/2021</td>
                          <td>-</td>
                          <td>32s</td>
                        </tr>
                        <tr>
                          <td>Advanced Features of TalentLMS <small>(002)</small></td>
                          <td>Simple</td>
                          <td>-</td>
                          <td>13/5/2021</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- // Tab 2 End -->
                <div class="tab-pane fade" role="tabpanel" id="custom-certifications" aria-labelledby="custom-certifications-tab">
                  <div class="certifications-content-not-found">
                    <img src="<?php echo base_url(); ?>/dist/img/certificates.svg">
                    <p>You do not have any certifications</p>
                  </div>
                  <!-- Not found deta end -->
                </div>
                <!-- // Tab 3 End -->
                <div class="tab-pane fade" role="tabpanel" id="custom-badges" aria-labelledby="custom-badges-tab">
                  <div class="userreport-badges-content">
                    <h6 class="bg-grey p-3">ACTIVITY BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Newbie</strong></div>
                          <div class="badge-list-para mobile-none">(4 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Grower</strong></div>
                          <div class="badge-list-para mobile-none">(8 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Adventurer</strong></div>
                          <div class="badge-list-para mobile-none">(16 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Explorer</strong></div>
                          <div class="badge-list-para mobile-none">(32 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Star</strong></div>
                          <div class="badge-list-para mobile-none">(64 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Superstar</strong></div>
                          <div class="badge-list-para mobile-none">(128 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Master</strong></div>
                          <div class="badge-list-para mobile-none">(256 logins)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-activity-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt mobile-none"><strong>Activity Grandmaster</strong></div>
                          <div class="badge-list-para mobile-none">(512 logins)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">LEARNING BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Newbie</strong></div>
                          <div class="badge-list-para">(1 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Grower</strong></div>
                          <div class="badge-list-para">(2 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Adventurer</strong></div>
                          <div class="badge-list-para">(4 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Explorer</strong></div>
                          <div class="badge-list-para">(8 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Star</strong></div>
                          <div class="badge-list-para">(16 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Superstar</strong></div>
                          <div class="badge-list-para">(32 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Master</strong></div>
                          <div class="badge-list-para">(64 completed course)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-learning-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Learning Grandmaster</strong></div>
                          <div class="badge-list-para">(128 completed course)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">TEST BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Newbie</strong></div>
                          <div class="badge-list-para">(2 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-2"><img src="/<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Grower</strong></div>
                          <div class="badge-list-para">(4 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Adventurer</strong></div>
                          <div class="badge-list-para">(8 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Explorer</strong></div>
                          <div class="badge-list-para">(16 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Star</strong></div>
                          <div class="badge-list-para">(32 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Superstar</strong></div>
                          <div class="badge-list-para">(64 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Master</strong></div>
                          <div class="badge-list-para">(128 passed tests)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-test-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Test Grandmaster</strong></div>
                          <div class="badge-list-para">(256 passed tests)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">ASSIGNMENT BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-1"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Newbie</strong></div>
                          <div class="badge-list-para">(1 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-2"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Grower</strong></div>
                          <div class="badge-list-para">(2 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Adventurer</strong></div>
                          <div class="badge-list-para">(4 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Explorer</strong></div>
                          <div class="badge-list-para">(8 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Star</strong></div>
                          <div class="badge-list-para">(16 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Superstar</strong></div>
                          <div class="badge-list-para">(32 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Master</strong></div>
                          <div class="badge-list-para">(64 passed assignments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-assignment-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Assignment Grandmaster</strong></div>
                          <div class="badge-list-para">(128 passed assignments)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">PERFECTIONISM BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-1"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Newbie</strong></div>
                          <div class="badge-list-para">(1 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-2"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Grower</strong></div>
                          <div class="badge-list-para">(2 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Adventurer</strong></div>
                          <div class="badge-list-para">(4 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Explorer</strong></div>
                          <div class="badge-list-para">(8 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Star</strong></div>
                          <div class="badge-list-para">(16 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Superstar</strong></div>
                          <div class="badge-list-para">(32 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Master</strong></div>
                          <div class="badge-list-para">(64 passed tests or assignments with score 90%+)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-perfectionism-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Perfectionism Grandmaster</strong></div>
                          <div class="badge-list-para">(128 passed tests or assignments with score 90%+)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">SURVEY BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-1"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Newbie</strong></div>
                          <div class="badge-list-para">(1 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-2"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Grower</strong></div>
                          <div class="badge-list-para">(2 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Adventurer</strong></div>
                          <div class="badge-list-para">(4 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Explorer</strong></div>
                          <div class="badge-list-para">(8 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Star</strong></div>
                          <div class="badge-list-para">(16 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Superstar</strong></div>
                          <div class="badge-list-para">(32 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Master</strong></div>
                          <div class="badge-list-para">(64 completed survey)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-survey-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Survey Grandmaster</strong></div>
                          <div class="badge-list-para">(128 completed survey)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">COMMUNICATION BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-1"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Newbie</strong></div>
                          <div class="badge-list-para">(2 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-2"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Grower</strong></div>
                          <div class="badge-list-para">(4 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Adventurer</strong></div>
                          <div class="badge-list-para">(8 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Explorer</strong></div>
                          <div class="badge-list-para">(16 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Star</strong></div>
                          <div class="badge-list-para">(32 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Superstar</strong></div>
                          <div class="badge-list-para">(64 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Master</strong></div>
                          <div class="badge-list-para">(128 discussion topics or comments)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-communication-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Communication Grandmaster</strong></div>
                          <div class="badge-list-para">(256 discussion topics or comments)</div>
                        </li>
                      </ul>
                    </div>
                    <h6 class="bg-grey p-3 mb-3">CERTIFICATION BADGES</h6>
                    <div class="badge-list activity-list">
                      <ul>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-1"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Newbie</strong></div>
                          <div class="badge-list-para">(1 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-2"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Grower</strong></div>
                          <div class="badge-list-para">(2 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-3"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Adventurer</strong></div>
                          <div class="badge-list-para">(4 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-4"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Explorer</strong></div>
                          <div class="badge-list-para">(8 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-5"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Star</strong></div>
                          <div class="badge-list-para">(16 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-6"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Superstar</strong></div>
                          <div class="badge-list-para">(32 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-7"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Master</strong></div>
                          <div class="badge-list-para">(64 certifications)</div>
                        </li>
                        <li class="badge-list-box">
                          <div class="badge-gr-img badge-certification-8"><img src="<?php echo base_url(); ?>dist/img/download-badges.png" /></div>
                          <div class="badge-list-txt"><strong>Certification Grandmaster</strong></div>
                          <div class="badge-list-para">(128 certifications)</div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- // Tab 4 End -->
                <div class="tab-pane fade" role="tabpanel" id="custom-timeline" aria-labelledby="custom-timeline-tab">
                  <div class="table-responsive">
                    <table id="example3" class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>EVENTS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><button type="button" class="btn btn-success btn-xs">LOGIN </button> <span class="tl-event-entity">You</span>  signed in - <small><em class="tl-event-time">13 minutes ago</em></small></td>
                        </tr>
                        <tr>
                          <td><button type="button" class="btn btn-success btn-xs">LOGIN </button> <span class="tl-event-entity">You</span>  signed in - <small><em class="tl-event-time">28/05/2021</em></small></td>
                        </tr>
                        <tr>
                          <td><button type="button" class="btn btn-danger btn-xs">CREATE </button> <span class="tl-event-entity">You</span> created the group <span class="tl-event-entity"><span title="Startup">Startup</span></span> <small><em class="tl-event-time">13 minutes ago</em></small></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- // Tab 5 End -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->

            
          </form>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php $this->load->view('include/footer2');?>