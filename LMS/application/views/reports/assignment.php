  
<?php $this->load->view('include/header2');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Assignment reports</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>/user/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Assignment reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content assignments-content pb-2">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Assignment reports</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="p-3">
              <div class="microstats-section mobile-none">
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">assignments</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">submissions</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">passed</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">average grade</div>
                  </div>
                </div>
                <!-- end -->
                <div class="item">
                  <div class="item-data">
                    <div class="item-value" title="0">0</div>
                    <div class="item-caption">last activity</div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- end -->
              <div class="table-responsive">
                  <table id="example1" class="table table-striped table-hover user-table">
                    <thead>
                      <tr>
                        <th>Assignment</th>
                        <th>Course</th>
                        <th>Submissions</th>
                        <th>Passed</th>
                        <th>Average grade</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a href="#">Working</a></td>
                        <td>This is a SCORM Example Course <small>(006)</small></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>
                          <a href="#" class="tbl-btn"><i class="fa fa-signal"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td><a href="#">Working</a></td>
                        <td>Content and Einstein Mind <small>(006)</small></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>
                          <a href="#" class="tbl-btn"><i class="fa fa-signal"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <!-- end -->
            </div>
            <!-- end -->
          </div>
          <!-- /.card-body -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>