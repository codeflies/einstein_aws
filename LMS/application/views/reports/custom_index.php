 <?php $this->load->view('include/header2');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Custom reports</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>/user/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Custom reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content custom-rept-content pb-2">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Custom reports</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="p-3">
              <div class="custom-report-not-found">
                <img src="<?php echo base_url(); ?>/dist/img/reports.svg">
                <h3>Custom reports</h3>
                <p>Custom reports give you a full overview of your learners' performance in just a few clicks.</p>
                <p>Upgrade your plan to create personalized reports and perform actions that affect several users at once.</p>
              </div>
              <!-- end -->
            </div>
            <!-- end -->
          </div>
          <!-- /.card-body -->
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php $this->load->view('include/footer2');?>