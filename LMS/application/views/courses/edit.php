<?php $this->load->view('include/header2'); 
?>
<?php
 function fetch_menu($cat_data,$parent_id){
    foreach($cat_data as $menu){
    if($menu->id == $parent_id){
        $selected = "selected";
      }
      else{
        $selected = "";
      }
    echo "<option value='".$menu->id."' ".$selected .">".$menu->name."</option>";
      if(!empty($menu->sub)){
        $space="&nbsp;&nbsp;&nbsp;&nbsp;";
        $arrow = ">";
        $menu_id = $menu->id;
        fetch_sub_menu($menu->sub,$space,$parent_id,$arrow);
      }
    }
  }

  function fetch_sub_menu($sub_menu,$space,$parent_id,$arrow){
    
    foreach($sub_menu as $menu1){
      if($menu1->id == $parent_id){
        $selected = "selected";
      }
      else{
        $selected = "";
      }
      echo "<option value='".$menu1->id."' ".$selected .">".$arrow.$space.$menu1->name."</option>";
      if(!empty($menu1->sub)){
        $space.="&nbsp;&nbsp;&nbsp;&nbsp";
        $arrow.=">";
        $menu_id = $menu1->id;
        fetch_sub_menu($menu1->sub,$space,$parent_id,$arrow);
      }
    }
  }

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!-- <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Course</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>user/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Course</li>
                    </ol>
                </div>
            </div>
        </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Update Course</h3>
                </div>
                <nav class="navbar navbar-expand p-0">
                    <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('Courses/edit/'.$course_id); ?>">Course</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url('Course/user/'.$course_id); ?>">User</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url('Course/branches/'.$course_id); ?>">Branches</a>
                        </li>
                    </ul>
                    <!-- <ul class="navbar-nav ml-auto pb-2 mobile-none">
                            <li>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-primary">Profile</a>
                                    <a href="#" class="btn btn-default">Progress</a>
                                    <a href="#" class="btn btn-default">Infographic</a>
                                </div>
                            </li>
                        </ul> -->
                </nav>
                <!-- /.card-header -->
                <!-- form start -->
                <?php 
                  $attributes = array('enctype' => 'multipart/form-data', 'id' => 'CourseADD');
               echo form_open(base_url().'courses/update', $attributes); 
              ?>

                <input type="hidden" name="update_course" value="UpdateCourse">
                <input type="hidden" name="user_id" value="<?php echo $CourseData[0]->id; ?>">
                <div class="card-body">
                    <div class="invalid-feedback">
                        <p><?php echo $this->session->flashdata('message'); ?></p>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="course-name">Course Name</label>
                                    <input type="text" class="form-control"
                                        value="<?php echo $CourseData[0]->course_name; ?>" name="course_name"
                                        placeholder="e.g. Introduction to Accounting" />
                                </div>
                                <div class="invalid-feedback">
                                    <?php echo form_error('course-name'); ?>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="lp-name">Category</label>
                                    <?php 
                                        $parent_id = $CourseData[0]->category;
                                        ?>
                                        <?php 
                                            echo '<select class="form-control select2bs4" name="category">';
                                            echo '<option>Select or create a Category</option>';
                                            echo fetch_menu($cat_data,$parent_id);
                                            echo '</select>';
                                        ?>
                                    <!-- <select class="form-control select2bs4" style="width: 100%;" name="category">
                                        <option selected="selected">Select or create a Category</option>
                                        <?php //if(isset($cat_data)){ 
                                            //foreach ($cat_data as $cat_value) { ?>
                                            <option <?php //if($cat_value->id == $CourseData[0]->category ) {
                                            //echo "selected";
                                            //}?> 
                                            value="<?php //echo $cat_value->id;?>"><?php //echo $cat_value->name;?></option>
                                        <?php //} 
                                         //} ?>
                                    </select> -->
                                </div>
                                <div class="invalid-feedback">
                                    <?php echo form_error('category'); ?>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea rows="4" class="form-control" name="description"
                                        placeholder="Short description up to 800 characters"><?php echo $CourseData[0]->description; ?></textarea>
                                </div>
                                <div class="invalid-feedback">
                                    <?php echo form_error('description'); ?>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="user-active">
                                        <label class="form-check-label" name="active" for="user-active">Active</label>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="user-deactive"
                                            id="user-deactive">
                                        <label class="form-check-label" for="user-deactive">Hide from course catalog <i
                                                class="fa fa-info-circle"></i></label>
                                    </div>
                                </div>

                                <!-- /.form-group -->
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code"><i class=""></i>Choose a file to upload</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image">
                                <label class="custom-file-label" for="exampleInputFile1">Choose a file to upload</label>
                            </div>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="code"><i class="fas fa-tag mr-2"></i> Course code</label>
                                <input type="text" class="form-control" name="course_code"
                                    value="<?php echo $CourseData[0]->course_code; ?>" placeholder="Course code" />
                            </div>

                            <div class="invalid-feedback">
                                <?php echo form_error('course-code'); ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ct-price"><i class="fas fa-cart-arrow-down mr-2"></i> Price</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-dollar-sign"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="price" value="<?php echo $CourseData[0]->price;?>"
                                        lass="form-control">
                                </div>
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('price'); ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label><i class="fas fa-video mr-2"></i> Intro video</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="code">Use YouTube</label>
                                <input type="text" class="form-control" name="You_Tube" placeholder="Unlimited" />
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="code">Use a Video</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="Use_Video">
                                        <label class="custom-file-label" for="exampleInputFile1">Choose a file to
                                            upload</label>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="capacity"><i class="fas fa-users mr-2"></i>Capacity</label>
                                <input type="text" class="form-control" name="Capacity"
                                    value="<?php echo $CourseData[0]->Capacity;?>" placeholder="Unlimited" />
                            </div>

                            <div class="invalid-feedback">
                                <?php echo form_error('Capacity'); ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.form-group -->
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="code"><i class="fas fa-clock mr-2"></i> Course expiration</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="code">Time Limit</label>
                                <input type="text" class="form-control" name="course_expiration" placeholder="Days" />
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('course_expiration'); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Expiration date</label>
                                <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="expiration_date"
                                        data-target="#reservationdatetime" />
                                    <div class="input-group-append" data-target="#reservationdatetime"
                                        data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('course_expiration'); ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="checkbox" id="retain-active">
                                    <label class="form-check-label" for="retain-active">Retain access for users who have
                                        completed the course</label>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('checkbox'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lp-name" name="certification"><i class="fas fa-certificate mr-2"></i>
                                    Certification</label>
                                <select class="form-control select2bs4" style="width: 100%;">
                                    <option selected="selected">Select a Certification</option>
                                    <option>Classic</option>
                                    <option>Fancy</option>
                                    <option>Modern</option>
                                    <option>Simple</option>
                                </select>
                            </div>

                            <div class="invalid-feedback">
                                <?php echo form_error('certification'); ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lp-name" name="level"><i class="fas fa-signal mr-2"></i> Level</label>
                                <select class="form-control select2bs4" style="width: 100%;">
                                    <option selected="selected">Select a Level</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>

                            <div class="invalid-feedback">
                                <?php echo form_error('level'); ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update Course</button>
                </div>
                <?php echo form_close(); ?>
            </div>

            <br />
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('include/footer2');?>
<!-- Page specific script -->
<script>
$(function() {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
        'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {
        'placeholder': 'mm/dd/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({
        icons: {
            time: 'far fa-clock'
        }
    });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY hh:mm A'
        }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf('month')
                ]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
        },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
        format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()


    $('.my-colorpicker2').on('colorpickerChange', function(event) {
        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function() {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

})
// BS-Stepper Init
document.addEventListener('DOMContentLoaded', function() {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
})

// DropzoneJS Demo Code Start
Dropzone.autoDiscover = false

// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template")
previewNode.id = ""
var previewTemplate = previewNode.parentNode.innerHTML
previewNode.parentNode.removeChild(previewNode)

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
})

myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() {
        myDropzone.enqueueFile(file)
    }
})

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
})

myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
})

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
})

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
// document.querySelector("#actions .start").onclick = function() {
//   myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
// }
// document.querySelector("#actions .cancel").onclick = function() {
//   myDropzone.removeAllFiles(true)
// }
// DropzoneJS Demo Code End
</script>