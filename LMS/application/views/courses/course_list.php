<?php $this->load->view('include/header2');?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!-- <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Courses</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Course Listing</li>
                    </ol>
                </div>
            </div>
        </div> -->
    </section>

    <!-- Main content -->
    <section class="content courselist-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Courses</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-tbl-head">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <a href="<?php base_url(); ?>create" class="btn btn-primary">Add Course</a>
                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon"
                                                data-toggle="dropdown" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu" role="menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-copy"></i> Copy from
                                                    another domain</a>
                                            </div>
                                        </div>

                                        <div style="display: none" id="new-btn2" class="btn-group">
                                            <a href="<?php base_url(); ?>create" class="btn btn-primary">Add Course
                                                new</a>
                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon"
                                                data-toggle="dropdown" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu" role="menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-copy"></i> Copy from
                                                    another domain</a>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="col-sm-6 text-right mobile-none">
                                        <a href="#"><i class="fa fa-cart-arrow-down"></i> Buy Courses</a> &nbsp;
                                        <a href="#"><i class="fa fa-share"></i> View course catalog</a>

                                        <!-- <div id="courseListToolbar">
                                            <button type="button" class="btn btn-primary btn-sm refresh-btn">Refresh <i
                                                    class="fas fa-sync"></i></button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="table-responsive">
                                <div>
                                    <input class="py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="courseListSearch" autocomplete="off">
                                </div>
                                <table id="courseListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="courseListSearch" data-show-columns="true"
                                    data-show-columns-search="true" data-show-export="true"
                                    data-search-selector="#courseListSearch" data-checkbox-header="false"
                                    data-click-to-select="true" data-checkbox="true"
                                    data-buttons-prefix="btn-sm btn btn-success" data-pagination="true"
                                    data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this record?
            </div>
            <div class="modal-footer">
                <?php 
          $attributes = array('enctype' => 'multipart/form-data', 'id' => 'DeleteCourse');
          echo form_open(base_url().'courses/delete', $attributes); 
        ?>

                <input type="hidden" name="deleteid" id="delbtn" />
                <button type="submit" class="btn btn-danger" onclick="ColseModal()">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<!-- // Clone -->

<div class="modal fade" id="Clone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> Add Clone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to Clone this record?
            </div>
            <div class="modal-footer">
                <?php 
          $attributes = array('enctype' => 'multipart/form-data', 'id' => 'CloneCourse');
          echo form_open(base_url().'courses/clone', $attributes); 
        ?>
                <input type="hidden" name="clone" id="clone" />
                <button type="submit" class="btn btn-success">Colone</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- END FUNCTION-->


<?php $this->load->view('include/footer2');?>
<!-- Page specific script -->
<script>
var $courseListTable = $('#courseListTable');

// Row action function
function rowAction(value, row, index) {
    var html = `<i class="fa fa-ellipsis-h"></i>
        <div class="hover-tbl-btn">
        <a href="edit/${row.id}" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
        <a class="tbl-btn btnSelect" data-id="${row.id}" data-count_${row.id}="0"><i class="fas fa-magnet"></i></a>
        <a onclick="DeleteCourse(${row.id})" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
    </div>`;
    return [
        html
    ].join("");
}

function SelectOption(value, row, index) {
    var html = `<input type="checkbox">`;
    return [
        html
    ].join("");
}
// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
    $courseListTable.bootstrapTable('destroy').bootstrapTable({
        url: 'datalist',
        showFullscreen: true,
        exportDataType: $(this).val(),
        exportTypes: ['csv', 'txt', 'excel', 'pdf'],
        columns: [{
                field: 'state',
                checkbox: true,
                visible: $(this).val() === 'selected',
            },
            {
                field: 'selectOption',
                clickToSelect: false,
                title: '<input type="checkbox">',
                formatter: SelectOption,
                sortable: false
            },
            {
                field: 'course_name',
                title: 'course',
                sortable: true
            },
            {
                field: 'name',
                title: 'Category',
                sortable: true
            },
            {
                field: 'update_at',
                title: 'Last updated on',
                sortable: true
            },

            {
                field: 'option',
                title: 'OPTIONS',
                formatter: rowAction
            }
        ]
    })
    courseListToolbar.onclick = () => {
        // startLoader();
        $courseListTable.bootstrapTable('refresh');
    }
})

window.courseListAjax = {
    complete: function(xhr) {
    }
}

$("#courseListTable").on('click', '.btnSelect', function() {
    var currentRow = $(this).closest("tr");
    var col = currentRow.find("td:eq(1)").text();
    var course_id = $(this).data('id');
    if (confirm("Do you want to create a clone of this course? "+col)) {
        var course_id = $(this).data('id');
        var url = 'clone';
        // Ajax request
        var response = CommonAjaxForObjData(url, 'POST', {
            id: course_id,
            clone_name: col
        });
        // Handling response
        if (response.success === true) {
            $courseListTable.bootstrapTable('refresh');
        }
        snackbar(response.message);
    }



});
</script>

<script>
$(document).ready(function() {
    $(".th-inner input").change(function() {
        if (this.checked) {
            $('#new-btn2').show();
        } else {
            $('#new-btn2').hide();
        }
    });
    $("#courseListTable tbody input").change(function() {
        if (this.checked) {
            $('#new-btn2').show();
        } else {
            $('#new-btn2').hide();
        }
    });
    $(".th-inner input").change(function() {
        var checked = $(this).is(":checked");
        if (checked) {
            $("#courseListTable tbody input").each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $("#courseListTable tbody input").each(function() {
                $(this).prop("checked", false);
            });
        }
    });
});
</script>