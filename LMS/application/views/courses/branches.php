<?php $this->load->view('include/header2'); 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content branches-content pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header ">
                            <h3 class="card-title"><a href="<?php echo base_url(); ?>">Home / <a href="<?php echo base_url('/branches/list'); ?>">Courses</a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <nav class="navbar navbar-expand p-0">
                                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('Courses/edit/'.$course_id); ?>">Course</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('Course/user/'.$course_id); ?>">User</a></li>
                                    <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('Course/branches/'.$course_id); ?>">Branches</a>
                                    </li>
                                </ul>
                            </nav>
                            <!-- branch not found end -->

                            <div class="dropdown-divider"></div>
                            <div class="table-responsive">
                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="CourseBranchListSearch" autocomplete="off">
                                </div>
                                <table id="CourseBranchListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="CourseBranchListAjax" data-show-columns="true" data-show-export="true"
                                    data-search-selector="#CourseBranchListSearch" data-checkbox-header="false"
                                    data-click-to-select="true" data-checkbox="true"
                                    data-buttons-prefix="btn-sm btn btn-success" data-pagination="true"
                                    data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>

                            
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('include/footer2');?>
<!-- page specific js -->
<script>
var $CourseBranchListTable = $('#CourseBranchListTable');

// Row action function
function rowAction(value, row, index) {
   var html = `<i class="fa fa-ellipsis-h"></i>
               <div class="hover-tbl-btn">
                    <a class="tbl-btn tbl-pen btnSelect" onclick="AddManger()"><i class="fas fa-plus"></i></a>
               </div>`;
   return [
       html
   ].join("");
}


// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
   $CourseBranchListTable.bootstrapTable('destroy').bootstrapTable({
       url: '<?php echo base_url('Courses/branchdatalist/'.$course_id); ?>',
       showFullscreen: true,
       exportDataType: $(this).val(),
       exportTypes: ['excel'],
       columns: [{
               field: 'state',
               checkbox: true,
               visible: $(this).val() === 'selected'
           },
           {
               field: 'name',
               title: 'USER TYPE',
               sortable: true
           },
           {
               field: 'option',
               title: 'OPTIONS',
               formatter: rowAction
           }
       ]
   })
})
window.CourseBranchListAjax = {
   complete: function(xhr) {
       stopLoader();
   }
}

</script>