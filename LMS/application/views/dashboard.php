<?php 
	include "include/header2.php";
 ?>
   <!-- Main content -->
    <div class="content admin-dashboard-home">
      <div class="container">
        <div class="card card-primary admin-dashboard-card">
          <div class="card-header">
            <h3 class="card-title">Home</h3>
            <div class="card-tools">
            </div>
          </div>
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-6">
                <div class="admin-dashboard">
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-user tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="list">Users </a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="create">Add user</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-book tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo  base_url(); ?>Courses/course_list">Courses </a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>Courses/create">Add Courses</a></li>
                          <li><a href="#">Course Store</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-server tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>category/list">Categories</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>category/create">Add Categories</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-users tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>group/list">Groups </a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>group/create">Add group</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-sitemap tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>branches/list">Branches</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>branches/create">Add Branches</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-clock tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>eventsengine/automatedaction">Events engine </a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>eventsengine/notification">Add notification</a></li>
                          <li><a href="<?php echo base_url(); ?>eventsengine/automatedaction">Add automation</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-user tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>usertype/list">User types</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>usertype/create">Add user type</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-sync tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>ImportExport/import">Import - Export</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>ImportExport/import">Import</a></li>
                          <li><a href="<?php echo base_url(); ?>ImportExport/export">Export</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-chart-pie tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="#">Reports</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>reports/userinfo">Users</a></li>
                          <li><a href="<?php echo base_url(); ?>reports/course">Courses</a></li>
                          <li><a href="<?php echo base_url(); ?>reports/branch">Branches</a></li>
                          <li><a href="<?php echo base_url(); ?>reports/group">Groups</a></li>
                          <li><a href="#">Scorm</a></li>
                          <li><a href="#">Tests</a></li>
                          <li><a href="#">Surveys</a></li>
                          <li><a href="#">Assignments</a></li>
                          <li><a href="#">ILTs</a></li>
                          <li><a href="<?php echo base_url(); ?>reports/infographics">Infographics</a></li>
                          <li><a href="#">Training matrix</a></li>
                          <li><a href="#">Timeline</a></li>
                          <li><a href="#">Custom</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                  <div class="admin-icons-block" onclick="location='#'">
                    <span class="admin-icon-stack">
                      <i class="fa fa-sliders-h tl-icon-stack-content"></i>
                    </span>
                    <div class="admin-icon-label">
                      <div class="admin-link">
                        <a href="<?php echo base_url(); ?>accountsettings/basic">Account & Settings</a>
                      </div>
                      <div class="mobile-none">
                        <ul>
                          <li><a href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/gamification/">Gamification</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                          <li><a href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- /.end -->
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col-md-6 -->
              <div class="col-lg-6">
                <div class="chart">
                  <img src="<?php echo base_url(); ?>dist/img/chart.svg">
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
        </div>
          
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


 <?php 
	include "include/footer2.php";
 ?>
 <script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label               : 'Digital Goods',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label               : 'Electronics',
          backgroundColor     : 'rgba(210, 214, 222, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [65, 59, 80, 81, 56, 55, 40]
        },
      ]
    }

    var areaChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    new Chart(areaChartCanvas, {
      type: 'line',
      data: areaChartData,
      options: areaChartOptions
    });
});
</script>
