<?php 
  include_once "include/header.php";
  $remember = false;
 ?>
<div class="login-box">
  <div class="login-logo">
    <a href="index.html">
        <img src="<?php echo  base_url(); ?>dist/img/em-logo.png" alt="Logo" class="brand-image">
      </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <?php echo form_open(base_url()); ?>
        <div class="invalid-feedback">
          <p><?php echo $this->session->flashdata('message'); ?></p>
        </div>
        <input type="hidden" name="educator_login" value="educator_login">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="domain_name"
          value="<?php 
                if(get_cookie("domain_name")) {
                  echo get_cookie("domain_name");
                }
              ?>" placeholder="Domain name">
          <div class="input-group-append">
            <div class="input-group-text">
              .einstienmind.com
            </div>
          </div>
        </div>
        <div class="invalid-feedback">
          <?php echo form_error('domain_name'); ?>
        </div>
        <div class="input-group mb-3">
          <input type="text" name="usernameEmail"
            value="<?php 
                if(get_cookie("userNameEmail")) {
                  echo get_cookie("userNameEmail");
                }
              ?>"
           class="form-control" placeholder="Username or Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="invalid-feedback">
          <?php echo form_error('usernameEmail'); ?>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control"
            value="<?php 
                if(get_cookie("password")) {
                  $remember = true;
                  echo get_cookie("password");
                }
              ?>"
           name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="invalid-feedback">
          <?php echo form_error('password'); ?>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="icheck-primary">
              <input type="checkbox" <?php 
                if ($remember === true) {
                  echo "checked";
                }
               ?> name="remember_me" value="true" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <div class="col-6">
            <p class="mb-1 text-right">
              <a href="<?php echo base_url(); ?>user/forgot_password">Forgot Password?</a>
            </p>
          </div>  
          <!-- /.col -->
          <!-- /.col -->
        </div>
          <p class="text-center">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </p>
        <?php echo form_close(); ?>
      <!-- <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
      <!-- /.social-auth-links -->

      <!-- <p class="mb-1">
        <a href="<?php echo base_url(); ?>user/forget_password">I forgot my password</a>
      </p> -->
      <p class="text-center ">
        <a href="<?php echo base_url(); ?>user/register" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<?php 
  include_once "include/footer.php";
 ?>
