  <?php $this->load->view('include/header2');?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic_">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="user-notifications p-3">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Signup</label>
                        <select class="form-control select2">
                          <option selected="selected">Manually (from Admin)</option>
                          <option>Direct</option>
                          <option>Direct + Captcha</option>
                          <option>Direct + Email verification</option>
                          <option>Direct + Admin activation</option>
                          <option>Manually (from Admin)</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Default user type</label>
                        <select class="form-control select2">
                          <option selected="selected">Learner-Type</option>
                          <option>SuperAdmin</option>
                          <option>Admin-Type</option>
                          <option>Trainer-Type</option>
                          <option>Learner-Type</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Default group</label>
                      <select class="form-control select2">
                        <option selected="selected">Select a group</option>
                        <option>Startup</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div id="accordion1">
                      <div class="card-header pl-2 br-0">
                        <h4 class="card-title w-100">
                          <span for="restrict" data-toggle="collapse" href="#passsetting"><i class="fa fa-lock"></i> Password settings</span>
                        </h4>
                      </div>
                      <div id="passsetting" class="collapse bg-grey" data-parent="#accordion1">
                        <div class="p-3">
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="enforce">
                                <label class="form-check-label" for="enforce">Enforce strong passwords &nbsp;<i class="fa fa-info"></i></label>
                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="enforcepass">
                                <label class="form-check-label" for="enforcepass">Enforce password change after</label>

                                <div class="input-group mb-3">
                                  <input type="text" class="form-control" disabled="">
                                  <div class="input-group-append">
                                    <span class="input-group-text">Days</span>
                                  </div>
                                </div>

                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="enforcelogin">
                                <label class="form-check-label" for="enforcelogin">Enforce password change on first login &nbsp;<i class="fa fa-info"></i></label>
                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="enforceafter">
                                <label class="form-check-label" for="enforceafter">Lock account after</label>
                                <div class="input-group mb-3">
                                  <input type="text" class="form-control" disabled="">
                                  <div class="input-group-append">
                                    <span class="input-group-text">failed attempts</span>
                                  </div>&nbsp;<span style="vertical-align: middle;"> for </span>&nbsp;
                                  <input type="text" class="form-control" disabled="">
                                  <div class="input-group-append">
                                    <span class="input-group-text">minutes</span>
                                  </div>
                                </div>

                              </div>
                            </div>
                            <!-- end -->
                        </div>
                      </div>
                    </div> 
                    <!-- end -->
                    <div id="accordion2">
                      <div class="card-header pl-2 br-0">
                        <h4 class="card-title w-100">
                          <span for="restrict" data-toggle="collapse" href="#termsservices"><i class="fa fa-file"></i> Terms of Service</span>
                        </h4>
                      </div>
                      <div id="termsservices" class="collapse" data-parent="#accordion2">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <textarea rows="4" class="form-control" placeholder="The terms of service is shown to each user when they first login to the system. It is necessary to accept it in order to continue. Leave empty if you don't want to have terms of service."></textarea>
                          </div>
                        </div>
                      </div>
                    </div> 
                    <!-- end -->
                    <div id="accordion3">
                      <div class="card-header pl-2 br-0">
                        <h4 class="card-title w-100">
                          <span for="restrict" data-toggle="collapse" href="#userformat"><i class="fa fa-user"></i> Visible user format</span>
                        </h4>
                      </div>
                      <div id="userformat" class="collapse" data-parent="#accordion3">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <select class="form-control">
                              <option selected="selected">F. Last Name</option>
                              <option>F. Last Name</option>
                              <option>First Name L.</option>
                              <option>First Name Last Name</option>
                              <option>Last Name F.</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div> 
                    <!-- end -->
                    <div id="accordion4">
                      <div class="card-header pl-2 br-0">
                        <h4 class="card-title w-100">
                          <span for="restrict" data-toggle="collapse" href="#socialoption"><i class="fa fa-comment"></i> Social options</span>
                        </h4>
                      </div>
                      <div id="socialoption" class="collapse bg-grey" data-parent="#accordion1">
                        <div class="p-3">
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="enforcepass">
                                <label class="form-check-label" for="enforcepass">Allow signups through social media</label>
                                <div class="mb-3">
                                  <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="facebook">
                                    <label class="form-check-label" for="facebook">Facebook</label>
                                  </div>
                                  <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="facebook">
                                    <label class="form-check-label" for="facebook">Google</label>
                                  </div>
                                  <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="facebook">
                                    <label class="form-check-label" for="facebook">Linkedin</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="allowinte">
                                <label class="form-check-label" for="allowinte">Allow interactions with social media in course catalog &nbsp;<i class="fa fa-info"></i></label>
                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="allowlinked">
                                <label class="form-check-label" for="allowlinked">Allow sharing certifications on LinkedIn &nbsp;<i class="fa fa-info"></i></label>
                              </div>
                            </div>
                            <!-- end -->
                            <div class="form-group">
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="allowrating">
                                <label class="form-check-label" for="allowrating">Allow course rating &nbsp;<i class="fa fa-info"></i></label>
                              </div>
                            </div>
                            <!-- end -->
                        </div>
                      </div>
                    </div> 
                    <!-- end -->
                    <div id="accordion5">
                      <div class="card-header pl-2 br-0">
                        <h4 class="card-title w-100">
                          <span for="restrict" data-toggle="collapse" href="#"><i class="fa fa-cloud"></i> Single Sign-On (SSO)</span>
                        </h4>
                      </div>
                    </div> 
                    <!-- end -->
                  </div>
                </div>
                <div class="dropdown-divider mt-0 mb-3"></div>
                <div class="form-group mb-0">
                  <div class="row">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Save</button> <span>or</span> <a href="#">Cancel</a>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>