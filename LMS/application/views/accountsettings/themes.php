  

<?php $this->load->view('include/header2');?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="user-notifications p-3">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Theme</label>
                        <select class="form-control select2">
                          <option selected="selected">Modern (Your active theme)</option>
                          <option>Default</option>
                          <option>City Lights</option>
                          <option>Color</option>
                          <option>Pets</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-sm-12">
                    <ul class="nav nav-tabs" id="color-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="color-colors-tab" data-toggle="pill" href="#color-colors" role="tab" aria-controls="color-colors" aria-selected="true">Colors</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="color-css-tab" data-toggle="pill" href="#color-css" role="tab" aria-controls="color-css" aria-selected="false">Css</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="color-js-tab" data-toggle="pill" href="#color-js" role="tab" aria-controls="color-js" aria-selected="false">Javascript</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="color-tabContent">
                      <div class="tab-pane fade show active" id="color-colors" role="tabpanel" aria-labelledby="color-colors-tab">
                        <div class="row pt-2">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="item">Item</label>
                              <select class="form-control select3">
                                <option selected="selected">Modern (Your active theme)</option>
                                <option>Default</option>
                                <option>City Lights</option>
                                <option>Color</option>
                                <option>Pets</option>
                              </select>
                            </div>
                          </div>
                          <!-- /.form-group -->
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Color picker:</label>
                              <input type="text" class="form-control my-colorpicker1">
                            </div>
                          </div>
                          <!-- /.form-group -->
                        </div>
                      </div>
                      <!-- end -->
                      <div class="tab-pane fade" id="color-css" role="tabpanel" aria-labelledby="color-css-tab">
                        <div class="col-sm-12 p-3">
                          <textarea id="codeMirrorDemo" class="p-3">
                            <div class="info-box bg-gradient-info">
                              <span class="info-box-icon"><i class="far fa-bookmark"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Bookmarks</span>
                                <span class="info-box-number">41,410</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                                  70% Increase in 30 Days
                                </span>
                              </div>
                            </div>
                          </textarea>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- end -->
                      <div class="tab-pane fade" id="color-js" role="tabpanel" aria-labelledby="color-js-tab">
                        <div class="col-sm-12 p-3">
                          <textarea id="codeMirrorDemo1" class="p-3">
                            <div class="info-box bg-gradient-info">
                              <span class="info-box-icon"><i class="far fa-bookmark"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Javascript</span>
                              </div>
                            </div>
                          </textarea>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- end -->
                    </div>
                  </div>
                </div>
                <div class="dropdown-divider mt-0 mb-3"></div>
                <div class="form-group mb-0">
                  <div class="row">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Save as new</button>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
     <?php $this->load->view('include/footer2');?>