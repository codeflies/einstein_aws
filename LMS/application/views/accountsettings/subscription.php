
<?php $this->load->view('include/header2');?>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="subscrib-content p-3">
                  <div class="hero-unit">
                    <p>You are registered to the <span class="tl-subscription-details">Free</span> plan</p>
                    <p>
                    You currently use <span class="tl-subscription-details">4</span> out of <span class="tl-subscription-details">5</span> users (80%) and <span class="tl-subscription-details">9</span> out of <span class="tl-subscription-details">10</span> courses (90%).     </p>
                  </div>
                  <!-- hero unit end -->
                  <div class="plans-toggle">
                    <div class="input-group">
                      <label class="control-label">Plan type</label>
                      <select class="custom-select">
                        <option selected="selected">Standard Plan</option>
                        <option>Active Plan</option>
                      </select>
                    
                      <label class="control-label"> Include EinsteinsMind™</label>
                      <select class="custom-select">
                        <option selected="selected">No</option>
                        <option>Yes</option>
                      </select>

                      <label class="control-label">Billed</label>
                      <select class="custom-select">
                        <option selected="selected">Yearly</option>
                        <option>Monthly</option>
                      </select>
                    </div>
                  </div>
                  <!-- plan select end -->
                  <div class="subscrib-plan-row">
                    <div class="row">
                      <div class="plan-box active-plan">
                        <div class="plan-box-head">
                          <h3>Free</h3>
                          <h4>
                            <span class="invisible">
                              <span class="dollar-price">$</span>0.00 <span>/ month</span>
                            </span>
                          </h4>
                        </div>
                        <ul>
                          <li class="plans-billing-period">&nbsp;</li>
                          <li class="plans-users-limit">Up to 5 users</li>
                          <li>Up to 10 courses</li>
                        </ul>
                        <div class="select-plan-btn active-plan-btn">
                          Your active plan
                        </div>
                      </div>
                      <div class="plan-box">
                        <div class="plan-box-head">
                          <h3>Starter</h3>
                          <h4><span class="dollar-price">$</span>59.00 <span>/ month</span></h4>
                          <p class="plans-billing-period">billed yearly</p>
                        </div>
                        <ul>
                          <li class="plans-users-limit">Up to 40 users</li>
                          <li>Unlimited courses</li>
                        </ul>
                        <div class="select-plan-btn">
                          <a class="btn btn-primary" href="#">Select plan</a>
                        </div>
                      </div>
                      <div class="plan-box">
                        <div class="plan-box-head">
                          <h3>Basic</h3>
                          <h4><span class="dollar-price">$</span>129.00 <span>/ month</span></h4>
                          <p class="plans-billing-period">billed yearly</p>
                        </div>
                        <ul>
                          <li class="plans-users-limit">Up to 100 users</li>
                          <li>Unlimited courses</li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Single Sign-On support</span></li>
                        </ul>
                        <div class="select-plan-btn">
                          <a class="btn btn-primary" href="#">Select plan</a>
                        </div>
                      </div>
                      <div class="plan-box">
                        <div class="plan-box-head">
                          <h3>Plus</h3>
                          <h4><span class="dollar-price">$</span>249.00 <span>/ month</span></h4>
                          <p class="plans-billing-period">billed yearly</p>
                        </div>
                        <ul>
                          <li class="plans-users-limit">Up to 500 users</li>
                          <li>Unlimited courses</li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Single Sign-On support</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Custom reports</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Automations</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Success Manager</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">SSL for your custom domain</span></li>
                        </ul>
                        <div class="select-plan-btn">
                          <a class="btn btn-primary" href="#">Select plan</a>
                        </div>
                      </div>
                      <div class="plan-box">
                        <div class="plan-box-head">
                          <h3>Premium</h3>
                          <h4><span class="dollar-price">$</span>429.00 <span>/ month</span></h4>
                          <p class="plans-billing-period">billed yearly</p>
                        </div>
                        <ul>
                          <li class="plans-users-limit">Up to 1000 users</li>
                          <li>Unlimited courses</li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Single Sign-On support</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Custom reports</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Automations</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">Success Manager</span></li>
                          <li><span data-toggle="tooltip" data-placement="bottom" title="Hooray Hooray Hooray Hooray Hooray">SSL for your custom domain</span></li>
                          <li>Live chat support</li>
                        </ul>
                        <div class="select-plan-btn">
                          <a class="btn btn-primary" href="#">Select plan</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- plan select end -->

                  <div class="subscrib-note">
                    <p><sub>Note: Need more users or have a special requirement? <a href="#" target="_blank">Contact us</a> so we can create a plan that fits your needs!</sub></p>
                    <a href="#" class="btn btn-default">Billing history and other options</a>
                  </div>
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

    <?php $this->load->view('include/footer2');?>