<?php $this->load->view('include/header2');?>
 <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
                <!-- home page create -->
                <div class="cms-editor-sec">
                  <div class="cms-head">
                    <div class="row">
                      <div class="col-sm-6">
                        <a href="javascript:void(0);" class="btn btn-primary" data-original-title="" title="Publish your homepage to activate it. You can switch to the simple homepage at any time.">Publish homepage</a>
                      </div>
                      <div class="col-sm-6 cms-head-right">
                        <div class="cms-edit-menu">
                          <ul class="d-inline">
                            <li><a href="#"><i class="fa fa-eye"></i></a></li>
                            <li><a href="#" class="btn btn-default">Edit menu</a></li>
                          </ul>
                        </div>
                        <div class="cms-menu">
                          <ul class="nav nav-tabs" id="cms-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="cms-home-tab" data-toggle="pill" href="#cms-home" role="tab" aria-controls="cms-home" aria-selected="true">Home</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="cms-about-tab" data-toggle="pill" href="#cms-about" role="tab" aria-controls="cms-about" aria-selected="false">About</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- head -->

                  <div class="cms-body">
                    <div class="tab-content" id="cms-tabContent">
                      <div class="tab-pane fade show active" id="cms-home" role="tabpanel" aria-labelledby="cms-home-tab">
                        <div class="cms-edit-page-sections">
                          <ul class="cms-page-sections-list">
                            <li id="section1" class="single-section-line">
                              <div class="cms-page-section-wrapper">
                                <a href="#" class="cms-page-section-sort-icon mobile-none btn-default btn"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                <div class="cms-section-hover-right-buttons">
                                  <a href="javascript:void(0);" class="btn btn-default cms-section-delete-icon mobile-none"><i class="fa fa-trash" title="Delete"></i></a>
                                  <a href="javascript:void(0);" class="btn btn-primary cms-edit-section-button mobile-none" data-id="1" data-sectiontype="1">Edit section</a>
                                </div>
                                <div class="cms-page-title cms-home-title text-center">
                                  <h1>Welcome to Einsteinsmind</h1>
                                </div>
                                <div class="cms-home-image">
                                  <img class="show" src="<?php echo base_url(); ?>dist/img/empty/cms_default.jpg">
                                </div>
                              </div>
                            </li>
                            <li id="section2" class="single-section-line">
                              <div class="cms-page-section-wrapper">
                                <a href="#" class="cms-page-section-sort-icon mobile-none btn-default btn"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                <div class="cms-section-hover-right-buttons">
                                  <a href="javascript:void(0);" class="btn btn-default cms-section-delete-icon mobile-none"><i class="fa fa-trash" title="Delete"></i></a>
                                  <a href="javascript:void(0);" class="btn btn-primary cms-edit-section-button mobile-none" data-id="1" data-sectiontype="1">Edit section</a>
                                </div>
                                <div class="cms-home-featured-title text-center">
                                  <h1>Featured courses</h1>
                                </div>
                                <div class="row justify-content-center">
                                  <a href="#" class="cms-course-wrapper">
                                    <div class="tl-cms-course-image-wrapper text-center">
                                      <img src="https://d3j0t7vrtr92dk.cloudfront.net/samplecourses/1548346756_intro.png?" alt="Introduction to" title="Introduction">
                                    </div>
                                    <div class="cms-course-title text-center">
                                      <div>
                                        <span title="Introduction to TalentLMS (001)" class="formatted-course-name">Introduction to Talen... 
                                          <span class="formatted-course-code">(001)</span>
                                        </span>
                                      </div>
                                    </div>
                                  </a>
                                  <a href="#" class="cms-course-wrapper">
                                    <div class="cms-course-image-wrapper text-center">
                                      <img src="https://d3j0t7vrtr92dk.cloudfront.net/samplecourses/1548346716_content.png?" alt="Content and" title="Content and (003)">
                                    </div>
                                    <div class="cms-course-title text-center">
                                      <div>
                                        <span title="Content and (003)" class="formatted-course-name">Content and TalentLMS 
                                          <span class="formatted-course-code">(003)</span>
                                        </span>
                                      </div>
                                    </div>
                                  </a>
                                  <a href="#" class="cms-course-wrapper">
                                    <div class="cms-course-image-wrapper text-center">
                                      <img src="https://d3j0t7vrtr92dk.cloudfront.net/samplecourses/1548346702_toolkit.png?" alt="Advanced Features of (002)" title="Advanced Features of (002)">
                                    </div>
                                    <div class="cms-course-title text-center">
                                      <div>
                                        <span title="Advanced Features of (002)" class="formatted-course-name">Advanced Features of ... 
                                          <span class="formatted-course-code">(002)</span>
                                        </span>
                                      </div>
                                    </div>
                                  </a>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.page end -->
                      <div class="tab-pane fade" id="cms-about" role="tabpanel" aria-labelledby="cms-about-tab">
                        <div class="cms-edit-page-sections">
                          <ul class="cms-page-sections-list">
                            <li id="section1" class="single-section-line">
                              <div class="cms-page-section-wrapper">
                                <a href="#" class="cms-page-section-sort-icon mobile-none btn-default btn"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                <div class="cms-section-hover-right-buttons">
                                  <a href="javascript:void(0);" class="btn btn-default cms-section-delete-icon mobile-none"><i class="fa fa-trash" title="Delete"></i></a>
                                  <a href="javascript:void(0);" class="btn btn-primary cms-edit-section-button mobile-none" data-id="1" data-sectiontype="1">Edit section</a>
                                </div>
                                <div class="cms-page-title cms-home-title text-center">
                                  <h1>About us</h1>
                                  <p>A short intro about your organization</p>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.page end -->
                    </div>
                  </div>
                  <!-- body -->

                  <div class="cms-foot">
                    <div class="cms-add-section-button" data-toggle="modal" data-target="#modal-banner">
                      <div class="icon-background icon-background-banner"></div>
                      <p>Add banner</p>
                    </div>
                    <div class="cms-add-section-button" data-toggle="modal" data-target="#modal-text">
                      <div class="icon-background icon-background-text"></div>
                      <p>Add text</p>
                    </div>
                    <div class="cms-add-section-button" data-toggle="modal" data-target="#modal-media">
                      <div class="icon-background icon-background-media"></div>
                      <p>Add text and media</p>
                    </div>
                    <div class="cms-add-section-button" data-toggle="modal" data-target="#modal-courses">
                      <div class="icon-background icon-background-courses"></div>
                      <p>Add courses</p>
                    </div>
                    <div class="cms-add-section-button" data-toggle="modal" data-target="#modal-key-point">
                      <div class="icon-background icon-background-key-points"></div>
                      <p>Add key points</p>
                    </div>
                  </div>
                  <!-- foot -->
                </div>
                <!-- home page end -->

                <!-- cms start -->
                <div class="cms-content-not-found">
                  <img src="<?php echo base_url(); ?>dist/img/empty/cms.svg">
                  <p>You are using the simple homepage. <br>You can build a content-rich homepage by using our homepage builder.</p>
                  <a href="create.html" class="btn btn-primary">Build your custom homepage</a>
                </div>
                <!-- cms end -->
                
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>