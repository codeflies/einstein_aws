  
<?php $this->load->view('include/header2');?>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content certificates-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="slider-area p-3">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Certification</label>
                      <select class="form-control select2">
                        <option selected="selected">Classic</option>
                        <option>Fancy</option>
                        <option>Modern</option>
                        <option>Simple</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <ul class="nav nav-tabs" id="color-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="background-color-tab" data-toggle="pill" href="#background-color" role="tab" aria-controls="background-color" aria-selected="true">Background</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="template-crt-tab" data-toggle="pill" href="#template-crt" role="tab" aria-controls="template-crt" aria-selected="false">Template</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="color-tabContent">
                      <div class="tab-pane fade show active" id="background-color" role="tabpanel" aria-labelledby="background-color-tab">
                        <div class="certification-shape">
                          <div id="backgrounds-carousel" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <ul class="certifications-list">
                                  <li>
                                    <div class="file-upload">
                                      <div class="image-upload-wrap">
                                        <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                        <div class="drag-text">
                                          <h3>Drag and drop a file or select add Image</h3>
                                        </div>
                                      </div>
                                      <div class="file-upload-content">
                                        <img class="file-upload-image" src="#" alt="your image" />
                                        <div class="image-title-wrap">
                                          <button type="button" onclick="removeUpload()" class="remove-image btn btn-xs btn-danger">Remove <span class="image-title">Image</span></button>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li><a href="#" class="thumbnail thumbnail-selected"><img src="<?php echo base_url(); ?>dist/img/certificates/classic.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/fancy.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/modern.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/simple.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new02.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new03.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new04.png"></a></li>
                                </ul>
                              </div>
                              <div class="carousel-item">
                                <ul class="certifications-list">
                                  <li><a href="#" class="thumbnail"><img src=".<?php echo base_url(); ?>/dist/img/certificates/classic.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/fancy.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/modern.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/simple.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new02.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new03.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new04.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/modern.png"></a></li>
                                </ul>
                              </div>
                              <div class="carousel-item">
                                <ul class="certifications-list">
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/classic.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/fancy.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/modern.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/simple.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new02.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new03.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/new04.png"></a></li>
                                  <li><a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/dist/img/certificates/modern.png"></a></li>
                                </ul>
                              </div>
                            </div>
                            <a class="carousel-control-prev" href="#backgrounds-carousel" role="button" data-slide="prev">
                              <span class="carousel-control-custom-icon" aria-hidden="true">
                                <i class="fas fa-angle-left"></i>
                              </span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#backgrounds-carousel" role="button" data-slide="next">
                              <span class="carousel-control-custom-icon" aria-hidden="true">
                                <i class="fas fa-angle-right"></i>
                              </span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                          <!-- /.form-group -->
                        </div>
                      </div>
                      <!-- end -->
                      <div class="tab-pane fade" id="template-crt" role="tabpanel" aria-labelledby="template-crt-tab">
                        <div class="col-sm-12 p-3">
                          <textarea id="codeMirrorDemo" class="p-3">
                            <div class="info-box bg-gradient-info">
                              <span class="info-box-icon"><i class="far fa-bookmark"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Javascript</span>
                              </div>
                            </div>
                          </textarea>

                          <div class="template-controls-list">
                            <a href="javascript:void(0);" class="label">Organization</a>
                            <a href="javascript:void(0);" class="label">First name</a>
                            <a href="javascript:void(0);" class="label">Last name</a>
                            <a href="javascript:void(0);" class="label">Email</a>
                            <a href="javascript:void(0);" class="label">Course</a>
                            <a href="javascript:void(0);" class="label">Average course score</a>
                            <a href="javascript:void(0);" class="label">Issue date</a>
                            <a href="javascript:void(0);" class="label">Latest issue date</a>
                            <a href="javascript:void(0);" class="label">Expiration date</a>
                            <a href="javascript:void(0);" class="label">Instructor name</a>
                            <a href="javascript:void(0);" class="label">Unique identifier</a>
                          </div>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- end -->
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="dropdown-divider mt-0 mb-3"></div>
                    <div class="form-group mb-0">
                      <div class="row">
                        <div class="col-sm-6">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-certificates">Preview</button>
                          <button type="button" class="btn btn-primary">Update</button>
                          <button type="button" class="btn btn-primary">Save as new</button>
                        </div>
                        <div class="col-sm-6 text-right mobile-none">
                          <button type="button" class="btn btn-default" disabled="">Reset to default template</button>
                          <button type="button" class="btn btn-default" disabled=""><i class="fa fa-trash"></i> Delete</button>
                        </div>
                      </div>
                    </div>
                    <!-- end -->
                  </div>
                </div>
                <!-- end -->                

              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<script>
  $(function () {

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    // Summernote
    $('#summernote').summernote()

    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
      mode: "htmlmixed",
      theme: "monokai"
    });
    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo1"), {
      mode: "htmlmixed",
      theme: "monokai"
    });
  });



  function readURL(input) {
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
  });
  $('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
});

</script>

   <?php $this->load->view('include/footer2');?>