<?php $this->load->view('include/header2');?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content gamification-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="gamification-sec p-3">
                <div class="card-tbl-head mb-3">
                  <div class="row">
                    <div class="col-sm-6">
                      <i class="fa fa-info-circle"></i> Click on any badge to modify it. The image must be 200X200 pixels.
                    </div>
                    <div class="col-sm-6 text-right mobile-none input-group">
                      Choose your default set &nbsp; <select class="form-control select2">
                              <option selected="selected">Old School</option>
                              <option>Sticker</option>
                              <option>Evolution</option>
                            </select>
                    </div>
                  </div>
                </div>
                <div class="userreport-badges-content">
                  <h6 class="bg-grey p-3">ACTIVITY BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(4 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(8 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(16 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(32 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(64 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(128 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(256 logins)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-activity-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt mobile-none"><strong>Activity Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para mobile-none">(512 logins)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">LEARNING BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(1 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 completed course)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-learning-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Learning Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 completed course)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">TEST BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 passed tests)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-test-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Test Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(256 passed tests)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">ASSIGNMENT BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(1 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 passed assignments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-assignment-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Assignment Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 passed assignments)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">PERFECTIONISM BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(1 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-2"><img src="/<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 passed tests or assignments with score 90%+)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-perfectionism-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Perfectionism Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 passed tests or assignments with score 90%+)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">SURVEY BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(1 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 completed survey)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-survey-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Survey Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 completed survey)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">COMMUNICATION BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 discussion topics or comments)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-communication-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Communication Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(256 discussion topics or comments)</div>
                      </li>
                    </ul>
                  </div>
                  <h6 class="bg-grey p-3 mb-3">CERTIFICATION BADGES</h6>
                  <div class="badge-list activity-list">
                    <ul>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-1"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Newbie</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(1 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-2"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Grower</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(2 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-3"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Adventurer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(4 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-4"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Explorer</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(8 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-5"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Star</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(16 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-6"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Superstar</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(32 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-7"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Master</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(64 certifications)</div>
                      </li>
                      <li class="badge-list-box">
                        <div class="badge-gr-img badge-certification-8"><img src="<?php echo base_url(); ?>/dist/img/download-badges.png"></div>
                        <div class="badge-list-txt"><strong>Certification Grandmaster</strong> <i class="fa fa-pen txt-edit"></i></div>
                        <div class="badge-list-para">(128 certifications)</div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php $this->load->view('include/footer2');?>