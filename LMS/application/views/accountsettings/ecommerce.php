<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <div class="container">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1>Account & Settings</h1>
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
	              <li class="breadcrumb-item active">Account & Settings</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

	    <!-- Main content -->
	    <section class="content notifications-content pb-3">
	      <div class="container">
	        <div class="card card-primary">
	          <div class="card-header">
	            <h3 class="card-title">Account & Settings</h3>
	          </div>
	          <!-- /.card-header -->
	          <!-- form start -->
	          <form>
	            <div class="card-body pl-0 pr-0">
	              <nav class="navbar navbar-expand p-0">
	                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
	                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
	                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
	                </ul>
	              </nav>
	              <div class="dropdown-divider mt-0 mb-3"></div>
	              	<div class="user-notifications p-3">
					<div class="row">
						<div class="col-sm-12">
							<h5 class="p-2 mb-3 bg-grey">E-COMMERCE PROCESSOR</h5>
						</div>
	                 	<div class="col-sm-6">
		                    <div class="form-group">
		                        <select class="form-control select2">
		                          <option selected="selected">Select your e-commerce processor</option>
		                          <option>Paypal</option>
		                          <option>Stripe</option>
		                        </select>
		                    </div>
		                    <div class="form-group">
			                    <div class="input-group">
			                    	<input type="text" class="form-control" placeholder="Paypal email address" /> &nbsp;&nbsp;<i class="fa fa-info-circle"></i>
			                    </div>
			                </div>
		                    <div class="form-group">
		                    	<img src="<?php echo base_url(); ?>/dist/img/stripe-connect.png" />
		                    </div>
	                  	</div>
	                </div>
	                <div class="row">
	                	<div class="col-sm-12">
							<h5 class="p-2 mb-3 bg-grey">BEHAVIOR</h5>
						</div>
	                  	<div class="col-sm-12">
		                    <div id="accordion1">
		                      <div class="card-header pl-2 br-0">
		                        <h4 class="card-title w-100">
		                          <span for="restrict" data-toggle="collapse" href="#subscription"><i class="fa fa-map-pin"></i> Subscription</span>
		                        </h4>
		                      </div>
		                      <div id="subscription" class="collapse bg-grey" data-parent="#accordion1">
		                        <div class="p-3">
		                            <div class="form-group">
		                              	<div class="form-check">
			                                <input type="checkbox" class="form-check-input" id="enforceafter"  disabled="">
			                                <label class="form-check-label" for="enforceafter">Access to all paid courses at a charge of</label>
			                                <div class="input-group mb-3">
			                                  <input type="text" class="form-control" disabled="">
			                                  <div class="input-group-append">
			                                    <span class="input-group-text">$</span>
			                                  </div>&nbsp;<span style="vertical-align: middle;"> Paid </span>&nbsp;
			                                  <input type="text" class="form-control" disabled="">
			                                  <div class="input-group-append">
			                                    <span class="input-group-text">minutes</span>
			                                  </div> &nbsp;&nbsp; <i class="fa fa-info"></i>
			                                </div>
		                              	</div>
		                            </div>
		                            <!-- end -->
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="enforce" disabled="">
		                                <label class="form-check-label" for="enforce">Free for the first month &nbsp;<i class="fa fa-info"></i></label>
		                              </div>
		                            </div>
		                            <!-- end -->
		                        </div>
		                      </div>
		                    </div> 
		                    <!-- end -->
		                    <div id="accordion2">
		                      <div class="card-header pl-2 br-0">
		                        <h4 class="card-title w-100">
		                          <span for="restrict" data-toggle="collapse" href="#discounts"><i class="fa fa-eraser"></i> Discounts</span>
		                        </h4>
		                      </div>
		                      <div id="discounts" class="collapse bg-grey" data-parent="#accordion2">
		                        <div class="col-sm-12 p-3">
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="paid">
		                                <label class="form-check-label" for="paid">Discount all paid courses/categories/groups by &nbsp;</label>
		                              </div>
		                            </div>
		                            <div class="form-check">
		                                <div class="input-group mb-3">
		                                  <input type="text" class="form-control" >
		                                  <div class="input-group-append">
		                                    <span class="input-group-text">%</span>
		                                  </div>
		                                </div>
	                              	</div>
		                            <!-- end -->
		                        </div>
		                      </div>
		                    </div> 
		                    <!-- end -->
		                    <div id="accordion3">
		                    	<div class="card-header pl-2 br-0">
			                        <h4 class="card-title w-100">
			                          <span for="restrict" data-toggle="collapse" href="#invoices"><i class="fa fa-copy"></i> Invoices</span>
			                        </h4>
		                    	</div>
		                    	<div id="invoices" class="collapse bg-grey" data-parent="#accordion3">
			                        <div class="col-sm-12 p-3">
			                            <div class="form-group">
			                              <div class="form-check">
			                                <input type="checkbox" class="form-check-input" id="issue">
			                                <label class="form-check-label" for="issue">Issue invoices</label>
			                              </div>
			                            </div>
			                            <div class="form-group">
			                                <textarea class="form-control" rows="3" placeholder="Add your business info here. This info will be added to end-user invoices."></textarea>
		                              	</div>
			                            <!-- end -->
			                        </div>
		                    	</div>
		                    </div> 
		                    <!-- end -->
		                    <div id="accordion4">
		                      <div class="card-header pl-2 br-0">
		                        <h4 class="card-title w-100">
		                          <span for="restrict" data-toggle="collapse" href="#socialoption"><i class="fas fa-star"></i> Coupons</span>
		                        </h4>
		                      </div>
		                      <div id="socialoption" class="collapse bg-grey" data-parent="#accordion1">
		                        <div class="p-3">
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="enforcepass">
		                                <label class="form-check-label" for="enforcepass">Allow signups through social media</label>
		                                <div class="mb-3">
		                                  <div class="form-check">
		                                    <input type="checkbox" class="form-check-input" id="facebook">
		                                    <label class="form-check-label" for="facebook">Facebook</label>
		                                  </div>
		                                  <div class="form-check">
		                                    <input type="checkbox" class="form-check-input" id="facebook">
		                                    <label class="form-check-label" for="facebook">Google</label>
		                                  </div>
		                                  <div class="form-check">
		                                    <input type="checkbox" class="form-check-input" id="facebook">
		                                    <label class="form-check-label" for="facebook">Linkedin</label>
		                                  </div>
		                                </div>
		                              </div>
		                            </div>
		                            <!-- end -->
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="allowinte">
		                                <label class="form-check-label" for="allowinte">Allow interactions with social media in course catalog &nbsp;<i class="fa fa-info"></i></label>
		                              </div>
		                            </div>
		                            <!-- end -->
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="allowlinked">
		                                <label class="form-check-label" for="allowlinked">Allow sharing certifications on LinkedIn &nbsp;<i class="fa fa-info"></i></label>
		                              </div>
		                            </div>
		                            <!-- end -->
		                            <div class="form-group">
		                              <div class="form-check">
		                                <input type="checkbox" class="form-check-input" id="allowrating">
		                                <label class="form-check-label" for="allowrating">Allow course rating &nbsp;<i class="fa fa-info"></i></label>
		                              </div>
		                            </div>
		                            <!-- end -->
		                        </div>
		                      </div>
		                    </div> 
		                    <!-- end -->
		                    <div id="accordion5">
		                      	<div class="card-header pl-2 br-0">
			                        <h4 class="card-title w-100">
			                          <span for="restrict" data-toggle="collapse" href="#credits"><i class="fas fa-circle"></i> Credits</span>
			                        </h4>
		                      	</div>
		                    	<div id="credits" class="collapse bg-grey" data-parent="#accordion5">
			                        <div class="col-sm-12 p-3">
			                            <div class="form-group">
			                              <div class="form-check">
			                                <input type="checkbox" class="form-check-input" id="issue">
			                                <label class="form-check-label" for="issue">Issue invoices</label>
			                              </div>
			                            </div>
			                            <div class="form-group">
			                                <textarea class="form-control" rows="3" placeholder="Add your business info here. This info will be added to end-user invoices."></textarea>
		                              	</div>
			                            <!-- end -->
			                        </div>
		                    	</div>
		                    </div> 
		                    <!-- end -->
	                 	</div>

	                </div>
	                <div class="dropdown-divider mt-0 mb-3"></div>
	                <div class="form-group mb-0">
	                  <div class="row">
	                    <div class="col-sm-6">
	                      <button type="button" class="btn btn-primary">Save</button> <span>or</span> <a href="#">Cancel</a>
	                    </div>
	                    <div class="col-sm-6 text-right mobile-none">
	                    	<button type="button" class="btn btn-primary">E-commerce timeline</button>
	                    </div>
	                  </div>
	                </div>
	                <!-- end -->
	              </div>
	              <!-- Tabs end -->
	            </div>
	            <!-- /.card-body -->
	          </form>
	        </div>
	      </div>
	      <!-- /.container-fluid -->
	    </section>
	    <!-- /.content -->
	</div>

	 <?php $this->load->view('include/footer2');?>