<?php $this->load->view('include/header2');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/basic_index">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href=" <?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class=" p-3">
                <h5 class="mb-2 bg-grey p-2">IDENTITY</h5>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="bt-name">Site Name</label>
                      <input type="text" class="form-control" id="bt-name" placeholder="" />
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="bt-title">Site description</label>
                      <input type="text" class="form-control" id="bt-title" placeholder="" />
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="exampleInputFile">Select Image</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="exampleInputFile">Select Favicon</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- end -->
                <h5 class="mt-4 bg-grey p-2">LOCALE</h5>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Default language</label>
                      <div class="input-group">
                        <select class="form-control select2">
                          <option selected="selected">English</option>
                          <option>Hindi</option>
                        </select> &nbsp;&nbsp;&nbsp;
                        <a href="#" data-toggle="modal" data-target="#modal-language" title="Overrides"><i class="fa fa-pen"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Default time zone</label>
                      <div class="input-group date" id="timepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker"/>
                        <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Date format</label>
                      <select class="form-control select2">
                        <option selected="selected">DD/MM/YYYYY</option>
                        <option>YYYY/MM/DD</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Currency</label>
                      <select class="form-control select2">
                        <option selected="selected">US Dollar</option>
                        <option>Euro</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- end -->
                <h5 class="mt-4 mb-3 bg-grey p-2">ANNOUNCEMENT</h5>
                <ul class="nav nav-tabs" id="custom-ann" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-internal-tab" data-toggle="pill" href="#custom-internal" role="tab" aria-controls="custom-internal" aria-selected="true">Internal</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-external-tab" data-toggle="pill" href="#custom-external" role="tab" aria-controls="custom-external" aria-selected="false">External</a>
                  </li>
                </ul>
                <div class="tab-content" id="custom-annContent">
                  <div class="tab-pane fade show active" id="custom-internal" role="tabpanel" aria-labelledby="custom-internal-tab">
                    <div class="col-sm-12">
                      <div class="form-group pt-3">
                        <label for="description">Announcement</label>
                        <textarea class="form-control" rows="4" id="escription" placeholder=""></textarea>
                      </div>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="tab-pane fade" id="custom-external" role="tabpanel" aria-labelledby="custom-external-tab">
                    <div class="col-sm-12">
                      <div class="form-group pt-3">
                        <label for="ann-description">Announcement</label>
                        <textarea class="form-control" rows="4" id="ann-escription" placeholder=""></textarea>
                      </div>
                    </div>
                    <!-- /.form-group -->
                  </div>
                </div>
                <!-- end -->

                <h5 class="mt-4 bg-grey p-2">SECURITY</h5>
                <div class="pl-2">
                  <div class="form-check API">
                    <input type="checkbox" class="form-check-input" id="user-deactive">
                    <label class="form-check-label" for="user-deactive">Enable API <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="apibox">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="API" />
                      </div>
                      <small class="fas fa-retweet"></small>  <a href="#">Download API documentation and PHP library</a>
                    </div>
                  </div>
                  <div class="form-check xAPI">
                    <input type="checkbox" class="form-check-input" id="xAPI" onclick="checkbox()">
                    <label class="form-check-label" for="xAPI">Enable xAPI <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="xapibox">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="API" />
                      </div>
                      <small class="fas fa-retweet"></small>  <a href="#">Download API documentation</a>
                    </div>
                  </div>
                  <div class="form-check rdomain">
                    <input type="checkbox" class="form-check-input" id="rdomain">
                    <label class="form-check-label" for="rdomain">Restrict registration to specific domains <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="rdomainbox">
                      <div class="input-group">
                        <input type="url" class="form-control" placeholder="Domain" />
                      </div>
                    </div>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="multilogin">
                    <label class="form-check-label" for="multilogin">Disallow multiple logins from the same user &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="accessapp">
                    <label class="form-check-label" for="accessapp">Disallow access from the mobile APP &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check idverify">
                    <input type="checkbox" class="form-check-input" id="idverify">
                    <label class="form-check-label" for="idverify">Intercom identity verification <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="idverifybox">
                      <div class="input-group">
                        <label for="skey">Secret key &nbsp;</label>
                        <input type="text" class="form-control" id="skey" placeholder="" />
                      </div>
                    </div>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="dvideo">
                    <label class="form-check-label" for="dvideo">Disable video transcoding service &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="slogo">
                    <label class="form-check-label" for="slogo">Add system logo as watermark on uploaded videos &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="usemodern">
                    <label class="form-check-label" for="usemodern">Use modern viewer for documents &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="alframe">
                    <label class="form-check-label" for="alframe">Allow application to be loaded in a frame &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="essl">
                    <label class="form-check-label" for="essl">Enforce SSL &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check locksystem">
                    <input type="checkbox" class="form-check-input" id="locksystem">
                    <label class="form-check-label" for="locksystem">Lock system <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="locksystembox">
                      <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" placeholder="The system is currently locked"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="searchengines">
                    <label class="form-check-label" for="searchengines">Hide from search engines &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                </div>
                <!-- end -->
                
                <h5 class="mt-4 bg-grey p-2">COURSE SETTINGS</h5>
                <div class="form-group pl-2">
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="coursecatalog">
                    <label class="form-check-label" for="coursecatalog">External course catalog &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="branchcourses">
                    <label class="form-check-label" for="branchcourses">Show branch courses in main course catalog &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="coursesummary">
                    <label class="form-check-label" for="coursesummary">Show course summary upon entering a course &nbsp;<small class="fa fa-info-circle"></small></label>
                  </div>
                </div>
                <!-- end -->
                <h5 class="mt-4 bg-grey p-2">CONFERENCES</h5>
                <div class="form-group">
                  <label>Type</label>
                  <select class="form-control select2">
                    <option selected="selected">Integrated video conference (no Flash)</option>
                    <option>Integrated video conference</option>
                    <option>Integrated video conference (no Flash)</option>
                  </select>
                </div>
                <!-- end -->

                <h5 class="mt-4 bg-grey p-2">BAMBOOHR INTEGRATION</h5>
                <div class="form-group pl-2">
                  <div class="form-check bamboohr">
                    <input type="checkbox" class="form-check-input" id="bamboohr">
                    <label class="form-check-label" for="bamboohr">Enable BambooHR integration <small class="fa fa-info-circle"></small></label>
                    <div class="form-group col-sm-6 mt-2"style="display: none;" id="bamboohrbox">
                      <div class="input-group">
                        <input type="url" class="form-control" placeholder="Domain">
                        <div class="input-group-prepend">
                          <span class="input-group-text">.bamboohr.com</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end -->

                <h5 class="mt-4 bg-grey p-2">CUSTOM FIELDS</h5>
                <div id="accordion">
                  <div class="card-header pl-2  br-0">
                    <h4 class="card-title w-100">
                      <span for="restrict" data-toggle="collapse" href="#coursefields"><i class="fa fa-book"></i> Custom course fields &nbsp;<small class="fa fa-info-circle"></small></span>
                    </h4>
                  </div>
                  <div id="coursefields" class="collapse bg-grey p-3" data-parent="#accordion">
                    <div class="col-sm-12">
                      <div class="btn-group mb-3">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-custom-course">Add field</a>
                      </div>
                      <div class="table-responsive">
                        <table id="example1" class="table table-striped table-hover course-table">
                          <thead class="bg-primary">
                            <tr>
                              <th>Name</th>
                              <th>Type</th>
                              <th>Mandatory</th>
                              <th>Visible on reports</th>
                              <th>Visible to learners</th>
                              <th>Selective availability</th>
                              <th>Option</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Advanced</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>
                                <i class="fa fa-ellipsis-h"></i>
                                <div class="hover-tbl-btn">
                                  <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                                  <a href="#" class="tbl-btn tbl-logout" title="Clone"><i class="fas fa-magnet"></i></a>
                                  <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Advanced</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>
                                <i class="fa fa-ellipsis-h"></i>
                                <div class="hover-tbl-btn">
                                  <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                                  <a href="#" class="tbl-btn tbl-clone" title="Clone"><i class="fas fa-magnet"></i></a>
                                  <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="modal fade" id="modal-custom-course" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Add field</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="form-group">
                                <label for="">Name</label>
                                <div class="input-group mb-3">
                                  <input type="text" class="form-control">
                                  <div class="input-group-append">
                                    <span class="input-group-text">80</span>
                                  </div>
                                </div>
                              </div>
                              <!-- group end -->
                              <div class="form-group">
                                <label for="">Type</label>
                                <select class="form-control select3" style="width: 100%">
                                  <option selected="selected">Text</option>
                                  <option>Dropdown</option>
                                  <option>Checkbox</option>
                                  <option>Date</option>
                                </select>
                              </div>
                              <!-- group end -->
                              <div class="form-group">
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="mdtry">
                                  <label class="form-check-label" for="mdtry">Mandatory</label>
                                </div>
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="vson">
                                  <label class="form-check-label" for="vson">Visible on reports</label>
                                </div>
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="vlon">
                                  <label class="form-check-label" for="vlon">Visible on learners</label>
                                </div>
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="avon">
                                  <label class="form-check-label" for="avon">Available only on</label>
                                </div>
                              </div>
                              <!-- group end -->
                            </div>
                            <div class="modal-footer justify-content-center">
                              <button type="button" class="btn btn-primary">Add Field</button> <span>or</span> <a href="#">cancel</a>
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                    </div>
                  </div>
                </div> 
                <div id="accordion1">
                  <div class="card-header pl-2  br-0">
                    <h4 class="card-title w-100">
                      <span for="restrict" data-toggle="collapse" href="#userfields"><i class="fa fa-user"></i> Custom user fields &nbsp;<small class="fa fa-info-circle"></small></span>
                    </h4>
                  </div>
                  <div id="userfields" class="collapse bg-grey p-3" data-parent="#accordion1">
                    <div class="col-sm-12">
                      <div class="btn-group mb-3">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-custom-user">Add field</a>
                      </div>
                      <div class="table-responsive">
                        <table id="example3" class="table table-striped table-hover course-table">
                          <thead class="bg-primary">
                            <tr>
                              <th>Name</th>
                              <th>Type</th>
                              <th>Mandatory</th>
                              <th>Visible on reports</th>
                              <th>Selective availability</th>
                              <th>Option</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Advanced</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>
                                <i class="fa fa-ellipsis-h"></i>
                                <div class="hover-tbl-btn">
                                  <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                                  <a href="#" class="tbl-btn tbl-logout" title="Clone"><i class="fas fa-magnet"></i></a>
                                  <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Advanced</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>Simple</td>
                              <td>
                                <i class="fa fa-ellipsis-h"></i>
                                <div class="hover-tbl-btn">
                                  <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                                  <a href="#" class="tbl-btn tbl-clone" title="Clone"><i class="fas fa-magnet"></i></a>
                                  <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="modal fade" id="modal-custom-user" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Add field</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" name="" id="" class="form-control" />
                              </div>
                              <!-- group end -->
                              <div class="form-group">
                                <label for="">Type</label>
                                <select class="form-control select3" style="width: 100%">
                                  <option selected="selected">Text</option>
                                  <option>Dropdown</option>
                                  <option>Checkbox</option>
                                  <option>Date</option>
                                </select>
                              </div>
                              <!-- group end -->
                              <div class="form-group">
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="mdtry">
                                  <label class="form-check-label" for="mdtry">Mandatory</label>
                                </div>
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="vson">
                                  <label class="form-check-label" for="vson">Visible on reports</label>
                                </div>
                                <div class="form-check">
                                  <input type="checkbox" class="form-check-input" id="avon">
                                  <label class="form-check-label" for="avon">Available only on</label>
                                </div>
                              </div>
                              <!-- group end -->
                            </div>
                            <div class="modal-footer justify-content-center">
                              <button type="button" class="btn btn-primary">Add Field</button> <span>or</span> <a href="#">cancel</a>
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                    </div>
                  </div>
                </div> 
                <!-- end -->

              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <?php $this->load->view('include/footer2');?>