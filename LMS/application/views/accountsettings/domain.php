<?php $this->load->view('include/header2');?>


 <div class="content-wrapper domain-content">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content notifications-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-0"></div>
              <div class="domain-contentin p-3">
                <div class="form-group">
                  <div class="input-group">
                    <label for="doamin_input">Domain name</label>
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" id="doamin_input">
                    <div class="input-group-append">
                      <span class="input-group-text">.talentlms.com</span>
                    </div>
                    &nbsp;<i class="fa fa-info-circle"></i>
                  </div>
                </div>
                <!-- end -->
                <div class="dropdown-divider mt-0 mb-3"></div>
                <div class="form-group">
                  <div class="row justify-content-between">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Change domain name</button> <span>or</span> <a href="#">Cancel</a>
                    </div>
                    <div class="col-sm-6 text-right mobile-none">
                      <button type="button" class="btn btn-primary">Set external domain mapping</button>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

     <?php $this->load->view('include/footer2');?>