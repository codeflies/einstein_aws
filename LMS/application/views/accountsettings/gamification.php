 
<?php $this->load->view('include/header2');?>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account & Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Account & Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content gamification-content pb-3">
      <div class="container">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Account & Settings</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form>
            <div class="card-body pl-0 pr-0">
              <nav class="navbar navbar-expand p-0">
                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/basic">Notifications</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/cms">Homepage</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/users">Users</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/themes">Themes</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/certifications">Certifications</a></li>
                  <li class="nav-item"><a class="nav-link active" href="<?php echo base_url(); ?>accountsettings/gamification">Gamification</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/ecommerce">E-commerce</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/domain">Domain</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>accountsettings/subscription">Subscription</a></li>
                </ul>
              </nav>
              <div class="dropdown-divider mt-0 mb-3"></div>
              <div class="gamification-sec p-3">
                <div class="gamification-head text-right" id="allclose">
                  <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch />
                </div>
                <div class="gamification-body">
                  <fieldset>
                    <div class="switch-head bg-grey mb-3 p-2" id="pointstop">
                      <h5 class="d-inline">POINTS</h5>&nbsp; <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch />
                    </div>
                    <div class="switch-body pl-3">
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="login">
                        <label class="form-check-label" for="login">Each login gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="unit">
                        <label class="form-check-label" for="unit">Each unit completion gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="completion">
                        <label class="form-check-label" for="completion">Each course completion gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="certification">
                        <label class="form-check-label" for="certification">Each certification gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="test">
                        <label class="form-check-label" for="test">Each successful test completion gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="assignment">
                        <label class="form-check-label" for="assignment">Each successful assignment completion gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="successful">
                        <label class="form-check-label" for="successful">Each successful ILT completion gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="discussion">
                        <label class="form-check-label" for="discussion">Each discussion topic or comment gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="upvote">
                        <label class="form-check-label" for="upvote">Each upvote on discussion comments gives</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>
                      </div>
                    </div>
                  </fieldset>
                  <!-- fieldset end -->
                  <fieldset>
                    <div class="switch-head bg-grey mb-3 p-2 pb-3" id="badgesstop">
                      <h5 class="d-inline">BADGES</h5>&nbsp; <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch />

                      <a href="gamification_customizebadges.html" class="btn btn-default float-right d-inline"><i class="fa fa-certificate"></i> Customize badges</a>
                    </div>
                    <div class="switch-body pl-3">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges1">
                        <label class="form-check-label" for="badges1">Activity badges (4, 8, 16, 32, 64, 128, 256, 512 logins)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges2">
                        <label class="form-check-label" for="badges2">Learning badges (1, 2, 4, 8, 16, 32, 64, 128 completed courses)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges3">
                        <label class="form-check-label" for="badges3">Test badges (2, 4, 8, 16, 32, 64, 128, 256 passed tests)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges4">
                        <label class="form-check-label" for="badges4">Assignment badges (1, 2, 4, 8, 16, 32, 64, 128 passed assignments)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges5">
                        <label class="form-check-label" for="badges5">Perfectionism badges (1, 2, 4, 8, 16, 32, 64, 128 tests or assignments with score 90%+)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges6">
                        <label class="form-check-label" for="badges6">Survey badges (1, 2, 4, 8, 16, 32, 64, 128 completed surveys)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges7">
                        <label class="form-check-label" for="badges7">Communication badges (2, 4, 8, 16, 32, 64, 128, 256 topics or comments)</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="badges8">
                        <label class="form-check-label" for="badges8">Certification badges (1, 2, 4, 8, 16, 32, 64, 128 certifications)</label>
                      </div>
                    </div>
                  </fieldset>
                  <!-- fieldset end -->
                  <fieldset>
                    <div class="switch-head bg-grey mb-3 p-2">
                      <h5 class="d-inline">LEVELS</h5>&nbsp; <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch />
                    </div>
                    <div class="switch-body pl-3">
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="level1">
                        <label class="form-check-label" for="level1">Upgrade level every</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>         
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="checkbox" class="form-check-input" id="level2">
                        <label class="form-check-label" for="level2">Upgrade level every</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">completed courses</span>
                          </div>         
                      </div>
                      <div class="form-check input-group">
                        <input type="checkbox" class="form-check-input" id="level3">
                        <label class="form-check-label" for="level3">Upgrade level every</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">badges</span>
                          </div>                          
                      </div>
                    </div>
                  </fieldset>
                  <!-- fieldset end -->
                  <fieldset>
                    <div class="switch-head bg-grey mb-3 p-2">
                      <h5 class="d-inline">REWARDS</h5>&nbsp; <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch  />
                    </div>
                    <div class="switch-body pl-3">
                      <div class="form-check input-group mb-2">
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">%</span>
                          </div>&nbsp;&nbsp; 
                        <input type="checkbox" class="form-check-input" id="discountthan">
                        <label class="form-check-label" for="discountthan">discount for course purchases with more than</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">points</span>
                          </div>     
                      </div>
                      <div class="form-check input-group mb-2">
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">%</span>
                          </div>&nbsp;&nbsp;
                        <input type="checkbox" class="form-check-input" id="discountcourse">
                        <label class="form-check-label" for="discountcourse">discount for course purchases with more than</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">badges</span>
                          </div>     
                      </div>
                      <div class="form-check input-group mb-2">
                        <input type="text" class="">
                        <div class="input-group-append">
                          <span class="input-group-text">%</span>
                        </div>&nbsp;&nbsp;
                        <input type="checkbox" class="form-check-input" id="discountlevel">
                        <label class="form-check-label" for="discountlevel">discount for course purchases on level</label>&nbsp;&nbsp;
                          <input type="text" class="">
                          <div class="input-group-append">
                            <span class="input-group-text">upwards</span>
                          </div>     
                      </div>
                    </div>
                  </fieldset>
                  <!-- fieldset end -->
                  <fieldset>
                    <div class="switch-head bg-grey mb-3 p-2" id="leaderboard">
                      <h5 class="d-inline">LEADERBOARD</h5>&nbsp; <input type="checkbox" name="my-checkbox" data-bootstrap-switch />
                    </div>
                    <div class="switch-body pl-3">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="showlevels">
                        <label class="form-check-label" for="showlevels">Show levels</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="showpoints">
                        <label class="form-check-label" for="showpoints">Show points</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="showbadges">
                        <label class="form-check-label" for="showbadges">Show badges</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="showcourses">
                        <label class="form-check-label" for="showcourses">Show courses</label>
                      </div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="showcertifications">
                        <label class="form-check-label" for="showcertifications">Show certifications</label>
                      </div>                        
                    </div>
                  </fieldset>
                  <!-- fieldset end -->
                </div>
                <div class="dropdown-divider mt-5 mb-3"></div>
                <div class="form-group mb-0">
                  <div class="row">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-primary">Save</button> <span>or</span> <a href="#">Cancel</a>
                    </div>
                    <div class="col-sm-6 text-right mobile-none">
                      <button type="button" class="btn btn-danger">Reset to default settings</button>
                      <button type="button" class="btn btn-danger">Reset statistics</button>
                    </div>
                  </div>
                </div>
                <!-- end -->
              </div>
              <!-- Tabs end -->
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
     <?php $this->load->view('include/footer2');?>