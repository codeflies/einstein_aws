<?php 
if(!isset($this->session->userdata["user_id"])) {
    redirect(base_url());
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo base_url(); ?>dist/img/favicon.ico" type="image" sizes="16x16">
  <title>Einsein Mind - LMS</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/select2/css/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- BS Stepper -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bs-stepper/css/bs-stepper.min.css">
  <!-- dropzonejs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/dropzone/min/dropzone.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/adminlte.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/local.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/snackbar.css">

   <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/hummingbird-treeview.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/hummingbird-treeview.css">
  <!-- datatable -->
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.3/dist/bootstrap-table.min.css">

</head>
<div id="snackbar"></div>
<div class="loading_wrapper">
        <div class="loading">
        <span class="loader-inner loader-one"></span>
        <span class="loader-inner loader-two"></span>
        <span class="loader-inner loader-three"></span>
        <span class="loader-inner loader-four"></span>
</div>
</div>
<!--
`body` tag options:

  Apply one or more of the following classes to to the body tag
  to get the desired effect

  * sidebar-collapse
  * sidebar-mini
-->
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <div class="container">
        <!-- Left navbar links -->
        <a href="<?php echo base_url(); ?>" class="brand-link">
          <img src="<?php echo base_url(); ?>dist/img/em-logo.png" alt="Logo" class="brand-image">
        </a>

        <div class="mobile-toggle" id="togglemenu">
          <i class="fa fa-bars"></i>
        </div>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Navbar Search -->
          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown">
              <span class="user-name" title=""><?php echo $this->session->userdata["domain_name"]; ?></span> Administrator <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item bg-grey">
                <div class="icheck-success d-inline">
                  <input type="radio" name="r3" checked="" id="radioSuccess1">
                  <label for="radioSuccess1">
                  </label>
                </div> Administrator
              </a>
              <a href="#" class="dropdown-item bg-grey">
                <div class="icheck-success d-inline">
                  <input type="radio" name="r3" id="radioSuccess2">
                  <label for="radioSuccess2">
                  </label>
                </div> Instructor
              </a>
              <a href="#" class="dropdown-item bg-grey">
                <div class="icheck-success d-inline">
                  <input type="radio" name="r3" id="radioSuccess2">
                  <label for="radioSuccess2">
                  </label>
                </div> Learner
              </a>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url(); ?>/user/info/userinfo.html" class="dropdown-item">
                <i class="fas fa-address-card mr-2"></i> My info
              </a>
              <a href="pages/user/courses/courses.html" class="dropdown-item">
                <i class="fas fa-book mr-2"></i> My courses
              </a>
              <a href="#" class="dropdown-item">
                <i class="fas fa-certificate mr-2"></i> My certifications
              </a>
              <a href="<?php echo base_url(); ?>/reports/userinfo" class="dropdown-item">
                <i class="fas fa-server mr-2"></i> My progress
              </a>
              <a href="pages/user/groups/groups.html" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> My groups
              </a>
              <a href="<?php echo base_url(); ?>/branches/list" class="dropdown-item">
                <i class="fas fa-sitemap mr-2"></i> MY Branches
              </a>
              <a href="pages/user/files/files.html" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> MY Files
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown">Go To 
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="index.html" class="dropdown-item">
                <i class="fas fa-home mr-2"></i> Home
              </a>
              <a href="pages/user/user-list.html" class="dropdown-item">
                <i class="fas fa-user mr-2"></i> Users
              </a>
              <a href="<?php echo base_url(); ?>/courses/list" class="dropdown-item">
                <i class="fas fa-book mr-2"></i> Courses
              </a>
              <a href="pages/marketplace/einsteinslibrary_index.html" class="dropdown-item">
                <i class="fas fa-cart-arrow-down mr-2"></i> Course Store
              </a>
              <a href="<?php echo base_url(); ?>/category/list" class="dropdown-item">
                <i class="fas fa-server mr-2"></i> Categories
              </a>
              <a href="<?php echo base_url(); ?>/group/list" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> Groups
              </a>
              <a href="<?php echo base_url(); ?>/branches/list" class="dropdown-item">
                <i class="fas fa-sitemap mr-2"></i> Branches
              </a>
              <a href="<?php echo base_url(); ?>Reports/reports" class="dropdown-item">
                <i class="fas fa-chart-pie mr-2"></i> Reports
              </a>
              <a href="<?php echo base_url(); ?>/Importexport/Import" class="dropdown-item">
                <i class="fas fa-sync mr-2"></i> Import - Export
              </a>
              <a href="<?php echo base_url(); ?>/eventsengine/notification" class="dropdown-item">
                <i class="fas fa-clock mr-2"></i> Events engine
              </a>
              <a href="<?php echo base_url(); ?>/accountsettings/basic" class="dropdown-item">
                <i class="fas fa-sliders-h mr-2"></i> Account & Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="pages/user/create.html" class="dropdown-item dropdown-footer"><i class="fas fa-plus-circle mr-2"></i> Add user</a>
              <a href="pages/course/create.html" class="dropdown-item dropdown-footer"><i class="fas fa-plus-circle mr-2"></i> Add course</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link">Message 
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="<?php echo base_url(); ?>/message/inbox" class="dropdown-item">
                <i class="fas fa-inbox mr-2"></i> Go to Inbox
              </a>
              <a href="<?php echo base_url(); ?>/message/create" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> Send message
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">No messages</a>
            </div>
          </li>
          <li class="nav-item dropdown nav-item-help mobile-none">
            <a href="#" class="nav-link">Help <i class="fa fa-angle-down"></i></a>
            <div class="dropdown-menu dropdown-help dropdown-menu-right">
              <div class="row">
                <div class="col-5 col-sm-4">
                  <div class="nav flex-column nav-tabs " id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="base-tabs-tab" data-toggle="pill" href="#base-tabs" role="tab" aria-controls="base-tabs" aria-selected="true">Knowledge base </a>
                    <a class="nav-link" id="quick-tabs-tab" data-toggle="pill" href="#quick-tabs" role="tab" aria-controls="quick-tabs-tab" aria-selected="false">Quick guide</a>
                    <a class="nav-link" id="tour-tabs-tab" data-toggle="pill" href="#tour-tabs" role="tab" aria-controls="tour-tabs-tab"  aria-selected="false">Tour</a>
                    <a class="nav-link" id="videos-tabs-tab" data-toggle="pill" href="#videos-tabs" role="tab" aria-controls="videos-tabs-tab"  aria-selected="false">Videos</a>
                    <a class="nav-link" id="support-tabs-tab" data-toggle="pill" href="#support-tabs" role="tab" aria-controls="support-tabs" aria-selected="false">Contact support</a>
                    <a class="nav-link" id="live-tabs-tab" data-toggle="pill" href="#live-tabs" role="tab" aria-controls="live-tabs" aria-selected="false">Live support</a>
                  </div>
                </div>
                <div class="col-7 col-sm-8">
                  <div class="tab-content" id="vert-tabs-tabContent">
            <div class="tab-pane fade show active" id="base-tabs" role="tabpanel" aria-labelledby="base-tabs-tab">
              <div class="help-tab-search-box">
                <div class="search-box-head">
                  <input class="form-control" type="search" placeholder="Search Knowledge base" aria-label="Search">
                </div>
                <div class="search-box-body">
                  <ul id="help_tab_links" class="help_tab_links">
                    <li>
                      <div>
                        <a href="#" target="_blank">
                          <h2>Getting started with TalentLMS</h2>
                        </a>
                        <p>To <em>get</em> things moving, let’s walk you through your first steps and point you to a few useful resources along the way.</p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to set prerequisites for a course</h2></a>
                        <p>with eLearning” and “Content and TalentLMS”, then choose “<em>Getting</em> <em>Started</em> with eLearning” and “Content and TalentLMS” as</p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to use Postman to generate TalentLMS API code snippets in various programming languages</h2></a>
                        <p>You can click the bellow button to <em>get</em> <em>started</em> with Postman and our collection: ► Run in Postman</p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to work with the training matrix report</h2></a>
                        <p>Light blue: the user has not <em>started</em></p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to configure SCIM with Azure AD</h2></a>
                        <p>Click Provision from the left-side menu and then <em>Get</em> <em>started</em>.</p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to set up your Stripe payment gateway</h2></a>
                        <p>You can now <em>start</em> <em>getting</em> paid through Stripe's secure payment gateway.</p>
                      </div>
                    </li>
                    <li>
                      <div>
                        <a href="#" target="_blank"><h2>How to configure SSO with Okta</h2></a>
                        <p>To <em>get</em> <em>started</em>, you need an Okta account to handle the sign-in process and provide your users’ credentials to TalentLMS.</p>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="search-box-foot">
                  <a href="#" class="btn btn-primary" target="_blank">Go to knowledge base</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="quick-tabs" role="tabpanel" aria-labelledby="quick-tabs-tab">
                        <div class="header-quick-content">
                          <img src="<?php echo base_url(); ?>dist/img/ES_QuickGuide.svg" />
                          <p>An overview of basic functionality</p>
                          <a href="#" class="btn btn-primary">View quick guide</a>
                        </div>
            </div>
            <div class="tab-pane fade" id="tour-tabs" role="tabpanel" aria-labelledby="tour-tabs-tab">
                        <div class="header-tour-content">
                        <img src="<?php echo base_url(); ?>dist/img/ES_Tour.svg" />
                        <p>Restart the guided tour</p>
                        <a href="#" class="btn btn-primary">Restart tour</a>
                        </div> 
            </div>
            <div class="tab-pane fade" id="videos-tabs" role="tabpanel" aria-labelledby="videos-tabs-tab">
                        <div class="header-video-content">
                          <img src="<?php echo base_url(); ?>dist/img/ES_Videos.svg" />
                          <p>A vast collection of introductory videos</p>
                          <a href="#" class="btn btn-primary">View videos</a>
                        </div> 
            </div>
            <div class="tab-pane fade" id="support-tabs" role="tabpanel" aria-labelledby="support-tabs-tab">
              <div class="help-support-content">
                <form>
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Subject">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" rows="4" placeholder="Give us your feedback or ask for help"></textarea>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="exampleInputFile">
                                  <label class="custom-file-label" for="exampleInputFile">Upload a file</label>
                                </div>
                              </div>
                  </div>
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Next</button>
                  </div>
                </form>
              </div>
                      </div>
                      <div class="tab-pane fade" id="live-tabs" role="tabpanel" aria-labelledby="live-tabs-tab">
                        <div class="header-live-content">
                          <img src="<?php echo base_url(); ?>dist/img/LIVE_SUPPORT.svg" />
                          <p>Live support is only available on Premium plans</p>
                          <a href="#" class="btn btn-primary">Upgrade</a>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item mobile-none">
            <a class="nav-link" data-widget="navbar-search" href="#" role="button">
              <i class="fas fa-search"></i>
            </a>
            <div class="navbar-search-block">
              <form class="form-inline">
                <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>
          <li class="nav-item dropdown mobile-none">
            <a href="<?php echo base_url();?>user/logout" class="nav-link"><i class="fas fa-sign-out-alt"></i></a>
          </li>
          <li class="nav-item dropdown desktop-none">
            <a href="<?php echo base_url(); ?>user/logout" class="nav-link">Logout</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->
