<?php $this->load->view('include/header2');?>

<style>
.error_msg{
    color:red;
}

.custom-droup-down .custom-droup-in {
   display: block;
   background-color: #fafafa;
   border-radius: 3px;
   padding: 7px 11px;
   cursor: pointer;
   position: relative;
}

.custom-droup-down .custom-droup-in:after {
   content: "";
   position: absolute;
   right: 10px;
   top: 14px;
   border: 5px solid transparent;
   border-top: 5px solid #8d8d8d;
   height: 0px;
   width: 0px;
}

.custom-droup-down .custom-droup-down-inner {
   margin: 0;
   padding: 10px 0px;
   list-style-type: none;
   border: 1px solid #fafafa;
}

.custom-droup-down .custom-droup-down-inner>li {
   padding: 3px 10px;
   color: #a1a1a1;
}

.custom-droup-down .custom-droup-down-inner li span {
   display: block;
   padding: 4px 0px;
}

.custom-droup-down .custom-droup-down {
   margin: 0;
   padding: 0;
   list-style-type: none;
}

.custom-droup-down .custom-droup-down li {
   padding: 3px 10px;
   color: #2c2c2c;
   cursor: pointer;
}

.custom-droup-down .custom-droup-down li:hover {
   background-color: #6466f6;
   color: #ffffff;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"></section>

    <!-- Main content -->
    <section class="content branches-content pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header ">
                            <h3 class="card-title"><a href="<?php echo base_url(); ?>">Home / <a href="">Branches</a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php 
                         if (isset($result)) { ?>
                            <!-- branch not found end -->

                            <div class="card-tbl-head">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <a href="<?php echo base_url(); ?>branches/create"
                                                class="btn btn-primary">Add Branches</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-right mobile-none">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-default">Mass Actions</a>
                                            <button type="button" class="btn btn-default dropdown-toggle dropdown-icon"
                                                data-toggle="dropdown" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu" role="menu" style="">
                                                <a class="dropdown-item" onclick="add_courses()">Add a course
                                                    to all branches</a>
                                                <a class="dropdown-item" onclick="delete_courses()">Remove a course from all branches</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="table-responsive">
                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="branchListSearch" autocomplete="off">
                                </div>
                                <table id="branchListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="branchListAjax" data-show-columns="true" data-show-export="true"
                                    data-search-selector="#branchListSearch" data-checkbox-header="false"
                                    data-click-to-select="true" data-checkbox="true"
                                    data-buttons-prefix="btn-sm btn btn-success" data-pagination="true"
                                    data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>

                            <?php } else { ?>
                            <div class="branches-content-not-found">
                                <img src="<?php echo base_url(); ?>/dist/img/branches.svg">
                                <h3>Branches</h3>
                                <p>Branches function separately from your main account. They can have their own
                                    courses, users, domain, and appearance.</p>
                                <a href="<?php echo base_url(); ?>branches/create" class="btn btn-primary">Add your
                                    first Branche</a>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- /.content -->
</div>


<!-- Modal added courses to all branch -->
<div class="modal fade" id="add_courses_to_branch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add a course to all branches</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
         $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_AddCoursesToBranch');
         echo form_open(base_url().'branches/AddCoursesToAllBranch', $attributes); 
       ?>
            <div class="modal-body ">
                <select name="course_id" id="addCourse">
                <?php
                    if(isset($category) && isset($course_category)) { ?>
                    <option value="">Please select course</option>
                    <?php
                    foreach($category as $category_value){?>
                        <optgroup label="<?php echo $category_value->name; ?>">
                            <?php 
                            foreach($course_category as $course_category_value){ ?>
                                <?php 
                                if($category_value->id == $course_category_value->category){ ?>
                                    <option value="<?php echo $course_category_value->id;?>"><?php echo $course_category_value->course_name;?></option>
                                <?php }
                                ?>
                            <?php }
                            ?>
                        </optgroup>
                    <?php }
                    ?>
                    <?php } ?>
                </select><br>
                <span class="error_addcourse"></span>
            </div>

            <div class="modal-footer">
                <!-- <input type="hidden" name="deleteid" id="delbtn" /> -->
                <button type="submit" class="btn btn-success">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<!-- Modal Delete courses to all branch -->
<div class="modal fade" id="delete_courses_to_branch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remove a course from all branches</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
         $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_DeleteCoursesToAllBranch');
         echo form_open(base_url().'branches/DeleteCoursesToAllBranch', $attributes); 

       ?>
            <div class="modal-body ">
                <select name="course_id" id="deleteCourse">
                <?php
                    if(isset($category) && isset($course_category)) { ?>
                    <option value="">Please select course</option>
                    <?php
                    foreach($category as $category_value){?>
                        <optgroup label="<?php echo $category_value->name; ?>">
                            <?php 
                            foreach($course_category as $course_category_value){ ?>
                                <?php 
                                if($category_value->id == $course_category_value->category){ ?>
                                    <option value="<?php echo $course_category_value->id;?>"><?php echo $course_category_value->course_name;?></option>
                                <?php }
                                ?>
                            <?php }
                            ?>
                        </optgroup>
                    <?php }
                    ?>
                    <?php } ?>
                </select><br>
                <span class="error_deletecourse"></span>
            </div>

            <div class="modal-footer">
                <!-- <input type="hidden" name="deleteid" id="delbtn" /> -->
                <button type="submit" class="btn btn-danger">Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<?php $this->load->view('include/footer2');?>

<!-- page specific js -->
<script>
var $branchListTable = $('#branchListTable');

// Row action function
function rowAction(value, row, index) {
   var html = `<i class="fa fa-ellipsis-h"></i>
               <div class="hover-tbl-btn">
                   <a href="edit/${row.id}" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                   <a class="tbl-btn tbl-pen btnSelect" data-id="${row.id}"><i class="fas fa-magnet"></i></a>
                   <a onclick="DeleteBranch(${row.id})" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times" ></i></a>
               </div>`;
   return [
       html
   ].join("");
}


// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
   $branchListTable.bootstrapTable('destroy').bootstrapTable({
       url: 'datalist',
       showFullscreen: true,
       exportDataType: $(this).val(),
       exportTypes: ['excel'],
       columns: [{
               field: 'state',
               checkbox: true,
               visible: $(this).val() === 'selected'
           },
           {
               field: 'name',
               title: 'Branch Name',
               sortable: true
           },
           {
               field: 'description',
               title: 'Descriptions',
               sortable: true
           },
           {
               field: 'option',
               title: 'OPTIONS',
               formatter: rowAction
           }
       ]
   })
})
window.branchListAjax = {
   complete: function(xhr) {
       stopLoader();
   }
}

// code to read selected table row cell data (values).
$("#branchListTable").on('click', '.btnSelect', function() {
    var currentRow = $(this).closest("tr");
    var col = currentRow.find("td:eq(0)").text();
    var branch_id = $(this).data('id');
    if(confirm("Are you sure you want to clone the branch "+col)== true){
        var url = 'clone';
        // Ajax request
        var response = CommonAjaxForObjData(url, 'POST', {
            id: branch_id,
            clone_name: col
        });
        // Handling response
        if (response.success === true) {
            $branchListTable.bootstrapTable('refresh');
        }
        snackbar(response.message);
    }
});


// Show popup of add course to all branch
function add_courses() {
    $("#add_courses_to_branch").modal('show');   
}

// Show popup of delete course to all branch
function delete_courses(){
    $("#delete_courses_to_branch").modal('show');
}

$('#LMS_AddCoursesToBranch').submit(function(event) {
    event.preventDefault();
    var id = $('#addCourse').find(":selected").val();
    if(id != ""){
        $('.error_addcourse').text("");
        // Form Data
        let myForm = document.getElementById('LMS_AddCoursesToBranch');
        let formData = new FormData(myForm);
        // URL
        var url = $("#LMS_AddCoursesToBranch").attr("action");
        // Ajax request
        var response = CommonAjax(url, 'POST', formData);
        if (response !== false) {
            $("#add_courses_to_branch").modal('toggle');
            snackbar(response.message);
        } else {
            snackbar("Some error occured.");
        }
    }
    else{
        $('.error_addcourse').addClass("error_msg");
        $('.error_addcourse').text("Please select of course!");
    }
});

$('#LMS_DeleteCoursesToAllBranch').submit(function(event) {
    event.preventDefault();
    var id = $('#deleteCourse').find(":selected").val();
    if(id != ""){
        $('.error_addcourse').text("");
        // Form Data
        let myForm = document.getElementById('LMS_DeleteCoursesToAllBranch');
        let formData = new FormData(myForm);
        // URL
        var url = $("#LMS_DeleteCoursesToAllBranch").attr("action");
        // Ajax request
        var response = CommonAjax(url, 'POST', formData);
        if (response !== false) {
            $("#delete_courses_to_branch").modal('toggle');
            snackbar(response.message);
        } else {
            snackbar("Some error occured.");
        }
    }
    else{
        $('.error_deletecourse').addClass("error_msg");
        $('.error_deletecourse').text("Please select of course!");
    }
});
</script>

<script>
$(document).ready(function() {
   $(".custom-droup-in").click(function() {
       $(".custom-droup-down-inner").slideToggle(10);
       return false;
   });
   $(".custom-droup-down > li").click(function() {
       var selecte = $(this).text();
       $('.custom-droup-in').text(selecte);
   });
   $("body").click(function() {
       $(".custom-droup-down-inner").slideUp(0);
   });
   $(".custom-droup-down-inner > li > span").click(function(e) {
       e.stopPropagation();
   });
});
</script>