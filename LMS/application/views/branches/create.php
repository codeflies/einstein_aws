<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"></section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Add Branches</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <?php 
                $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_AddBranches');
                echo form_open(base_url().'branches/create', $attributes); 
              ?>
                <input type="hidden" name="add_branch" value="add_branch">
                <div class="card-body">
                    <div class="invalid-feedback">
                        <p><?php echo $this->session->flashdata('message'); ?></p>
                    </div>
                    <h5 class="mb-2 bg-grey p-2">IDENTITY</h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="bt-name">Name</label>
                                <input type="text" class="form-control marketing_name" id="bt-name"
                                    placeholder="e.g. marketing" name="name" data-url="<?php echo $url;?>" />
                                <input type="hidden" val="" name="" id="marketing_name_url" />
                                <span id="name_url"></span>
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('name'); ?>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="bt-title">Title</label>
                                <input type="text" class="form-control" id="bt-title"
                                    placeholder="Title used in search engines" name="title" />
                            </div>

                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" rows="4"
                                    placeholder="Short description up to 259 characters" name="description"></textarea>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Default Branch theme</label>
                                <select class="form-control select2" name="default_branch_theme">
                                    <option value="Default">Default</option>
                                    <option value="City Lights">City Lights</option>
                                    <option value="Color">Color</option>
                                    <option value="Final Frontier">Final Frontier</option>
                                    <option value="Modern">Modern</option>
                                    <option value="Nursery Room">Nursery Room</option>
                                    <option value="Pets">Pets</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Select Image</label>
                                <div class="input-group">
                                    <div class="custom-file form-group">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        <input type="file" class="custom-file-input form-control"
                                            onchange="imagesPreview(this, 'div.gallery')" name="image" accept="image/*"
                                            placeholder="Choose Image">
                                    </div>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('image'); ?>
                                    </div>
                                </div>
                                <div class="col-6 gallery">

                                </div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Select Favicon</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input"
                                            onchange="imagesPreview(this, 'div.gallery_1')" name="favicon">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('favicon'); ?>
                                    </div>
                                </div>
                                <div class="col-6 gallery_1">

                                </div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-12">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="Check" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Active</label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>

                    <h5 class="mt-3 bg-grey p-2">LOCALE</h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Branch language</label>
                                <select class="form-control select2" name="language">
                                    <option selected="selected" value="english">English</option>
                                    <option value="hindi">Hindi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Branch time zone</label>
                                <div class="input-group date" id="timepicker" data-target-input="nearest">
                                    <select name="timezone" class="form-control select2" id="" name="timezone">
                                        <?php if (isset($timedatadata)) { ?>
                                        <?php foreach ($timedatadata as $value) { ?>
                                        <option timeZoneId="" gmtAdjustment="<?php echo $value->utc_offset;?>"
                                            useDaylightTime="<?php echo $value->utc_offset;?>"
                                            value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div class="input-group-append" data-target="#timepicker"
                                        data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="mt-3 mb-3 bg-grey p-2">ANNOUNCEMENT</h5>

                    <ul class="nav nav-tabs" id="custom-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-internal-tab" data-toggle="pill"
                                href="#custom-internal" role="tab" aria-controls="custom-internal"
                                aria-selected="true">Internal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-external-tab" data-toggle="pill" href="#custom-external"
                                role="tab" aria-controls="custom-external" aria-selected="false">External</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="custom-tabContent">
                        <div class="tab-pane fade show active" id="custom-internal" role="tabpanel"
                            aria-labelledby="custom-internal-tab">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="description"></label>
                                    <textarea class="form-control" rows="4" placeholder="" name="internal"></textarea>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="tab-pane fade" id="custom-external" role="tabpanel"
                            aria-labelledby="custom-external-tab">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="description"></label>
                                    <textarea class="form-control" rows="4" placeholder="" name="external"></textarea>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </div>

                    <h5 class="mt-3 bg-grey p-2">USERS</h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Default user type</label>
                                <select class="form-control" name="default_user_type">
                                    <option selected="selected">Learn Type</option>
                                    <option>Super Admin</option>
                                    <option>Admin-Type</option>
                                    <option>Trainer-Type</option>
                                    <option>Learn-Type</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Signup</label>
                                <select class="form-control select2" name="sigup">
                                    <option selected="selected">Manually (from admin)</option>
                                    <option>City Lights</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>

                    <div id="accordion">
                        <div class="card-header pl-2 br-0">
                            <h4 class="card-title w-100">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="restrict"
                                        name="specific_domain">
                                    <label class="form-check-label" for="restrict" data-toggle="collapse"
                                        href="#collapseOne">Restrict registration to specific domains</label>
                                </div>
                            </h4>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="doamin" placeholder="Domain" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="registration">
                            <label class="form-check-label" for="registration">Restrict registration to</label>
                        </div>
                        <div class="form-group mt-1">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Restrict registration to"
                                    name="restrict_register_user">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Users</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 mt-2">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="disallow" name="diallow_from_login">
                            <label class="form-check-label" for="disallow">Disallow members of this branch to login from
                                main domain URL &nbsp;<i class="fa fa-info"></i></label>
                        </div>
                    </div>
                    <div id="accordion1">
                        <div class="card-header pl-2  br-0">
                            <h4 class="card-title w-100">
                                <span for="restrict" data-toggle="collapse" href="#termsservices"><i
                                        class="fa fa-book"></i> Terms of Service</span>
                            </h4>
                        </div>
                        <div id="termsservices" class="collapse" data-parent="#accordion1">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control"
                                        placeholder="The terms of service is shown to each user when they first login to the system. It is necessary to accept it in order to continue. Leave empty if you don't want to have terms of service."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="mt-4 bg-grey p-2">E-COMMERCE</h5>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>E-commerce Processor</label>
                            <select class="form-control select2" name="e_commerce">
                                <option>Select Your e-commerce processor</option>
                                <option selected="selected">Paypal</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Currency</label>
                            <select class="form-control select2" name="currency">
                                <option selected="selected">US Dollar</option>
                                <option>India Rupees</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="paypal-email">Paypal email address</label>
                            <input type="email" class="form-control" id="paypal-email"
                                placeholder="Paypal email address" name="paypal_email_address">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                $
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" disabled="">
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <select class="form-control" disabled="">
                                            <option>Monthly</option>
                                        </select>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="paid" disabled="">
                            <label class="form-check-label" for="paid">Access to all paid courses at a charge of
                                &nbsp;<small class="fa fa-info"></small></label>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="fmonth" disabled="">
                            <label class="form-check-label" for="fmonth">Free for the first month &nbsp;<small
                                    class="fa fa-info"></small></label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="credits">
                            <label class="form-check-label" for="credits">Activate credits &nbsp;<small
                                    class="fa fa-info"></small></label>
                        </div>
                    </div>
                    <h5 class="mt-3 bg-grey p-2">GAMIFICATION</h5>
                    <div class="form-group">
                        <label>Badge Set</label>
                        <select class="form-control select2" name="gamification">
                            <option selected="selected">Old School</option>
                            <option>Stickers</option>
                            <option>Evolution</option>
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->



                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Add Branch</button> <span>or</span> <a href=""
                        #>Cancel</a>
                </div>
                <?php echo form_close(); ?>
            </div>

            <br />
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
<!-- Modal Delete courses to all branch -->
<div class="modal fade" id="imagePreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body ">
                <img src="" id="imagepreview" style="width: 400px; height: 264px;">
            </div>

            <div class="modal-footer">

                <input type="hidden" name="deleteid" id="delbtn" />
                <button type="submit" class="btn btn-info">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('include/footer2');?>
<script>
$(document).ready(function() {
    // Initialize select2
    $(".select2").select2();
    $('.marketing_name').on('keyup', function() {
        var str = $(this).val();
        if (!str) {
            $('#name_url').empty();
        } else {
            if (str.indexOf(" ") !== -1) {
                var replace = str.replace(/\s/g, '');
                $('#bt-name').val(replace.toLowerCase());
            } else {
                var url = $(this).val();
                var marketing_url = 'URL:-' + url + '-' + $(this).data("url");
                $('#name_url').empty();
                var replace = str.replace(/\s/g, '');
                $('#bt-name').val(replace.toLowerCase());
                $('#name_url').append(marketing_url.toLowerCase());
            }
        }
    })
});

// function imagesPreviewSelect(input, placeToInsertImagePreview) { 
//     $(placeToInsertImagePreview).html("");
//     if (input.files) {
//         var filesAmount = input.files.length;
//         for (i = 0; i < filesAmount; i++) {
//             var reader = new FileReader();
//             reader.onload = function(event) {
//                 $($.parseHTML(`<img id="imageresource" class='${i}' width='50%' height="20%">`)).attr('src', event.target.result).appendTo(
//                     placeToInsertImagePreview);
//                     $('#imagepreview').attr('src', $('#imageresource').attr('src'));
//   $('#imagePreview').modal('show');
//             }
//             reader.readAsDataURL(input.files[i]);
//         }
//     }
// }
</script>