<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"></section>

    <!-- Main content -->
    <section class="content userinfo-content">
        <div class="container">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">User Name Here</h3>
                </div>
                <!-- /.card-header -->

                <div class="card-body">
                    <nav class="navbar navbar-expand p-0">
                        <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                            <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('branches/edit/'.$result->id)?>">Info</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="#">Homepage</a></li> -->
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branch/user/'.$result->id) ?>">User</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branch/course/'.$result->id); ?>">Courses</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branch/files/'.$result->id); ?>">Files</a></li>
                        </ul>
                        <!-- <ul class="navbar-nav ml-auto pb-2 mobile-none">
                            <li>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-primary">Profile</a>
                                    <a href="#" class="btn btn-default">Progress</a>
                                    <a href="#" class="btn btn-default">Infographic</a>
                                </div>
                            </li>
                        </ul> -->
                    </nav>
                    <div class="dropdown-divider mt-0 mb-3"></div>
                    
                    <!-- form start -->
                    <?php 
                $attributes = array('enctype' => 'multipart/form-data', 'id' => 'LMS_UpdateBranches');
                echo form_open(base_url().'branches/Update', $attributes); 
              ?>
                    <input type="hidden" name="update_branch" value="update_branch">
                    <input type="hidden" name="branch_id" value="<?php echo $result->id;?>">
                    <div class="tab-content">
                        <h5 class="mb-2 bg-grey p-2">IDENTITY</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="bt-name">Name</label>
                                    <input type="text" class="form-control marketing_name" id="bt-name"
                                        placeholder="e.g. marketing" value="<?php echo $result->name;?>" name="name"
                                        data-url="<?php echo $url;?>" />
                                    <input type="hidden" val="" name="" id="marketing_name_url" />
                                    <span id="name_url">
                                        <p>URL:-<?php echo strtolower(str_replace(' ', '', $result->name));?>-wp.localserverweb.com/einstein/LMS/
                                        </p>
                                    </span>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="bt-title">Title</label>
                                    <input type="text" class="form-control" id="bt-title"
                                        placeholder="Title used in search engines" value="<?php echo $result->title;?>"
                                        name="title" />
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" rows="4"
                                        placeholder="Short description up to 259 characters" name="description"
                                        value=""><?php echo $result->description;?></textarea>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Default Branch theme</label>
                                    <select class="form-control select2" name="default_branch_theme">
                                        <option value="Default"
                                            <?php if($result->default_branch_theme=='Default') echo 'selected="selected"'; ?>>
                                            Default</option>
                                        <option value="City Lights"
                                            <?php if($result->default_branch_theme=='City Lights') echo 'selected="selected"'; ?>>
                                            City Lights</option>
                                        <option value="Color"
                                            <?php if($result->default_branch_theme=='Color') echo 'selected="selected"'; ?>>
                                            Color</option>
                                        <option value="Final Frontier"
                                            <?php if($result->default_branch_theme=='Final Frontier') echo 'selected="selected"'; ?>>
                                            Final Frontier</option>
                                        <option value="Modern"
                                            <?php if($result->default_branch_theme=='Modern') echo 'selected="selected"'; ?>>
                                            Modern</option>
                                        <option value="Nursery Room"
                                            <?php if($result->default_branch_theme=='Nursery Room') echo 'selected="selected"'; ?>>
                                            Nursery Room</option>
                                        <option value="Pets"
                                            <?php if($result->default_branch_theme=='Pets') echo 'selected="selected"'; ?>>
                                            Pets</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputFile">Select Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input form-control"
                                                onchange="imagesPreview(this, 'div.gallery')" name="image"
                                                accept="image/*" placeholder="Choose Image">
                                            <label class="custom-file-label" for="exampleInputFile">Choose
                                                file</label>
                                        </div>
                                    </div>
                                    <?php if(isset($result->image)){ ?>
                                    <div class="image">
                                        <img src="<?php echo base_url('uploads/users/branches/'.$result->image);?>"
                                            class="img-responsive zoom-img" alt="">
                                    </div>
                                    <?php } ?>
                                    <div class="col-6 gallery">

                                    </div>
                                </div>

                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputFile">Select Favicon</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                onchange="imagesPreview(this, 'div.gallery_1')" name="favicon">
                                            <label class="custom-file-label" for="exampleInputFile">Choose
                                                file</label>
                                        </div>

                                    </div>
                                    <?php if(isset($result->image)){ ?>
                                    <div class="favicon">
                                        <img src="<?php echo base_url('uploads/users/branches/'.$result->favicon);?>"
                                            class="img-responsive zoom-img" alt="">
                                    </div>
                                    <?php } ?>
                                    <div class="col-6 gallery_1">

                                    </div>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" value="Check" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Active</label>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>

                        <h5 class="mt-3 bg-grey p-2">LOCALE</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Branch language</label>
                                    <select class="form-control select2" name="language">
                                        <option selected="selected" value="english"
                                            <?php if($result->language=='Default') echo 'selected="selected"'; ?>>
                                            English</option>
                                        <option value="hindi"
                                            <?php if($result->language=='hindi') echo 'selected="selected"'; ?>>
                                            Hindi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Branch time zone</label>
                                    <div class="input-group date" id="timepicker" data-target-input="nearest">
                                        <select name="timezone" class="form-control select2" id="" name="timezone">
                                            <?php if (isset($timedatadata)) { ?>
                                            <?php foreach ($timedatadata as $value) { ?>
                                            <?php if($result->timezone==$value->id){ ?>
                                            <option timeZoneId="" gmtAdjustment="<?php echo $value->utc_offset;?>"
                                                useDaylightTime="<?php echo $value->utc_offset;?>"
                                                value="<?php echo $value->id;?>" selected><?php echo $value->name;?>
                                            </option>
                                            <?php } 
                                else{ ?>
                                            <option timeZoneId="" gmtAdjustment="<?php echo $value->utc_offset;?>"
                                                useDaylightTime="<?php echo $value->utc_offset;?>"
                                                value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                                            <?php }?>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-append" data-target="#timepicker"
                                            data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 class="mt-3 mb-3 bg-grey p-2">ANNOUNCEMENT</h5>

                        <ul class="nav nav-tabs" id="custom-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-internal-tab" data-toggle="pill"
                                    href="#custom-internal" role="tab" aria-controls="custom-internal"
                                    aria-selected="true">Internal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-external-tab" data-toggle="pill" href="#custom-external"
                                    role="tab" aria-controls="custom-external" aria-selected="false">External</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="custom-tabContent">
                            <div class="tab-pane fade show active" id="custom-internal" role="tabpanel"
                                aria-labelledby="custom-internal-tab">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="description"></label>
                                        <textarea class="form-control" rows="4" placeholder=""
                                            name="internal"><?php echo $result->internal;?></textarea>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="tab-pane fade" id="custom-external" role="tabpanel"
                                aria-labelledby="custom-external-tab">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="description"></label>
                                        <textarea class="form-control" rows="4" placeholder=""
                                            name="external"><?php echo $result->external;?></textarea>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>

                        <h5 class="mt-3 bg-grey p-2">USERS</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Default user type</label>
                                    <select class="form-control" name="default_user_type">
                                        <option selected="selected">Learn Type</option>
                                        <option
                                            <?php if($result->default_user_type=='Learn Type') echo 'selected="selected"'; ?>>
                                            Learn Type</option>
                                        <option
                                            <?php if($result->default_user_type=='Super Admin') echo 'selected="selected"'; ?>>
                                            Super Admin</option>
                                        <option
                                            <?php if($result->default_user_type=='Admin-Type') echo 'selected="selected"'; ?>>
                                            Admin-Type</option>
                                        <option
                                            <?php if($result->default_user_type=='Trainer-Type') echo 'selected="selected"'; ?>>
                                            Trainer-Type</option>
                                        <option
                                            <?php if($result->default_user_type=='Learn-Type') echo 'selected="selected"'; ?>>
                                            Learn-Type</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Signup</label>
                                    <select class="form-control select2" name="sigup">
                                        <option
                                            <?php if($result->sigup=='Manually (from admin)') echo 'selected="selected"'; ?>
                                            value="Manually (from admin)">Manually (from admin)</option>
                                        <option <?php if($result->sigup=='City Lights') echo 'selected="selected"'; ?>
                                            value="City Lights">City Lights</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>

                        <div id="accordion">
                            <div class="card-header pl-2 br-0">
                                <h4 class="card-title w-100">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="restrict"
                                            name="specific_domain" value="<?php echo $result->specific_domain ;?>">
                                        <label class="form-check-label" for="restrict" data-toggle="collapse"
                                            href="#collapseOne">Restrict registration to specific domains</label>
                                    </div>
                                </h4>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="doamin" placeholder="Domain"
                                            name="domain" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="registration">
                                <label class="form-check-label" for="registration">Restrict registration to</label>
                            </div>
                            <div class="form-group mt-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Restrict registration to"
                                        name="restrict_register_user"
                                        value="<?php echo $result->register_total_user ;?>">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Users</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mt-2">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="disallow" name="diallow_from_login">
                                <label class="form-check-label" for="disallow">Disallow members of this branch to
                                    login from main domain URL &nbsp;<i class="fa fa-info"></i></label>
                            </div>
                        </div>
                        <div id="accordion1">
                            <div class="card-header pl-2  br-0">
                                <h4 class="card-title w-100">
                                    <span for="restrict" data-toggle="collapse" href="#termsservices"><i
                                            class="fa fa-book"></i> Terms of Service</span>
                                </h4>
                            </div>
                            <div id="termsservices" class="collapse" data-parent="#accordion1">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea class="form-control"
                                            placeholder="The terms of service is shown to each user when they first login to the system. It is necessary to accept it in order to continue. Leave empty if you don't want to have terms of service."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 class="mt-4 bg-grey p-2">E-COMMERCE</h5>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>E-commerce Processor</label>
                                <select class="form-control select2" name="e_commerce">
                                    <option>Select Your e-commerce processor</option>
                                    <option selected="selected">Paypal</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Currency</label>
                                <select class="form-control select2" name="currency">
                                    <option selected="selected">US Dollar</option>
                                    <option>India Rupees</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="paypal-email">Paypal email address</label>
                                <input type="email" class="form-control" id="paypal-email"
                                    placeholder="Paypal email address" name="paypal_email_address">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    $
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" disabled="">
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-lg-6 -->
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <select class="form-control" disabled="">
                                                <option>Monthly</option>
                                            </select>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-lg-6 -->
                                </div>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="paid" disabled="">
                                <label class="form-check-label" for="paid">Access to all paid courses at a charge of
                                    &nbsp;<small class="fa fa-info"></small></label>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="fmonth" disabled="">
                                <label class="form-check-label" for="fmonth">Free for the first month &nbsp;<small
                                        class="fa fa-info"></small></label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="credits">
                                <label class="form-check-label" for="credits">Activate credits &nbsp;<small
                                        class="fa fa-info"></small></label>
                            </div>
                        </div>
                        <h5 class="mt-3 bg-grey p-2">GAMIFICATION</h5>
                        <div class="form-group">
                            <label>Badge Set</label>
                            <select class="form-control select2" name="gamification">
                                <option <?php if($result->gamification=='Old School') echo 'selected="selected"'; ?>
                                    value="Old School">Old School</option>
                                <option <?php if($result->gamification=='Stickers') echo 'selected="selected"'; ?>
                                    value="Stickers">Stickers</option>
                                <option <?php if($result->gamification=='Evolution') echo 'selected="selected"'; ?>
                                    value="Evolution">Evolution</option>
                            </select>
                        </div>
                        <!-- // Tab 1 End -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update User </button> <span>or</span> <a
                                href="" #>Cancel</a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                    <!-- Tabs end -->
                </div>
                <!-- /.card-body -->


            </div>

            <br />
        </div>
        <!-- /.container-fluid -->
    </section>

    <?php $this->load->view('include/footer2');?>
    <script>
    $(document).ready(function() {

        // Initialize select2
        $(".select2").select2();

        $('.marketing_name').on('keyup', function() {
            if (!$(this).val()) {
                $('#name_url').empty();
            } else {
                var url = $(this).val();
                var marketing_url = 'URL:-' + url + '-' + $(this).data("url");
                $('#name_url').empty();
                $('#name_url').append(marketing_url);
            }

        })
    });

    // Add LMS USERS FUNCTION
    $('#LMS_UpdateBranches').submit(function(event) {
        event.preventDefault();
        startLoader();
        // Form Data
        let myForm = document.getElementById('LMS_UpdateBranches');
        let formData = new FormData(myForm);
        // URL
        var url = $("#LMS_UpdateBranches").attr("action");
        // Ajax request
        var response = CommonAjax(url, 'POST', formData);
        if (response !== false) {
            // Displaying validation errors
            var err = response.error;
            for (var key in err) {
                if (err.hasOwnProperty(key)) {
                    $(`[name='${key}']`).parent().next().html(`<p>${err[key]}</p>`);
                    $('.invalid-feedback').css({
                        display: 'block'
                    });
                }
            }
            snackbar(response.message);
            // Handling response
            if (response.success === true) {
                setTimeout(
                    function() {
                        window.location = response.target;
                    }, 5000);
            }
        } else {
            snackbar("Some error occured.");
        }
    });
    </script>