<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"></section>

    <!-- Main content -->
    <section class="content branches-content pb-3">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header ">
                            <h3 class="card-title"><a href="<?php echo base_url(); ?>">Home / <a href="<?php echo base_url('/branches/list'); ?>">Branches</a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <nav class="navbar navbar-expand p-0">
                                <ul class="nav nav-tabs mb-0 br-0 pl-0" role="tablist">
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branches/edit/'.$branch_id)?>">Info</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link" href="#">Homepage</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branch/user/'.$branch_id); ?>">User</a></li>
                                    <li class="nav-item"><a class="nav-link active" href="<?php echo base_url('branch/course/'.$branch_id); ?>">Courses</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url('branch/files/'.$branch_id); ?>">Files</a></li>
                                </ul>
                            </nav>
                            <!-- branch not found end -->

                            <div class="dropdown-divider"></div>
                            <div class="table-responsive">
                                <div>
                                    <input class=" py-2 pl-3 w-100 border" type="text" placeholder="search"
                                        id="branchUserListSearch" autocomplete="off">
                                </div>
                                <table id="branchUserListTable" data-search="true" data-visible-search="true"
                                    data-ajax-options="branchUserListAjax" data-show-columns="true" data-show-export="true"
                                    data-search-selector="#branchUserListSearch" data-checkbox-header="false"
                                    data-click-to-select="true" data-checkbox="true"
                                    data-buttons-prefix="btn-sm btn btn-success" data-pagination="true"
                                    data-side-pagination="server" data-server-sort="true"
                                    class="table-borderless user-table table-hover fonts_size font_family">
                                    <thead>
                                    </thead>
                                </table>
                            </div>

                            
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- /.content -->
</div>

<?php $this->load->view('include/footer2');?>

<!-- page specific js -->
<script>
var $branchUserListTable = $('#branchUserListTable');

// Row action function
function rowAction(value, row, index) {
   var html = `<i class="fa fa-ellipsis-h"></i>
               <div class="hover-tbl-btn">
               </div>`;
   return [
       html
   ].join("");
}


// EXPORT TO CSV OR PDF CODE BEGINS HERE
$(function() {
   $branchUserListTable.bootstrapTable('destroy').bootstrapTable({
       url: '<?php echo base_url(); ?>branches/branchCourselist',
       showFullscreen: true,
       exportDataType: $(this).val(),
       exportTypes: ['excel'],
       columns: [{
               field: 'state',
               checkbox: true,
               visible: $(this).val() === 'selected'
           },
           {
               field: 'course_name',
               title: 'COURSE',
               sortable: true
           },
           {
               field: 'name',
               title: 'CATEGORY',
               sortable: true
           },
           {
               field: 'option',
               title: 'OPTIONS',
               formatter: rowAction
           }
       ]
   })
})
window.branchUserListAjax = {
   complete: function(xhr) {
       stopLoader();
   }
}
</script>