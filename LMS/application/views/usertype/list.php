
<?php $this->load->view('include/header2');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">User Listing</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content user-content">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header ">
                <h3 class="card-title">User</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-tbl-head">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="btn-group">
                        <a href="<?php echo base_url(); ?>usertype/create" class="btn btn-primary">Add User</a>
                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu" style="">
                          <a class="dropdown-item" href="#">Import User(s)</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="dropdown-divider"></div>
                <div class="table-responsive">
                  <table id="example1" class="table table-striped table-hover user-table">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Administrator</th>
                      <th>Instructor</th>
                      <th>Learner</th>
                      <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>SuperAdmin</td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>Admin-Type</td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                      <td>
                        <i class="fa fa-ellipsis-h"></i>
                        <div class="hover-tbl-btn">
                          <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                          <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                        <td>Trainer-Type</td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td>
                          <i class="fa fa-ellipsis-h"></i>
                          <div class="hover-tbl-btn">
                            <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                            <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Learner-Type</td>
                        <td>-</td>
                        <td>-</td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div>
                            <td><div class="table_icon_icon"><i class="fas fa-pen"></div></td>
                      </tr>
                      <tr>
                        <td>MANAGEER</td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td><div class="table_icon_icon"><i class="fa fa-check"></i></div></td>
                        <td>
                          <i class="fa fa-ellipsis-h"></i>
                          <div class="hover-tbl-btn">
                            <a href="#" class="tbl-btn tbl-pen" title="Edit"><i class="fas fa-pen"></i></a>
                            <a href="#" class="tbl-btn tbl-close" title="Delete"><i class="fas fa-times"></i></a>
                          </div>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </tbody>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

   <?php $this->load->view('include/footer2');?>