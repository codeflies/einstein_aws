
<?php $this->load->view('include/header2');?>


<style>

.stylish-input-group .input-group-addon{
    background: white !important;
}
.stylish-input-group .form-control{
    //border-right:0;
    box-shadow:0 0 0;
    border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}

.h-scroll {
    background-color: #fcfdfd;
    height: 260px;
    overflow-y: scroll;
}

label:not(.form-check-label):not(.custom-file-label) {
    font-weight: 400;
    font-size: 18px;
}

.hummingbird-treeview .fa {
    font-style: normal;
    cursor: pointer;
    font-size: 16px;
}

</style>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="fp-name">Name</label>
                          <input type="text" class="form-control" id="fp-name" placeholder="e.g John" />
                        </div>
                      </div>
                      <!-- /.form-group -->
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="lp-name">User type default role</label>
                          <select class="form-control selectlanguage" style="width: 100%;">
                            <option selected="selected">Administrator</option>
                            <option>Instructor</option>
                            <option>Learneri</option>
                          </select>
                        </div>
                      </div>
                      <!-- /.form-group -->
                    </div>

                    <!-- multi-level checkbox -->
                  <!--   <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label for="lp-name">Permissions</label>
                            <div class="row">
                              <div class="col-md-3">
                                <div class="mutistep-dropdown">
                                  <ul>
                                    <li class="drop-children">
                                      <input type="checkbox" name="" id="level1">
                                      <label for="level1">Level 1</label>
                                      <ul>
                                        <li class="drop-children">
                                          <input type="checkbox" name="" id="level2">
                                          <label for="level2">Level 2</label>
                                            <ul>
                                              <li class="drop-children">
                                                <input type="checkbox" name="" id="level3">
                                                <label for="level3">Level 3</label>
                                                <ul>
                                                  <li>
                                                    <input type="checkbox" name="" id="level4">
                                                    <label for="level4">Level 4</label>
                                                  </li>
                                                </ul>
                                              </li>
                                            </ul>
                                        </li>
                                      </ul>
                                    </li>

                                    <li>
                                      <input type="checkbox" name="" id="level11">
                                      <label for="level11">Level 1</label>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="mutistep-dropdown">
                                  <ul>
                                    <li class="drop-children">
                                      <input type="checkbox" name="" id="level1">
                                      <label for="level1">Level 1</label>
                                      <ul>
                                        <li class="drop-children">
                                          <input type="checkbox" name="" id="level2">
                                          <label for="level2">Level 2</label>
                                            <ul>
                                              <li class="drop-children">
                                                <input type="checkbox" name="" id="level3">
                                                <label for="level3">Level 3</label>
                                                <ul>
                                                  <li>
                                                    <input type="checkbox" name="" id="level4">
                                                    <label for="level4">Level 4</label>
                                                  </li>
                                                </ul>
                                              </li>
                                            </ul>
                                        </li>
                                      </ul>
                                    </li>

                                    <li>
                                      <input type="checkbox" name="" id="level11">
                                      <label for="level11">Level 1</label>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="mutistep-dropdown">
                                  <ul>
                                    <li class="drop-children">
                                      <input type="checkbox" name="" id="level1">
                                      <label for="level1">Level 1</label>
                                      <ul>
                                        <li class="drop-children">
                                          <input type="checkbox" name="" id="level2">
                                          <label for="level2">Level 2</label>
                                            <ul>
                                              <li class="drop-children">
                                                <input type="checkbox" name="" id="level3">
                                                <label for="level3">Level 3</label>
                                                <ul>
                                                  <li>
                                                    <input type="checkbox" name="" id="level4">
                                                    <label for="level4">Level 4</label>
                                                  </li>
                                                </ul>
                                              </li>
                                            </ul>
                                        </li>
                                      </ul>
                                    </li>

                                    <li>
                                      <input type="checkbox" name="" id="level11">
                                      <label for="level11">Level 1</label>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="mutistep-dropdown">
                                  <ul>
                                    <li class="drop-children">
                                      <input type="checkbox" name="" id="level1">
                                      <label for="level1">Level 1</label>
                                      <ul>
                                        <li class="drop-children">
                                          <input type="checkbox" name="" id="level2">
                                          <label for="level2">Level 2</label>
                                            <ul>
                                              <li class="drop-children">
                                                <input type="checkbox" name="" id="level3">
                                                <label for="level3">Level 3</label>
                                                <ul>
                                                  <li>
                                                    <input type="checkbox" name="" id="level4">
                                                    <label for="level4">Level 4</label>
                                                  </li>
                                                </ul>
                                              </li>
                                            </ul>
                                        </li>
                                      </ul>
                                    </li>

                                    <li>
                                      <input type="checkbox" name="" id="level11">
                                      <label for="level11">Level 1</label>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                        </div>        
                      </div>
                    </div> -->
					<div id="treeview_container" class="hummingbird-treeview" style="height: 230px; overflow-y: hidden;">
					      <ul id="treeview" class="hummingbird-base">
					      <li data-id="0">
					    <i class="fa fa-plus"></i>
					    <label>
					        <input  type="checkbox" /> Administrator
					    </label>
					    <ul>
					        <li data-id="1">
					      <i class="fa fa-plus"></i>
					      <label>
					          <input type="checkbox" /> Instructor
					      </label>
					      <ul>
					          <li>
					        <label>
					            <input class="hummingbird-end-node" type="checkbox" />User 1
					        </label>
					          </li>
					          <li>
					        <label>
					            <input class="hummingbird-end-node" type="checkbox" /> User 2
					        </label>
					          </li>
					      </ul>
					        </li>
					        <li data-id="1">
					      <i class="fa fa-plus"></i>
					      <label>
					          <input type="checkbox" /> Instructor 2
					      </label>
					      <ul>
					          <li>
					        <label>
					            <input class="hummingbird-end-node" type="checkbox" /> Test 1
					        </label>
					          </li>
					          <li>
					        <label>
					            <input class="hummingbird-end-node" type="checkbox" /> Test 2
					        </label>
					          </li>
					      </ul>
					        </li>
					    </ul>
					      </li>
					  </ul>
					    </div>

                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add User</button> <span>or</span> <a href=""#>Cancel</a>
                </div>
              </form>
            </div>

            <br />
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>



<?php $this->load->view('include/footer2');?>

<script type="text/javascript">
    
    function GetFormData() {
        let formData = {
            GetFormData : true
        };
        // URL
        var url = "create";
        // Ajax request
        var response = CommonAjax(url, 'POST', formData);
        if (response !== false) {
            // Handling response
            if (response.success === true) {
                console.log(response.data);
                // window.location = response.target;
            } else {
                snackbar(response.message);
            }
        } else {
            snackbar("Some error occured.");
        }  
    }

    GetFormData();

    
 /* $(function(){
        var children=$('.drop-children > label').filter(function(){return $(this).nextAll().length>0})
        $('<span class="toChild">▼</span>').insertAfter(children)
        $('.drop-children .toChild').click(function (e) {
            $(this).next().slideToggle(300);
              return false;
        });
    })*/
</script>


<!-- Create User js  -->

<script>
$("#treeview").hummingbird();
$( "#checkAll" ).click(function() {
  $("#treeview").hummingbird("checkAll");
});
$( "#uncheckAll" ).click(function() {
  $("#treeview").hummingbird("uncheckAll");
});
$( "#collapseAll" ).click(function() {
  $("#treeview").hummingbird("collapseAll");
});
$( "#checkNode" ).click(function() {
  $("#treeview").hummingbird("checkNode",{attr:"id",name: "node-0-2-2",expandParents:false});
});
</script>
