<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function Insert($data)
	{
		
		$query = $this->db->insert('groups', $data);
		
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}
	}
// Get Group  List
		public function GetGroup($data, $educator_id){

		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( (g.educator_id='{$educator_id}') AND (g.name like '%{$keyword1}%' OR g.description like '%{$keyword1}%' AND g.name like '%{$keyword2}%' OR g.description like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( (g.educator_id='{$educator_id}') AND (g.name like '%{$keyword1}%' OR g.description like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "g.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT g.id, g.name, g.description FROM `groups` as g WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	// Edit Function
 public function GetGroupById($id, $educator_id)
	{
		$this->db->where(
			[
				'id' => $id,
				'educator_id' => $educator_id
			]
		);
		return $this->db->get('groups');
	}

// Update Query For Selected Student

	public function UpdateGroup($data, $id)
	{
		$this->db->where("id", $id);
        $result = $this->db->update("groups", $data);
        if ($result) {
        	
        	return $id;
        } else {
        	return false;
        }
	}

	// Check data of group
	public function GetCheckData(){
		$this->db->select("*");
		$this->db->from("groups");
		$query = $this->db->get();
		return $query->result();
	}

	public function CountGroup($educator_id){
		return $this->db->simple_query("SELECT groups.id FROM `groups` where groups.educator_id='{$educator_id}'");
	}

	public function GetGroupUserData($id)
    {
        $this->db->select('groups.educator_id')
		->from('groups')
		->where('groups.id',$id);
		$query = $this->db->get()->row();
		return $query;
    }

	public function GetGroupUserList($data,$educator_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( (users.educator_id='{$educator_id}') AND (users.name like '%{$keyword1}%' AND users.name like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( (users.educator_id='{$educator_id}') AND (users.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "users.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT users.id, users.username FROM `users` WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountGroupUser($educator_id)
	{
		return $this->db->simple_query("SELECT users.id from users WHERE users.educator_id='{$educator_id}'");
	}

	public function GetGroupCourseList($data,$educator_id)
	{
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( (course.educator_id='{$educator_id}') AND (course.name like '%{$keyword1}%' AND course.name like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( (course.educator_id='{$educator_id}') AND (course.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "course.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT course.id, course.course_name, course.description FROM `course` WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountGroupCourse($educator_id)
	{
		return $this->db->simple_query("SELECT course.id from course WHERE course.educator_id='{$educator_id}'");
	}

	public function GetGroupFileData($educator_id,$group_id,$data)
	{
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";
			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((file_uploads.educator_id='{$educator_id}' AND file_uploads.group_id='{$user_id}') AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%' OR course_category.name like '%{$keyword1}%' AND file_uploads.name like '%{$keyword2}%' OR file_uploads.visibility like '%{$keyword2}%' OR file_uploads.type like '%{$keyword2}%' OR file_uploads.created_at like '%{$keyword2}%' OR course_category.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((file_uploads.educator_id='{$educator_id}' AND file_uploads.group_id='{$user_id}') AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "file_uploads.educator_id='{$educator_id}' AND file_uploads.	group_id='{$group_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT file_uploads.id, file_uploads.name, file_uploads.pre_name, file_uploads.visibility, file_uploads.type, file_uploads.size,file_uploads.created_at from file_uploads WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountGroupFileData($educator_id,$group_id)
	{
		return $this->db->simple_query("SELECT file_uploads.id from file_uploads WHERE file_uploads.educator_id='{$educator_id}' AND file_uploads.group_id='{$group_id}'");
	}

	public function GetFilesUploaded($ImageData)
	{
		$query = $this->db->insert('file_uploads', $ImageData);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			return  true;
		} else {
			return false;
		}
	}

	public function UodateFileName($file_id,$File_name)
	{
		# code...
	}
}