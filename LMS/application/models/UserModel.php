<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	// Username validation
	public function UserNameExist($username, $educator_id, $user_id)
	{
		$educator_cond = "";
    	if($educator_id != "") {
    		$educator_cond = " AND educator_id = '{$educator_id}'";
    	} else {
    		$educator_cond = "";
    	}
    	// if action update user cond
    	if ($user_id != "") {
			$user_id_cond = " AND id != '{$user_id}'"; 
		} else {
			$user_id_cond = "";
		}
		$query = $this->db->simple_query("SELECT id FROM users WHERE username = '{$username}' {$educator_cond} {$user_id_cond}");
		if($query->num_rows > 0) {	
			return true;
		} else {
			return false;
		}
	}

	// Email validation
	public function EmailExist($email, $educator_id, $user_id)
	{
		// educator condition
		$educator_cond = "";
    	if($educator_id != "") {
    		$educator_cond = " AND educator_id = '{$educator_id}'";
    	} else {
    		$educator_cond = "";
    	}
    	// if action update user cond
    	if ($user_id != "") {
			$user_id_cond = " AND id != '{$user_id}'"; 
		} else {
			$user_id_cond = "";
		}
		$query = $this->db->simple_query("SELECT id FROM users WHERE email = '{$email}' {$educator_cond} {$user_id_cond}");
		if($query->num_rows > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function Insert($data)
	{
		$query = $this->db->insert('users', $data);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}
	}

	// user update
	public function UpdateUser($data, $id)
	{
		$this->db->where("id", $id);
        $result = $this->db->update("users", $data);
        if ($result) {
        	return $id;
        } else {
        	return false;
        }
	}
	// Get User list
	public function GetUser($data, $educator_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( ( u.user_type = ut.id AND ut.id != 1 AND u.educator_id = '{$educator_id}' ) 
				AND ( u.full_name like '%{$keyword1}%' OR u.email like '%{$keyword1}%' OR ut.name like '%{$keyword1}%' OR DATE(u.created_at) like '%{$keyword1}%' AND u.full_name like '%{$keyword2}%' OR u.email like '%{$keyword2}%' OR ut.name like '%{$keyword2}%' OR DATE(u.created_at) like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( ( u.user_type = ut.id AND ut.id != 1 AND u.educator_id = '{$educator_id}')
				 AND (u.full_name like '%{$keyword1}%' OR u.email like '%{$keyword1}%' OR ut.name like '%{$keyword1}%' OR DATE(u.created_at) like '%{$keyword1}%' ) )";
			}
		} else {
			$search_cond = "u.user_type = ut.id AND u.educator_id = '{$educator_id}'";
			// $search_cond = "u.user_type = ut.id AND ut.id != 1 AND u.educator_id = '{$educator_id}'";
		}

		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("
			SELECT u.id, u.full_name, u.email, DATE(u.created_at) as registration_date, u.last_login, ut.name as user_type FROM users as u, user_type as ut WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}
			");
	}
	// Count user
	public function CountUser($educator_id)
	{
		return $this->db->simple_query("SELECT id from users where educator_id = '$educator_id' && user_type != 1");
	}

	// Get single user data
	public function GetUserById($id, $educator_id)
	{
		$this->db->where(
			[
				'id' => $id,
				'educator_id' => $educator_id
			]
		);
		return $this->db->get('users');
	}

	public function GetCourseList($id)
	{
		$this->db->where(
			[
				'id' => $id
			]
		);
		return $this->db->get('course');
	}


	public function Login($data)
    {
    	$email_username = "";
    	if($data["email"] === "") {
    		$email_username = " u.username = '{$data["username"]}'";
    	} else {
    		$email_username = " u.email = '{$data["email"]}'";
    	}

        $query = $this->db->simple_query("SELECT u.id, u.username, u.educator_id, u.password, e.domain_name, e.welcome_info FROM users as u, educator as e where e.domain_name = '{$data["domain_name"]}' AND e.id = u.educator_id AND {$email_username}");

        if($query->num_rows > 0) {
            $result = $query->fetch_assoc();
    		// password verification
    		if (password_verify($data["password"], $result["password"])) {
            	return $result;
    		} else {
    			return false;
    		} 
        } else {
            return false;
        }
    }

	public function Update_e_id($id)
	{
		$query = $this->db->query("select users.username,educator.domain_name,educator.user_id,educator.welcome_info from `users` INNER join `educator` on users.educator_id=educator.id where users.id='{$id}'")->row();
		return $query;
	}

	public function GetypeUserType()
	{
		$this->db->select("*");
		$this->db->from("user_type");
		$query = $this->db->get();
		return $query->result();
	}

	public function GetUserData($id)
	{
		$this->db->select('users.educator_id')
		->from('users')
		->where('users.id',$id);
		$query = $this->db->get()->row();
		return $query;
	}

	public function GetUserGroupList($data, $educator_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( ( groups.educator_id = '{$educator_id}' ) 
				AND ( groups.name like '%{$keyword1}%' AND groups.name  like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( ( groups.educator_id = '{$educator_id}') AND (groups.name like '%{$keyword1}%' ) )";
			}
		} else {
			$search_cond = "groups.educator_id = '{$educator_id}'";
		}

		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("
			SELECT groups.id, groups.name FROM `groups` WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}
			");
	}

	public function GetUserCourseList($data,$educator_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( ( course.educator_id = '{$educator_id}' ) 
				AND ( course.name like '%{$keyword1}%' AND course.name  like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( ( course.educator_id = '{$educator_id}') AND (course.name like '%{$keyword1}%' ) )";
			}
		} else {
			$search_cond = "course.educator_id = '{$educator_id}'";
		}

		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("
			SELECT course.id, course.course_name, course.created_at FROM course WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}
			");
	}

	public function GetUserFileData($data,$educator_id,$user_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";
			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((file_uploads.educator_id='{$educator_id}' AND file_uploads.user_id='{$user_id}') AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%' OR course_category.name like '%{$keyword1}%' AND file_uploads.name like '%{$keyword2}%' OR file_uploads.visibility like '%{$keyword2}%' OR file_uploads.type like '%{$keyword2}%' OR file_uploads.created_at like '%{$keyword2}%' OR course_category.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((file_uploads.educator_id='{$educator_id}' AND file_uploads.user_id='{$user_id}') AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "file_uploads.educator_id='{$educator_id}' AND file_uploads.user_id='{$user_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT file_uploads.id, file_uploads.name, file_uploads.pre_name, file_uploads.visibility, file_uploads.type, file_uploads.size,file_uploads.created_at from file_uploads WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountUserFileData($educator_id,$user_id)
	{
		$this->db->select('*')
		->from('file_uploads')
		->where(['file_uploads.educator_id'=>$educator_id,'file_uploads.user_id'=>$user_id]);
		$query = $this->db->get();
		return $query;
	}

	public function GetFilesUploaded($ImageData)
	{
		$query = $this->db->insert('file_uploads', $ImageData);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			return  true;
		} else {
			return false;
		}
	}

	public function UodateFileName($id,$File_name)
	{
		$data = [
			"name"=>$File_name
		];
		$this->db->where("id", $id);
        $result = $this->db->update("file_uploads", $data);
        if ($result) {
        	$response["success"] = true;
			$response["message"] = "File name update successfully!";
			echo json_encode($response);
        } else {
        	$response["success"] = false;
			$response["message"] = "File name not update!";
			echo json_encode($response);
        }
	}

	public function DeleteFileUser($id)
	{
		$query = $this->db->where('id',$id)->get("file_uploads")->row();
		$arr = explode('.', $query->pre_name);
		$file_name = $arr[0].$query->type;
		delete_files("uploads/users/files/".$file_name,TRUE);
		$this->db->where('id', $id);
  		$delete = $this->db->delete('file_uploads');
		if($delete){
			$response["success"] = true;
			$response["message"] = "File delete successfully!";
			echo json_encode($response);;
		}
		else{
			$response["success"] = false;
			$response["message"] = "File delete unsuccessfully!";
			echo json_encode($response);
		}
	}
}

/* End of file UserModel.php */
/* Location: .//tmp/fz3temp-2/UserModel.php */