<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class BranchModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function InsertBranch($BranchData)
	{
		$query = $this->db->insert('branch', $BranchData);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}  
	}

	public function TimeZoneData(){
		$query = $this->db->get("timezone");
		$timedatadata = $query->result();
		return $timedatadata;
	}

	public function CheckBranchName($branchname, $educator_id, $branch_id)
	{
    	if($educator_id != "") {
    		$educator_cond = " AND educator_id = '{$educator_id}'";
    	} else {
    		$educator_cond = "";
    	}
    	// if action update branch cond
    	if ($branch_id != "") {
			$branch_id_cond = " AND id != '{$branch_id}'"; 
		} else {
			$branch_id_cond = "";
		}

		// echo "SELECT id FROM `branch` WHERE `name` = '{$branchname}' {$educator_cond} {$branch_id_cond}"; die();

		$query = $this->db->simple_query("SELECT id FROM `branch` WHERE `name` = '{$branchname}' {$educator_cond} {$branch_id_cond}");
		if($query->num_rows > 0) {	
			return true;
		} else {
			return false;
		}
	}
	
	public function BranchList(){
		$query = $this->db->get("branch");
		$branchdata = $query->result();
		return $branchdata;
	}

	public function GetBranch($data, $educator_id){

		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( (b.educator_id='{$educator_id}') AND (b.name like '%{$keyword1}%' OR b.description like '%{$keyword1}%' AND b.name like '%{$keyword2}%' OR b.description like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( (b.educator_id='{$educator_id}') AND (b.name like '%{$keyword1}%' OR b.description like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "b.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT b.id, b.name, b.description FROM `branch` as b WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountBranches($educator_id)
	{
		return $this->db->simple_query("SELECT id from branch where educator_id = '$educator_id'");
	}


	public function Editdata($id)
	{
		$editbranchdata = $this->db->get_where('branch', array('id' => $id))->row();
		return $editbranchdata;
	}

	public function UpdateBranch($data, $id)
	{
		$this->db->where("id", $id);
        $result = $this->db->update("branch", $data);
        if ($result) {
        	return $id;
        } else {
        	return false;
        }
	}

	public function CloneData($clone_id, $name, $educator_id){

		if($clone_id!='' && $name!='' && $educator_id){
			
			//
			$this->db->select('*');
			$this->db->where('id', $clone_id);
			$branch_result = $this->db->get('branch');			
			$branch_data = $branch_result->result();

			
			// 
			$this->db->select('id');
			$this->db->where('count_of', $clone_id);
			$result = $this->db->get('branch');
			$count = $result->num_rows()+1;
			$new_branch_name = $branch_data[0]->name;
			$new_branch_name .= $count;
			
			
			// Branch data
            $BranchData = [
                "name" => $new_branch_name,
                "title" => $branch_data[0]->title,
                "description" => $branch_data[0]->description,
                "default_branch_theme" => $branch_data[0]->default_branch_theme,
                "language" => $branch_data[0]->language,
                "timezone" => $branch_data[0]->timezone,
                "internal" => $branch_data[0]->internal,
                "external" => $branch_data[0]->external,
                "default_user_type" => $branch_data[0]->default_user_type,
                "register_total_user" => $branch_data[0]->register_total_user,
                "e_commerce_processor" => $branch_data[0]->e_commerce_processor,
                "currency" => $branch_data[0]->currency,
                "paypal_email_address" => $branch_data[0]->paypal_email_address,
                "gamification" => $branch_data[0]->gamification,
                "educator_id" => $branch_data[0]->educator_id,
                "count_of" => $branch_data[0]->id,
            ];


          	return $this->InsertBranch($BranchData);
		}	
	}

	public function GetCourses($educator_id){		
		$SQL="SELECT course.id, course.educator_id, course.course_name from course inner join educator on educator.id=course.educator_id";    
	    $courses_data = $this->db->simple_query($SQL)->result();
	    return $courses_data;
		$this->db->where('educator_id', $educator_id);
		$this->db->get('courses', limit, offset);

	}

	public function Category()
	{

		$this->db->select(['id','name'])
		->from('course_category');
		$query = $this->db->get(); 
		return $query->result();
	}

	public function CourseCategory($educator_id)
	{	
		$this->db->select(['course.id','course_name','category'])
		->from('course')
		->join('course_category','course_category.id=course.category','INNER JOIN')
		->where('course_category.educator_id',$educator_id);
		$query = $this->db->get(); 
		return $query->result();
	}

	public function AddCoursesToAllBranches($educator_id, $course_id)
	{
		// Search of records not exist in course id
		$result = $this->db->simple_query("SELECT branch.id, branch.course_to_branch FROM `branch` WHERE JSON_SEARCH(branch.course_to_branch, 'all',$course_id ) IS NULL AND branch.educator_id = $educator_id");

		// GET COURSE NAME 
		$Course_name = $this->db->get_where('course', array('id' => $course_id))->row();

		$i= 0;

		// Check your course are exits and not
		if($result->num_rows > 0){
			while ($row = $result->fetch_assoc()) {
				$arr = json_decode($row['course_to_branch']);
				if(is_array($arr)){
					$arr[] = $course_id;
					$data = [
						'course_to_branch' => json_encode($arr)
					];
					$this->db->where('id',$row['id']);
					$this->db->update('branch', $data);
					$i++;
				}
				else{
					$data = [
						'course_to_branch' => json_encode(array($course_id))
					];
					$this->db->where('id',$row['id']);
					$this->db->update('branch', $data);
					$i++;
				}
			}
			$response["success"] = true;
			$response["message"] = "Success your course ".$Course_name->course_name." was added to ".$i." branches";
			echo json_encode($response);
		}
		else
		{
			$response["success"] = true;
			$response["message"] = "Your course ".$Course_name->course_name." already belongs to all branches";
			echo json_encode($response);
		}
	}

	public function DeleteCoursesToAllBranchModel($educator_id, $course_id)
	{
		// Search of records exist in course id
		$result = $this->db->simple_query("SELECT branch.id, branch.course_to_branch FROM `branch` WHERE JSON_SEARCH(branch.course_to_branch, 'all',$course_id ) AND branch.educator_id = $educator_id");

		// GET COURSE NAME 
		$Course_name = $this->db->get_where('course', array('id' => $course_id))->row();

		$i= 0;
		// Check your course are exits and not
		if($result->num_rows > 0){
			while ($row = $result->fetch_assoc()) {
				$arr = json_decode($row['course_to_branch']);
				$new_arr = [];
				$arr_count = count($arr);
				for($j=0; $j<$arr_count; $j++){
					if($arr[$j] != $course_id){
						$new_arr[] = $arr[$j];
					}
				}				
				if(is_array($new_arr)){
					$data = [
						'course_to_branch' => json_encode($new_arr)
					];
					$this->db->where('id',$row['id']);
					$this->db->update('branch', $data);
					$i++;
				}
			}
			$response["success"] = true;
			$response["message"] = "Success your removed ".$Course_name->course_name." from ".$i." branches";
			echo json_encode($response);
		}
		else
		{
			$response["success"] = false;
			$response["message"] = "course ".$Course_name->course_name." is not assigned to any branches";
			echo json_encode($response);
		}
	}

	//Get Branch Data
	public function GetBranchData($branch_id)
	{
		$this->db->select('branch.educator_id')
		->from('branch')
		->where('branch.id',$branch_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}
		else{
			return $query->result();
		}
	}

	// Get Educator
	public function GetBranchUser($educator_id,$data)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((users.educator_id='{$educator_id}') AND (users.username like '%{$keyword1}%' OR users.email like '%{$keyword1}%' OR user_type.name like '%{$keyword1}%' AND users.username like '%{$keyword2}%' OR users.email like '%{$keyword2}%' OR user_type.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((users.educator_id='{$educator_id}') AND (users.username like '%{$keyword1}%' OR users.email like '%{$keyword1}%' OR user_type.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "users.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT users.id, users.username, users.email, user_type.name FROM users INNER join user_type ON user_type.role_id=users.user_type WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	// Count of branch user
	public function CountBranchUser($educator_id)
	{
		return $this->db->simple_query("SELECT users.id, users.username, users.email, user_type.name FROM users INNER join user_type ON user_type.role_id=users.user_type WHERE users.educator_id='{$educator_id}'");	
	}

	// Get data of brach courses list
	public function GetBranchCouser($educator_id,$data)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((course.educator_id='{$educator_id}) AND (course.course_name like '%{$keyword1}%' OR course_category.name like '%{$keyword1}%' AND course.course_name like '%{$keyword2}%' OR course_category.name like '%{$keyword2}%' OR user_type.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((course.educator_id='{$educator_id}') AND (course.course_name like '%{$keyword1}%' OR course_category.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = " course.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT course.id, course.course_name, course_category.name from course left join course_category on course_category.id=course.category WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	// Count of select branch of all course
	public function CountBranchCourses($educator_id)
	{
		return $this->db->simple_query("SELECT course.id from course left join course_category on course_category.id=course.category WHERE course.educator_id='{$educator_id}'");
	}

	// Upload function of file

	public function GetFilesUploaded($ImageData)
	{
		$query = $this->db->insert('file_uploads', $ImageData);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			return  true;
		} else {
			return false;
		}
	}

	// Get data of all files
	public function GetBranchFileData($educator_id,$branch_id,$data)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";
			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((file_uploads.educator_id='{$educator_id}) AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%' OR course_category.name like '%{$keyword1}%' AND file_uploads.name like '%{$keyword2}%' OR file_uploads.visibility like '%{$keyword2}%' OR file_uploads.type like '%{$keyword2}%' OR file_uploads.created_at like '%{$keyword2}%' OR course_category.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((file_uploads.educator_id='{$educator_id}') AND (file_uploads.name like '%{$keyword1}%' OR file_uploads.visibility like '%{$keyword1}%' OR file_uploads.type like '%{$keyword1}%' OR file_uploads.created_at like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "file_uploads.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT file_uploads.id, file_uploads.name, file_uploads.visibility, file_uploads.type, file_uploads.size,file_uploads.created_at from file_uploads WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	// Count of based on select brach files
	public function CountBranchFileData($educator_id,$branch_id)
	{
		$this->db->select('*')
		->from('file_uploads')
		->where(['file_uploads.educator_id'=>$educator_id,'file_uploads.branch_id'=>$branch_id]);
		$query = $this->db->get();
		return $query;
	}
	
	public function UodateFileName($id,$File_name)
	{
		$data = [
			"name"=>$File_name
		];
		$this->db->where("id", $id);
        $result = $this->db->update("file_uploads", $data);
        if ($result) {
        	$response["success"] = true;
			$response["message"] = "File name update successfully!";
			echo json_encode($response);
        } else {
        	$response["success"] = false;
			$response["message"] = "File name not update!";
			echo json_encode($response);
        }
	}

	public function DeleteFileBranch($id){
		$query = $this->db->where('id',$id)->get("file_uploads")->row();
		$arr = explode('.', $query->name);
		$file_name = $arr[0].$query->type;
		delete_files("upload/users/branches/files/".$file_name,TRUE);
		$this->db->where('id', $id);
  		$delete = $this->db->delete('file_uploads');
		if($delete){
			$response["success"] = true;
			$response["message"] = "File delete successfully!";
			echo json_encode($response);;
		}
		else{
			$response["success"] = false;
			$response["message"] = "File delete unsuccessfully!";
			echo json_encode($response);
		}
	}
}

/* End of file BranchModel.php */
/* Location: .//tmp/fz3temp-2/BranchModel.php */