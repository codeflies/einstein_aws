<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserTypeModel extends CI_Model {

	// Get module 
	public function GetModule()
	{
		return $this->db->simple_query("SELECT id, name from module_type");
	}

	// Get Roles
	public function GetRole()
	{
		return $this->db->simple_query("SELECT id, name from role_type where id != '1'");
	}


	// Get Permission Category by module
	public function GetPermissionCategory($module_id)
	{
		$this->db->select('id, category, parent_category_id');
		$this->db->where([
			'module_type_id' => $module_id
		]);
		return $this->db->get('permission_category');
	}

	// Get child category id
	public function GetSubCategory($parent_id)
	{
		$this->db->select('id, category');
		$this->db->where([
			'parent_category_id' => $parent_id
		]);
		return $this->db->get('permission_category');
	}	

	// Get Permission category action
	public function GetPermissionAction($category_id)
	{
		$this->db->select('id, action_name');
		$this->db->where('category_id', $category_id);
		return $this->db->get('permission_action');
	}
}
	
	/* End of file UserTypeModel.php */
