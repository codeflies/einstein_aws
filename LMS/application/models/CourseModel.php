<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function Insert($data)
	{
		
		$query = $this->db->insert('course', $data);
		
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}
	}


	public function CourseList(){
		$query = $this->db->get("course");
		$data = $query->result();
		return $data;
	
	}

public function GetCourse($data, $educator_id){

		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( (n.educator_id='{$educator_id}') AND (n.course_name like '%{$keyword1}%' OR n.category like '%{$keyword1}%' OR n.update_at like '%{$keyword1}%' AND n.course_name like '%{$keyword2}%' OR n.category like '%{$keyword2}%'ADD n.update_at like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( (n.educator_id='{$educator_id}') AND (n.course_name like '%{$keyword1}%' OR n.category like '%{$keyword1}%' OR n.update_at like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "n.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		return $this->db->simple_query("SELECT  n.id, n.course_name, n.category , n.update_at, course_category.name FROM `course` as n left join course_category on n.category=course_category.id
			WHERE {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}
	public function CountCourse($educator_id)
	{
		return $this->db->simple_query("SELECT id from course where educator_id = '$educator_id'");
	}

/// Colone 
	public function CloneData($clone_id, $course_name, $educator_id){
		if($clone_id!='' && $course_name!='' && $educator_id){
			$this->db->select('*');
			$this->db->where('id', $clone_id);
			$course_result = $this->db->get('course');			
			$course_data = $course_result->result();

			$this->db->select('id');
			$this->db->where('count_of', $clone_id);
			$result = $this->db->get('course');
			$count = $result->num_rows()+" "+1;
			$new_course_name = $course_data[0]->course_name;
			$new_course_name .= $count;
			
			// Course data
           	$CourseData = [
                    "course_name" =>$new_course_name." (clone)",
                    "category" =>$course_data[0]->category,
                    "description" => $course_data[0]->description,
                    "course_code" => $course_data[0]->course_code,
                    "price" => $course_data[0]->price,
                    "Capacity" => $course_data[0]->Capacity,
                    "course_expiration"=> $course_data[0]->course_expiration,
                    "expiration_date" => $course_data[0]->expiration_date,
                    "certification" => $course_data[0]->certification,
                    "level" =>$course_data[0]->level,
                     "educator_id" => $course_data[0]->educator_id,
                     "count_of" => $course_data[0]->id,
                ];
          	return $this->Insert($CourseData);
		}	
	}

	// Edit function
	 public function GetCourseById($id, $educator_id)
	{
		$this->db->where(
			[
				'id' => $id,
				'educator_id' => $educator_id
			]
		);
		return $this->db->get('course');
	}
	public function UpdateCourse($data, $id)
	{
		$this->db->where("id", $id);
        $result = $this->db->update("course", $data);

		if ($result == true) {
        	return $id;
        } else {
        	return false;
        }
	}

	public function Getcategory($educator_id)
	{
		// $query = $this->db->get("course_category");
		// $cat_data = $query->result();
		// return $cat_data;
		$this->db->select('*');
        $this->db->from('course_category');
        $this->db->where('parent_categories', 0);
		$this->db->where('educator_id',$educator_id);
 
        $parent = $this->db->get();
        
        $cat_data = $parent->result();
        $i=0;
        foreach($cat_data as $main_cat){
 
            $cat_data[$i]->sub = $this->sub_category($main_cat->id,$educator_id);
            $i++;
        }
        return $cat_data;
	}

	public function sub_category($id,$educator_id){
 
        $this->db->select('*');
        $this->db->from('course_category');
        $this->db->where('parent_categories', $id);
		$this->db->where('educator_id',$educator_id);
 
        $child = $this->db->get();
        $cat_data = $child->result();
        $i=0;
        foreach($cat_data as $sub_cat){
 
            $cat_data[$i]->sub = $this->sub_category($sub_cat->id,$educator_id);
            $i++;
        }
        return $cat_data;      
    }

	// check of course
	public function GetCourseData($course_id)
	{
		$query = $this->db->get("course where course.id='{$course_id}'")->row();
		return $query;
	}

	// list of branch based on select branch
	public function GetCourseBranchData($educator_id,$data)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((branch.educator_id='{$educator_id}') AND (branch.name like '%{$keyword1}%' AND branch.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((branch.educator_id='{$educator_id}') AND (branch.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "branch.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}
		return $this->db->simple_query("SELECT branch.id,branch.name from branch where {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	// Count of branch based on select course

	public function CountCourseBranch($educator_id)
	{
		$query = $this->db->query("SELECT branch.id,branch.name from branch where branch.educator_id=$educator_id");
		return $query->num_rows();
	}

	public function GetCourseGroupData($educator_id,$data){
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "((groups.educator_id='{$educator_id}') AND (groups.name like '%{$keyword1}%' AND groups.name like '%{$keyword2}%') )";
			} else {
				$search_cond = "((groups.educator_id='{$educator_id}') AND (groups.name like '%{$keyword1}%'))";
			}
		}
		else{
			$search_cond = "groups.educator_id='{$educator_id}'";
		}
		
		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}
		return $this->db->simple_query("SELECT groups.id,groups.name from `groups` where {$search_cond} {$order_cond} LIMIT {$data["offset"]}, {$data["limit"]}");
	}

	public function CountCourseGroup($educator_id){
		$query = $this->db->query("SELECT groups.id,groups.name from `groups` where groups.educator_id=$educator_id");
		return $query->num_rows();
	}

	// Function of add Member
	public function AddGroupMember($educator_id,$group_id,$id){
		$query = $this->db->get("course where course.id='{$id}' AND course.educator_id='{$educator_id}'")->row();
		$member_id = json_decode($query->group_member);
		if(!empty($member_id)){
			$member_id[] = $group_id;
		}
		else{
			$member_id = [$group_id];
		}
		$data = [
			'group_member'=>json_encode($member_id)
		];
		$this->db->where(["id"=>$id,"educator_id"=>$educator_id]);
        $result = $this->db->update("course", $data);
		if ($result) {
        	return true;
        } else {
        	return false;
        }
	}

	public function MinusGroupMember($educator_id,$group_id,$id){
		$query = $this->db->get("course where course.id='{$id}' AND course.educator_id='{$educator_id}'")->row();
		$member_id = json_decode($query->group_member);
		$r_arr = array_keys($member_id,$group_id);
		unset($member_id[$r_arr[0]]);
		$data = [
			'group_member'=>json_encode($member_id)
		];
		$this->db->where(["id"=>$id,"educator_id"=>$educator_id]);
        $result = $this->db->update("course", $data);
		if ($result) {
        	return true;
        } else {
        	return false;
        }
	}
}
