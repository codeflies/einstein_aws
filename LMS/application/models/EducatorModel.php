<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EducatorModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	// domain validation
	public function DomainExist($domain)
	{
		$this->db->select('domain_name');
		$this->db->where('domain_name', $domain);
		$query = $this->db->get('educator');
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Educator insert
	public function Insert($data)
	{
		$query = $this->db->insert('educator', $data);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}
	}

	// Update educator
	public function Update($data, $id)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('educator', $data);
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			return  true;
		} else {
			return false;
		}	
	}

	// getEducator ID
	public function GetEducatorId($domain_name)
	{
		$this->db->select('id');
		$this->db->where('domain_name', $domain_name);
		$query = $this->db->get('educator');
		if($query->num_rows() > 0) {
			return $query;
		} else {
			return false;
		}
	}

}

/* End of file EducatorModel.php */
