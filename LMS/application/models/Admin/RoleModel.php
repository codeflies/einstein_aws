<?php 


class RoleModel extends CI_Model {

	public function InsertData($data){
      return $this->db->insert('role_type', $data);
	}

	public function RoleData(){
		$query = $this->db->get("role_type");
		$roledata = $query->result();
		return $roledata;
	}

	public function EditRoleData($id){
		$editroledata = $this->db->get_where('role_type', array('id' => $id))->row();
		return $editroledata;
	}

	public function UpdateRoleData($id){
		$data=array(
		  	'name'=>$this->input->post('name'),
		  	'is_active'=>$this->input->post('is_active')
        );
        if($id==0){
            return $this->db->insert('role_type',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('role_type',$data);
        }   
	}

}

/* End of file RoleModel.php */
/* Location: .//tmp/fz3temp-2/RoleModel.php */
