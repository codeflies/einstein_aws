<?php 


class ModuleTypeModel extends CI_Model {

	
	public function InsertData($data){
      return $this->db->insert('module_type', $data);
	}

	public function ModuleTypeData(){
		$query = $this->db->get("module_type");
		$roledata = $query->result();
		return $roledata;
	}

	public function EditModuleTypeData($id){
		$editroledata = $this->db->get_where('module_type', array('id' => $id))->row();
		return $editroledata;
	}

	public function UpdateModuleTypeData($id){
		$data=array(
		  	'name'=>$this->input->post('name'),
		  	'is_active'=>$this->input->post('is_active')
        );
        if($id==0){
            return $this->db->insert('module_type',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('module_type',$data);
        }   
	}
}

/* End of file RoleModel.php */
/* Location: .//tmp/fz3temp-2/RoleModel.php */
