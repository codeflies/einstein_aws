<?php

class RBASModel extends CI_Model {

	public function RoleData()
	{
		$query = $this->db->get("module_type");
		$roledata = $query->result();
		return $roledata;
	}

	public function CategoryData()
	{
		$query = $this->db->get("perm_categories");
		$categorydata = $query->result();
		return $categorydata;
		
	}

}

/* End of file RBASModel.php */
/* Location: .//tmp/fz3temp-2/RBASModel.php */