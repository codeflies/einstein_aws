<?php 


class CategoryModel extends CI_Model {

	public function InsertData($data){
      return $this->db->insert('perm_categories', $data);
	}

	public function CategoryData(){
		$query = $this->db->get("perm_categories");
		$categorydata = $query->result();
		return $categorydata;
	}

	public function EditCategoryData($id){
		$editcategory = $this->db->get_where('perm_categories', array('id' => $id))->row();
		return $editcategory;
	}

	public function UpdateCategoryData($id){
		$data=array(
		  	'name'=>$this->input->post('name'),
		  	'is_active'=>$this->input->post('is_active')
        );
        if($id==0){
            return $this->db->insert('perm_categories',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('perm_categories',$data);
        }   
	}

	public function Moduledata(){
		$query = $this->db->get("module_type");
		$moduletypedata = $query->result();
		return $moduletypedata;
	}

}

/* End of file CategoryModel.php */
/* Location: .//tmp/fz3temp-2/CategoryModel.php */