<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function CheckName($name, $educator_id){
		$query = $this->db->simple_query("SELECT id FROM course_category WHERE name = '{$name}' && educator_id = '{$educator_id}'");
		if($query->num_rows > 0) {	
			return true;
		} else {
			return false;
		}
	}


	public function Insert($data)
	{
		
		$query = $this->db->insert('course_category', $data);
		
		$affected_rows = $this->db->affected_rows();
		if($affected_rows > 0) {
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		} else {
			return false;
		}
	}

	// user category
	public function UpdateUser($data, $id)
	{
		$this->db->where("id", $id);
	        $result = $this->db->update("course_category", $data);
	        return $result;
	}

	// Get User list
	public function GetUser()
	{
		return $this->db->get('course_category');
	}

	public function get_categories($educator_id){
		$this->db->select('*');
        $this->db->from('course_category');
        $this->db->where('parent_categories', 0);
		$this->db->where('educator_id',$educator_id);
 
        $parent = $this->db->get();
        
        $categories = $parent->result();
        $i=0;
        foreach($categories as $main_cat){
 
            $categories[$i]->sub = $this->sub_category($main_cat->id);
            $i++;
        }
        return $categories;
	}

	public function sub_category($id){
 
        $this->db->select('*');
        $this->db->from('course_category');
        $this->db->where('parent_categories', $id);
 
        $child = $this->db->get();
        $categories = $child->result();
        $i=0;
        foreach($categories as $sub_cat){
 
            $categories[$i]->sub = $this->sub_category($sub_cat->id);
            $i++;
        }
        return $categories;      
    }


	// Get Category list
	public function GetCategory($data, $educator_id)
	{
		// Search condition
		if ($data['search'] != "") {
			$keyword1 = explode(" ", $data["search"])[0];
			$keyword2 = explode(" ", $data["search"])[1] ?? "";

			if ($keyword1 != "" AND $keyword2 != "") {
				$search_cond = "( ( course_category.educator_id ='{$educator_id}' AND course_category.parent_categories='0' ) 
				AND ( course_category.name like '%{$keyword1}%' AND course_category.name like '%{$keyword2}%' ) )";
			} else {
				$search_cond = "( ( course_category.educator_id ='{$educator_id}' AND course_category.parent_categories='0' )
				 AND ( course_category.name like '%{$keyword1}%' ) )";
			}
		} else {
			$search_cond = "course_category.educator_id ='{$educator_id}' AND course_category.parent_categories='0'";
		}

		// Sorting condition
		if($data['sort'] != "" AND $data['order_by'] != "") {
			$order_cond = " ORDER BY {$data["sort"]} {$data["order_by"]}";
		} else  {
			$order_cond = "";
		}

		$this->db->select('*');    
		$this->db->from('course_category');
		$this->db->where($search_cond);
		//$this->db->limit($data["offset"],$data["limit"]);
		
		
 
        $parent = $this->db->get();
        
        $categories = $parent->result();

        $i=0;
        foreach($categories as $main_cat){
 
            $categories[$i]->sub = $this->sub_category($main_cat->id);
            $i++;
        }

		return $categories;
       
		
		// return $this->db->simple_query("
		// 	SELECT id, name from course_category where  {$search_cond} {$order_cond} and parent_categories=0 LIMIT {$data["offset"]}, {$data["limit"]}
		// ");
	}

// Update Function


	// Update Query For Selected Student

	public function Updatecategory($data, $id)
	{
		$this->db->where("id", $id);
        $result = $this->db->update("course_category", $data);
        if ($result) {
        	
        	return $id;
        } else {
        	return false;
        }
	}
// Edit Function
	 public function GetCategoryById($id, $educator_id)
	{
		$this->db->where(['id' => $id,'educator_id' => $educator_id]);
		return $this->db->get('course_category');
	}
}