<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

       public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library('form_validation');
        $this->load->Model('GroupModel');
        $this->load->Model('UserModel');
        }

    // Create  Group
    public function Create()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("add_group") !== null)
        {
            $this->GroupCreateUpdate($this->input->post("add_group"));
        } else {
            do {
                $uniqkey = uniqid();
                $exist = false;
                // query to check key exist or not
                $result = $this->db->simple_query("SELECT id FROM `groups` where group_key = '{$uniqkey}'");
                if ($result->num_rows > 0) {
                    $exist = true;
                }
            } while ($exist);
            // return key
            // return $uniqkey;
            $data['key'] = $uniqkey;
            $this->load->view('group/create',$data);
        }
    }
  
 // Group UPDATE FUNCTION
    
    public function Update()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("update_group") !== null && $this->input->post("user_id") !== null)
        {
            $this->GroupCreateUpdate($this->input->post("update_group"));
        } else {
            $this->load->view('group/list');
        }
    }

   // Common method for Group add and update
  
    private function GroupCreateUpdate(string $action)
    {   
        if($this->input->server('REQUEST_METHOD') == "POST" && $action != "")
        {
            // response object
            $response = [
                'success' => false,
                'message' => '',
                'error' => array(),
                'data' => array(),
                'target' =>  ''
            ];
       
            // form validation
            $this->form_validation->set_rules(
                'name','Name ','trim|required|min_length[4]|max_length[21]'
            );
            // Check branch name
           
            $this->form_validation->set_rules(
                'description','description','trim|required'
            );
            // $this->form_validation->set_rules(
            //     'price','price','trim|required'
            // );
            $this->form_validation->set_rules(
                'group_key','group_key','trim|required'
            );

            // Setting update category id according to action
            if ($action === "add_group") {
                $group_id_update = "";
            } else {
                // category update id 
                $group_id_update = $this->input->post("user_id"); 
            }
            // EDUCATOR DETAILS
            $educator_id = $this->session->userdata["educator_id"];
            if($this->form_validation->run() == false)
            {
                $response['error'] = $this->form_validation->error_array();
                $response['message'] = 'Please fill the form correctly...!';
                echo json_encode($response);
                exit;
            } else {
                // Category data
                $GroupData = [
                    "name" => $this->input->post("name"),
                    "description" => $this->input->post("description"),
                    "price" => $this->input->post("price"),
                    "group_key" => $this->input->post("group_key"),
                    "educator_id" => $educator_id
                ];

                 // ADD Update condtion 
                if ($action === "add_group") {
                    // response message
                    $return_message = "Group Added Successfully";
                    // Insert into Users
                    $result = $this->GroupModel->Insert($GroupData); 
                } else if ($action === "update_group") {
                    // response message
                    $return_message = "Group Updated Successfully";
                    // Update User
                    $result = $this->GroupModel->UpdateGroup($GroupData, $group_id_update);
                }
                // Insert into CategoryData
                
                if($result === false) { 
                    $response["message"] = "Some error occured..!";
                    echo json_encode($response);
                    exit;
                } else {
                    // Returning Response
                    $response["success"] = true;
                    $response["message"] = $return_message;
                    $response["target"] = base_url()."group/list";
                    echo json_encode($response);
                }
            }
        } else {
            $this->load->view('group/list');
        }
    }
    // End Function

    public function List(){
        $data['result'] = $this->GroupModel->GetCheckData();
            $this->load->view('group/list',$data);
        }
    public function datalist(){
        // EDUCATOR DETAILS
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch user
        $list = $this->GroupModel->GetGroup($data, $educator_id);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->GroupModel->CountGroup($educator_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $list->num_rows;
        }   
        else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);
    }

    public function edit($id){
        // var_dump($id); die();
        // EDUCATOR DETAILS
        $educator_id = $this->session->userdata["educator_id"];
        $result = $this->GroupModel->GetGroupById($id, $educator_id);
        $data['GroupData'] = $result->result();
        $data['group_id'] = $id;
        $this->load->view('group/edit', $data);
    }
    
    // Delete Function
    public function Delete(){
        $delete_group = $this->input->post("group_id");
        $this->db->where('id', $delete_group);
        $this->db->delete('groups');
        $err_arr = $this->form_validation->error_array(); 
        $response["success"] = true;
        $response['message'] = "Group Delete Successfully!";
        $response['error'] = $err_arr;
        echo json_encode($response);
    }

    public function GroupUser($group_id)
    {
        $check_group_user__data['result'] = $this->GroupModel->GetGroupUserData($group_id);
        if($check_group_user__data['result']){
            $data['educator_id'] = $check_group_user__data['result']->educator_id;
            $data['group_id'] = $group_id;
            $this->load->view('group/user',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function GroupUsersList($educator_id)
    {
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch user
        $list = $this->GroupModel->GetGroupUserList($data, $educator_id);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->GroupModel->CountGroupUser($educator_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $list->num_rows;
        }   
        else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);
    }

    public function GroupCourse($group_id)
    {
        $check_group_course__data['result'] = $this->GroupModel->GetGroupUserData($group_id);
        if($check_group_course__data['result']){
            $data['educator_id'] = $check_group_course__data['result']->educator_id;
            $data['group_id'] = $group_id;
            $this->load->view('group/course',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function GroupCourseList($educator_id)
    {
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch user
        $list = $this->GroupModel->GetGroupCourseList($data, $educator_id);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->GroupModel->CountGroupCourse($educator_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $list->num_rows;
        }   
        else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);
    } 
    
    public function GroupFiles($group_id)
    {
        $check_group_files__data['result'] = $this->GroupModel->GetGroupUserData($group_id);
        if($check_group_files__data['result']){
            $data['educator_id'] = $check_group_files__data['result']->educator_id;
            $data['group_id'] = $group_id;
            $this->load->view('group/files',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function GroupFilesList($group_id)
    {
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch Courses
        $file_data = $this->GroupModel->GetGroupFileData($educator_id,$group_id,$data);
        
        if ($file_data->num_rows > 0) {
            // $fetch_data = $file_data->fetch_assoc();
            foreach($file_data as $row){
                $arr['rows'][] = $row;
            }
            $total_count = $this->GroupModel->CountGroupFileData($educator_id,$group_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $file_data->num_rows;
        } else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);
    }

    public function GroupUploadImage()
    {
        $educator_id = $this->session->userdata["educator_id"];
        $group_id = $this->input->post("group_id");
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);

        if(!empty($_FILES["image"]["tmp_name"])){ 
            $file_size = $_FILES['image']["size"]/1024;

            // File upload configuration 
            $targetDir = "uploads/group/files"; 
            $allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg', 'gif','mp4'); 
            $fileName = basename($_FILES['image']['name']); 
            $targetFilePath = $targetDir."/".$fileName; 

            // Type of file
            $file_type = strstr($fileName,".");
            // Check whether file type is valid 
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to the server 
                if(move_uploaded_file($_FILES['image']['tmp_name'], $targetFilePath) && ($group_id!='')){ 
                    // Branch data
                    $ImageData = [
                        "educator_id" => $educator_id,
                        "group_id" => $group_id,
                        "name" => $fileName,
                        "type" => $file_type,
                        "size" => intval($file_size).' KB',
                        "pre_name" => $fileName
                    ];
                    $check_branch_data = $this->GroupModel->GetFilesUploaded($ImageData);
                    $response["success"] = true;
                    $response["message"] = "File has uploaded successfully!";
                } 
            } 
        } 
        else{
            $response["success"] = false;
            $response["message"] = "File uploaded unsuccessfully!";
        }
        echo json_encode($response); 
    }

    public function RenameFile()
    {
        $file_id = $this->input->post("name_id");
        $File_name = $this->input->post("rename");
        $this->UserModel->UodateFileName($file_id,$File_name);
    }

    public function DeleteUserFile()
    {
        $file_id = $this->input->post("file_id");
        $this->UserModel->DeleteFileUser($file_id);
    }
}

   