<?php
   defined('BASEPATH') OR exit('No direct script access allowed');

   class Accountsettings extends CI_Controller {

      public function __construct()
    {
        parent::__construct();

        


    
    }

         public function Basic()
         {
            $this->load->view('accountsettings/basic');
        }

        public function Certifications(){
            $this->load->view('accountsettings/certifications');
        }



         public function Cms(){
            $this->load->view('accountsettings/cms');
        }

         public function Domain(){
            $this->load->view('accountsettings/domain');
        }

         public function Ecommerce(){
            $this->load->view('accountsettings/ecommerce');
        }


         public function Gamification(){
            $this->load->view('accountsettings/gamification_customizebadges');
        }

         public function Subscription(){
            $this->load->view('accountsettings/subscription');
        }


       public function Themes(){
            $this->load->view('accountsettings/themes');
        }



         public function Users(){
            $this->load->view('accountsettings/users');
        }



    }

   