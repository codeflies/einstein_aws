<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Category extends CI_Controller {

        public function __construct()
    {
        parent::__construct();
       
        
        $this->load->helper(array('form','url'));
        $this->load->library('form_validation');
         $this->load->Model('CategoryModel');
    }

//Check  name availability
    public function Checkname($name, $educator_id)
    {
    
        $exist = $this->CategoryModel->CheckName($name,$educator_id);
        return $exist;
    }
 // Create Category
    public function Create()
    {
        // EDUCATOR DETAILS
        $educator_id = $this->session->userdata["educator_id"];

        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("add_category") !== null)
        {
            $this->CategoryCreateUpdate($this->input->post("add_category"));
        } else {
           $data['category'] = $this->CategoryModel->get_categories($educator_id);

        //    $select_start = '<select class="form-control select2bs4" name="parent_categories">';
        //    $select_option = '<option selected="selected">Select a Parent Category</option>';
            
        //    $html_option = $this->fetch_menu($data['category']);
        //    $select_close = '</select>';

        //     $data['select'] = array(
        //         'select' => $select_start,
        //         'select_option' => $select_option,
        //         'options' => $this->fetch_menu($data['category']),
        //         'select_close' => $select_close
        //     );
	        
            $this->load->view('category/create' ,$data);
        }
    }
    // CATEGORY UPDATE FUNCTION
    
    public function Update()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("update_category") !== null && $this->input->post("user_id") !== null)
        {
            $this->CategoryCreateUpdate($this->input->post("update_category"));
        } else {
            $this->load->view('category/list');
        }
    }
         // Common method for Category add and update
  
    private function CategoryCreateUpdate(string $action)
    {   
        if($this->input->server('REQUEST_METHOD') == "POST" && $action != "")
        {
            // response object
            $response = [
                'success' => false,
                'message' => '',
                'error' => array(),
                'data' => array(),
                'target' =>  ''
            ];
       
            // form validation
            $this->form_validation->set_rules(
                'name','Name ','trim|required|min_length[4]|max_length[21]'
            );
            // Check branch name
           
            $this->form_validation->set_rules(
                'parent_categories','Parent Categories ','trim|required'
            );
            // $this->form_validation->set_rules(
            //     'price','price','trim|required'
            // );
            // Setting update category id according to action
            if ($action === "AddCategory") {
                $category_id_update = "";
            } else {
                // category update id 
                $category_id_update = $this->input->post("user_id"); 
            }
            // EDUCATOR DETAILS
            $educator_id = $this->session->userdata["educator_id"];
            if($this->form_validation->run() == false)
            {
                $response['error'] = $this->form_validation->error_array();
                $response['message'] = 'Please fill the form correctly...!';
                echo json_encode($response);
                exit;
            } else {
                // Category data
                $CategoryData = [
                    "name" => $this->input->post("name"),
                    "parent_categories" => $this->input->post("parent_categories"),
                    "price" => $this->input->post("price"),
                    "educator_id" => $educator_id
                ];
                 // ADD Update condtion 
                if ($action === "AddCategory") {
                    // response message
                    $return_message = "Category Added Successfully";
                    // Insert into Users
                    $result = $this->CategoryModel->Insert($CategoryData);    
                } else if ($action === "UpdateCategory") {
                    // response message
                    $return_message = "Category Updated Successfully";
                    // Update User
                    $result = $this->CategoryModel->Updatecategory($CategoryData, $category_id_update);
                }
                // Insert into CategoryData
                
                if($result === false) { 
                    $response["message"] = "Some error occured..!";
                    echo json_encode($response);
                    exit;
                } else {
                    // Returning Response
                    $response["success"] = true;
                    $response["message"] = $return_message;
                    $response["target"] = base_url()."category/list";
                    echo json_encode($response);
                }
            }
        } else {
            $this->load->view('category/list');
        }
    }
    // List Category
   public function List(){
        $this->load->view('category/list');
    }
   public function datalist()
        {
            // EDUCATOR DETAILS
            $educator_id = $this->session->userdata["educator_id"];
            // Get value
            $data = [
                'sort' => $this->input->get("sort") ?? "",
                'search' => $this->input->get("search") ?? "",
                'order_by' => $this->input->get("order") ?? "",
                'offset' => $this->input->get("offset") ?? "",
                'limit' => $this->input->get("limit") ?? ""
            ];
            $arr['rows'] = [];
            $list = $this->CategoryModel->GetCategory($data, $educator_id);
            //echo "<pre>";print_r(count($list));
            //$arr['parent'][] = $this->fetch_menu($category);

            if (count($list) > 0) {
            foreach($list as $lists){
            // while($row = $list) {
                if(count($list) >= 0){   
                    $arr['rows'][]= $lists;

                if($lists->sub != ''){
                    foreach($lists->sub as $sub){

                        if(($lists->id) == ($sub->parent_categories)){
                            $nameValue = $sub->name;

                            $sub->name = "&nbsp;>&nbsp;".$nameValue;
    
                            $arr['rows'][]= $sub;
                        }
                        if($sub->sub != ''){
                       foreach($sub->sub as $subtwo){

                            if(($sub->id) == ($subtwo->parent_categories)){
                            $secondValue = $subtwo->name;

                            $subtwo->name = "&nbsp;&nbsp;>>&nbsp;".$secondValue ;    

                            $arr['rows'][]= $subtwo;
                            }

                            
                            if($subtwo->sub != ''){
                            foreach($subtwo->sub as $subthree){

                                if(($subtwo->id) == ($subthree->parent_categories)){
                                    $thirdValue = $subthree->name;
                                    $subthree->name = "&nbsp;&nbsp;&nbsp;>>>&nbsp;".$thirdValue;
                                    $arr['rows'][]= $subthree;
                                }
                            }
                        }

                            // foreach($subthree->sub as $subfour){

                            //     if(($subthree->id) == ($subfour->parent_categories)){
                            //         $fourValue = $subfour->name;
                            //         $subfour->name = "&nbsp;&nbsp;&nbsp;&nbsp;>>>>&nbsp;".$fourValue;
                            //         $arr['rows'][]= $subfour;
                            //     }
                            // }

                            // foreach($subfour->sub as $subfive){

                            //     if(($subfour->id) == ($subfive->parent_categories)){
                            //         $fiveValue = $subfive->name;
                            //         $subfive->name = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>>>&nbsp;".$fiveValue;
                            //         $arr['rows'][]= $subfive;
                            //     }
                            // }

                        } 
                      }
                    }
                    
                }  
                }else{
                    $arr['rows'][]= $lists;
                }
                
             }
         }   else {
                $arr['rows'] = [];
        }
        
        echo json_encode($arr);
        // echo"<pre>";print_r(json_encode($arr));
    }
        // Creategory info
    public function edit($id)
        {
            $educator_id = $this->session->userdata["educator_id"];
            $result = $this->CategoryModel->GetCategoryById($id, $educator_id);
            $data['CategoryData'] = $result->result();
            $result = $this->CategoryModel->get_categories($educator_id);
            foreach($result as $row){
                $data['parent_category'][] = $row;
            }
            $this->load->view('category/edit', $data);
        }
// Delete Function
        public function Delete(){
        $delete_category = $this->input->post("category_id");

        /*Select child */
        $this->db->select('*');
        $this->db->from('course_category');
        $this->db->where('parent_categories', $delete_category);
        $del = $this->db->get();
        $results = $del->result();

        $update_child = $results[0]->id;
        /*Update Child*/
        $data = array(
            'parent_categories' => '0'
            );
        
        $this->db->set($data);
        $this->db->where('id', $update_child);
        $this->db->update('course_category');

        /*Delete*/

        $this->db->where('id', $delete_category);
        $this->db->delete('course_category');
        $err_arr = $this->form_validation->error_array(); 
        $response["success"] = true;
        $response['message'] = "Category Delete Successfully!";
        $response['error'] = $err_arr;
        echo json_encode($response);
    }
    

}

   