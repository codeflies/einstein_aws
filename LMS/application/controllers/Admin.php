<?php

class Admin extends CI_Controller {

	public function __construct(){

	        parent::__construct();
			$this->load->helper(array('form','url'));
	  	 	$this->load->model('Admin/AdminModel');
	  	 	// For authentication in every method except index and register
	        $class = $this->router->fetch_class();
	        $method = $this->router->fetch_method();
	        if($method !== 'login') {
	            if (!isset($this->session->userdata['admin_id'])) {
	  				redirect(base_url().'admin/login', 'refresh');
	                $this->session->set_flashdata('message', 'Please login to continue..!');
	            }
	        }
	        // $this->load->library('session');

	}

	

	public function register(){

		if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("register") !== null){

			echo $this->input->post("register");
			$user=array(
		      'name'=>$this->input->post('name'),
		      'email'=>$this->input->post('email'),
		      'password'=>md5($this->input->post('password')),
		    );
		    // print_r($user);

			$email_check=$this->AdminModel->emailcheck($user['email']);

			if($email_check){
			  $this->AdminModel->register($user);
			  $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
			  redirect('admin/login');

			}
			else{

			  $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
			  redirect('admin');
			}

		}

		else{
			$this->load->view("admin/auth/register");
		}
	}

	public function login(){

		if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("login") !== null){

			$password = md5($this->input->post('password'));
			$data=array(
		  		'email'=>$this->input->post('email'),
		  		'password'=> $password
	    	);

	    	$result = $this->AdminModel->login($data);
	    	if ($result != false) {
	    		$this->session->set_userdata('admin_id',$result['id']);
		        
		        redirect(base_url().'admin/dashboard');
	    	} else {
			  	$this->session->set_flashdata('error_msg', 'Please enter correct password!');
				$this->load->view('admin/auth/login');
	    	}  
		}
		else
		{
			$this->load->view("admin/auth/login");

		}
	  	
	}

	public function Dashboard()
	{
		if(isset($this->session->userdata["admin_id"])) {
			$this->load->view('admin/index');	
        }
        else
        {
	  		redirect(base_url().'admin/login', 'refresh');
        }

	}


	public function logout(){

	  $this->session->sess_destroy();
	  redirect(base_url().'admin/login', 'refresh');
	}

	public function CreatePermission()
	{
	  	$this->load->model('Admin/RBASModel');
	    
	    $data["result"] = $this->RBASModel->RoleData();
	    $data["category"] = $this->RBASModel->categorydata();
	    // var_dump($data['result']); die();

	    $this->load->view('admin/usertype/create', $data);
	}

	public function CreateModule(){
		if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post() !== null){
			$data=array(
		  		'name'=>$this->input->post('name'),
		  		'is_active'=>$this->input->post('is_active')
	    	);   		    	
	  		$this->load->model('Admin/ModuleTypeModel');
	    	$result = $this->ModuleTypeModel->InsertData($data);
			$this->session->set_flashdata('success', 'Add Module Type Successfully');
		    redirect(base_url().'admin/module-user/list');
		}
		else{
			$this->load->view('admin/moduletype/create');
		}
	}

	public function ListModule(){
	  	$this->load->model('Admin/ModuleTypeModel');
	    $data["result"] = $this->ModuleTypeModel->ModuleTypeData();
		$this->load->view('admin/moduletype/list',$data);
	}

	public function EditModule($id){
	  	$this->load->model('Admin/ModuleTypeModel');
	    $data["result"] = $this->ModuleTypeModel->EditModuleTypeData($id);
		$this->load->view('admin/moduletype/edit',$data);
	}

	public function UpdateModule($id){
	  	$this->load->model('Admin/ModuleTypeModel');
	    $data["result"] = $this->ModuleTypeModel->UpdateModuleTypeData($id);
		$this->session->set_flashdata('success', 'Update Module Type Successfully');
		redirect(base_url().'admin/module-user/list');
	}

	public function DeleteModule($id){
		$this->db->where('id', $id);
       	$this->db->delete('module_type');
		$this->session->set_flashdata('success', 'Delete Module Type Successfully');
       	redirect(base_url('admin/module-user/list'));
	}

	// Function of category

	public function CategoryList(){
		$this->load->model('Admin/CategoryModel');
	    $data["result"] = $this->CategoryModel->CategoryData();
		$this->load->view('admin/category/list',$data);
	}

	public function CreateCategory(){
		if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post() !== null){
			$data=array(
		  		'name'=>$this->input->post('name'),
		  		'is_active'=>$this->input->post('is_active')
	    	);   		    	
			$this->load->model('Admin/CategoryModel');
	    	$result = $this->CategoryModel->InsertData($data);
			$this->session->set_flashdata('success', 'Add Category Successfully');
		    redirect(base_url().'admin/category/list');
		}
		else{
			$this->load->model('Admin/CategoryModel');
	    	$data["moduledata"] = $this->CategoryModel->Moduledata();
			$this->load->view('admin/category/create',$data);
		}
	}

	public function EditCategory($id){
		$this->load->model('Admin/CategoryModel');
	    $data["result"] = $this->CategoryModel->EditCategoryData($id);
		$this->load->view('admin/category/edit',$data);
	}

	public function UpdateCategory($id){
		$this->load->model('Admin/CategoryModel');
	    $data["result"] = $this->CategoryModel->UpdateCategoryData($id);
		$this->session->set_flashdata('success', 'Update Category Successfully');
		redirect(base_url().'admin/category/list');
	}

	public function DeleteCategory($id){
		$this->db->where('id', $id);
       	$this->db->delete('perm_categories');
		$this->session->set_flashdata('success', 'Delete Category Successfully');
       	redirect(base_url('admin/category/list'));
	}

	//Function of Role

	public function CreateRole(){
		if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post() !== null){
			$data=array(
		  		'name'=>$this->input->post('name'),
		  		'is_active'=>$this->input->post('is_active')
	    	);   		    	
	  		$this->load->model('Admin/RoleModel');
	    	$result = $this->RoleModel->InsertData($data);
			$this->session->set_flashdata('success', 'Add Role Successfully');
		    redirect(base_url().'admin/role/list');
		}
		else{
			$this->load->view('admin/role/create');
		}
	}

	public function ListRole(){
	  	$this->load->model('Admin/RoleModel');
	    $data["result"] = $this->RoleModel->RoleData();
		$this->load->view('admin/role/list',$data);
	}

	public function EditRole($id){
	  	$this->load->model('Admin/RoleModel');
	    $data["result"] = $this->RoleModel->EditRoleData($id);
		$this->load->view('admin/role/edit',$data);
	}

	public function UpdateRole($id){
	  	$this->load->model('Admin/RoleModel');
	    $data["result"] = $this->RoleModel->UpdateRoleData($id);
		$this->session->set_flashdata('success', 'Update Role Successfully');
		redirect(base_url().'admin/role/list');
	}

	public function DeleteRole($id){
		$this->db->where('id', $id);
       	$this->db->delete('role_type');
		$this->session->set_flashdata('success', 'Delete Role Successfully');
       	redirect(base_url('admin/role/list'));
	}

}

?>