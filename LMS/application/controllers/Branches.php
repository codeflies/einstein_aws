<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branches extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('form','url');
        $this->load->model('BranchModel');
        $this->load->library('form_validation');  
		$this->load->helper("file");
	}

    public function Create()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("add_branch") !== null)
        {
            $this->BranchCreateUpdate($this->input->post("add_branch"));
        } else {
            $base_url =  base_url();
            $data["timedatadata"] = $this->BranchModel->TimeZoneData();
            $data["url"] = substr($base_url,8);
            $this->load->view('branches/create',$data);
        }
    }

    public function Update()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("update_branch") !== null && $this->input->post("branch_id") !== null)
        {
            $this->BranchCreateUpdate($this->input->post("update_branch"));
        } else {
            $this->load->view('branches/list');
        }
    }

    public function BranchCreateUpdate(string $action)
    {

        if($this->input->server('REQUEST_METHOD') == "POST" && $action != "")
        {
            $educator_id = $this->session->userdata["educator_id"];
            
            // Define of response array
            $response = [
                'success' => false,
                'message' => '',
                'error' => array(),
                'data' => array(),
                'target' =>  ''
            ];

            // upload directory
            $upload_directory = "uploads/users/branches/";
            // alllowed ext.
            $allowed_extension = array("jpg", "png", "jpeg");
            // file validation
            if($_FILES['image']['tmp_name'] != ""){ 
                $file_name_1 = $_FILES['image']["name"];
                $file_size_1 = $_FILES['image']["size"];
                $temp_name_1 = $_FILES['image']["tmp_name"];
                $file_name_1 = $_FILES['image']["name"];

                $file_extension_1 = pathinfo($file_name_1, PATHINFO_EXTENSION);

                if(!in_array($file_extension_1, $allowed_extension)) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['image'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['message'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                } else if($file_size_1 >  2000000 ) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['image'] = "Please choose any file less than 2MB.";
                    $response['message'] = "Please choose any file less than 2MB.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                }
            }    

            if($_FILES['favicon']['tmp_name'] != ""){ 
                $file_name_2 = $_FILES['favicon']["name"];
                $file_size_2 = $_FILES['favicon']["size"]; // size in bytes
                $temp_name_2 = $_FILES['favicon']["tmp_name"];
                $file_name_2 = $_FILES['favicon']["name"];

                $file_extension_2 = pathinfo($file_name_2, PATHINFO_EXTENSION);

                if(!in_array($file_extension_2, $allowed_extension)) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['favicon'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['message'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                } else if($file_size_2 >  2000000 ) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['favicon'] = "Please choose any file less than 2MB.";
                    $response['message'] = "Please choose any file less than 2MB.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                }
            }

            // skip password required validation on update
            if ($action === "AddUser") {
                $this->form_validation->set_rules(
                    'name','name ','trim|required|min_length[4]|max_length[21]'
                );
                $branch_id_update = "";
            } else {
                $this->form_validation->set_rules(
                    'name','name ','trim|required|min_length[4]|max_length[21]'
                );
                // user update id 
                $branch_id_update = $this->input->post("branch_id"); 
            }

            // Check branch name
            $branchname_exist = $this->BranchModel->CheckBranchName($this->input->post("name"), $educator_id, $branch_id_update);

            if($this->form_validation->run() == false)
            {
                $response['error'] = $this->form_validation->error_array();
                $response['message'] = "Please fill the form correctly...!";
                echo json_encode($response);
                exit;
            }
            elseif($branchname_exist === true){
                $err_arr = $this->form_validation->error_array(); 
                $err_arr['name'] = "Branch already exits!";
                $response['message'] = "Branch already exits!";
                $response['error'] = $err_arr;
                echo json_encode($response);
                exit;
            }
            else{
                // Branch data
                $BranchData = [
                    "name" => $this->input->post("name"),
                    "title" => $this->input->post("title"),
                    "description" => $this->input->post("description"),
                    "default_branch_theme" => $this->input->post("default_branch_theme"),
                    "language" => $this->input->post("language"),
                    "timezone" => $this->input->post("timezone"),
                    "internal" => $this->input->post("internal"),
                    "external" => $this->input->post("external"),
                    "default_user_type" => $this->input->post("default_user_type"),
                    "register_total_user" => $this->input->post("restrict_register_user"),
                    "e_commerce_processor" => $this->input->post("e_commerce"),
                    "currency" => $this->input->post("currency"),
                    "paypal_email_address" => $this->input->post("paypal_email_address"),
                    "gamification" => $this->input->post("gamification"),
                    "educator_id" => $educator_id
                ];

                // ADD Update condtion 
                if ($action === "add_branch") {
                    // response message
                    $return_message = "Branch Added Successfully";
                    // Insert into branch
                    $result = $this->BranchModel->InsertBranch($BranchData);   

                } else if ($action === "update_branch") {
                    // response message
                    $return_message = "Branch Updated Successfully";
                    // Update branch
                    $branch_id_update = $this->input->post("branch_id");
                    $result = $this->BranchModel->UpdateBranch($BranchData, $branch_id_update);
                }

                if ($result !== false) {
                    $BranchID  = $result; 
                    // image upload
                    if($_FILES['image']['tmp_name'] != ""){ 
                        $new_file_name = $BranchID."_image.png";
                        $upload_path = $upload_directory.$new_file_name;
                        $uploaded = move_uploaded_file($temp_name_1, $upload_path);
                        if ($uploaded) {
                            $data = [
                                "image" => $new_file_name
                            ];
                            $result = $this->BranchModel->UpdateBranch($data, $BranchID);
                            if($result) {
                                $response["success"] = true;
                                $response["message"] = $return_message;
                            } else {
                                $response["message"] = "Some error occured.";
                                echo json_encode($response);
                                exit;
                            }
                        }
                    }

                    //fevicon upload
                    if($_FILES['favicon']['tmp_name'] != ""){ 
                        $new_file_name = $BranchID."_favicon.png";
                        $upload_path = $upload_directory.$new_file_name;
                        $uploaded = move_uploaded_file($temp_name_2, $upload_path);
                        if ($uploaded) {
                            $data = [
                                "favicon" => $new_file_name
                            ];
                            $result = $this->BranchModel->UpdateBranch($data, $BranchID);
                            if($result) {
                                $response["success"] = true;
                                $response["message"] = $return_message;
                            } else {
                                $response["message"] = "Some error occured.";
                                echo json_encode($response);
                                exit;
                            }
                        }
                    }
                    // Response
                    $response["success"] = true;
                    $response["message"] = $return_message;
                    $response["target"] = base_url()."branches/list";
                    echo json_encode($response);  
                } else {
                    $response["message"] = "Some error occured.";
                    echo json_encode($response);
                    exit;
                }
            }
        }
    }

    public function List()
    {
        $educator_id = $this->session->userdata["educator_id"];
        $data['result'] = $this->BranchModel->BranchList();
        $data['category'] = $this->BranchModel->Category();
        $data['course_category'] = $this->BranchModel->CourseCategory($educator_id);
        $this->load->view('branches/list',$data);
    }

    public function Delete(){
        $delete_branch = $this->input->post("branch_id");
        $this->db->where('id', $delete_branch);
        $this->db->delete('branch');
        $err_arr = $this->form_validation->error_array(); 
        $response["success"] = true;
        $response['message'] = "Branch Delete Successfully!";
        $response['error'] = $err_arr;
        echo json_encode($response);
    }

    public function datalist(){
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];
        $list = $this->BranchModel->GetBranch($data, $educator_id);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->BranchModel->CountBranches($educator_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $list->num_rows;
        } else {
            $arr['rows'] = [];
        }
        
        echo json_encode($arr);
    }

    public function edit($id)
    {
        $rec['result'] = $this->BranchModel->Editdata($id);
        $base_url =  base_url();
        $rec["timedatadata"] = $this->BranchModel->TimeZoneData();
        $rec["url"] = substr($base_url,8);
        $this->load->view('branches/edit', $rec);
    }

    public function Clone(){
        $clone_id = $this->input->post("id");
        $name = $this->input->post("clone_name");
        $educator_id = $this->session->userdata["educator_id"];
        $clone_data = $this->BranchModel->CloneData($clone_id, $name, $educator_id);

        if($clone_data !== false){
        	$response["success"] = true;
            $response["message"] = "Branch clone Successfully!";
            echo json_encode($response); 
        }
        else{
        	$response["message"] = "Some error occured.";
            echo json_encode($response);
            exit;
        }
    }

    public function AddCoursesToAllBranch()
    {
        $educator_id = $this->session->userdata["educator_id"];
        $course_id = $this->input->post("course_id");
        $data = $this->BranchModel->AddCoursesToAllBranches($educator_id, $course_id);
    }

    public function DeleteCoursesToAllBranch()
    {
        $educator_id = $this->session->userdata["educator_id"];
        $course_id = $this->input->post("course_id");
        $data = $this->BranchModel->DeleteCoursesToAllBranchModel($educator_id, $course_id);
    }

    public function BranchUser($branch_id)
    {
        $check_branch_data['result'] = $this->BranchModel->GetBranchData($branch_id);
        if($check_branch_data['result']){
            $data['branch_id'] = $branch_id;
            $this->load->view('branches/user',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function branchuserlist()
    {
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch user
        $check_branch_user = $this->BranchModel->GetBranchUser($educator_id,$data);
        if ($check_branch_user->num_rows > 0) {
            while($row = $check_branch_user->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->BranchModel->CountBranchUser($educator_id);
            
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $check_branch_user->num_rows;
        } else {
            $arr['rows'] = [];
        }
        
        echo json_encode($arr);
    }

    // List of all based on select branch in courses
    public function BranchCourses($branch_id)
    {
        $check_branch_data['result'] = $this->BranchModel->GetBranchData($branch_id);
        if($check_branch_data['result']){
            $data['branch_id'] = $branch_id;
            $this->load->view('branches/course',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    // Ajax of all course list
    public function branchCourselist()
    {
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch Courses
        $check_branch_user = $this->BranchModel->GetBranchCouser($educator_id,$data);
        if ($check_branch_user->num_rows > 0) {
            while($row = $check_branch_user->fetch_assoc()) {
                $arr['rows'][] = $row;
            }
            $total_count = $this->BranchModel->CountBranchCourses($educator_id);
            
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $check_branch_user->num_rows;
        } else {
            $arr['rows'] = [];
        }
        
        echo json_encode($arr); 
    }

    // Files upload of branch
    public function BranchFiles($branch_id)
    {
        $check_branch_data['result'] = $this->BranchModel->GetBranchData($branch_id);
        if($check_branch_data['result']){
            $data['branch_id'] = $branch_id;
            $this->load->view('branches/files',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    // Upload image of multi type files

    public function UploadImage()
    {
        $educator_id = $this->session->userdata["educator_id"];
        $branch_id = $this->input->post("branch_id");

        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);

        if(!empty($_FILES["image"]["tmp_name"])){ 
            $file_size = $_FILES['image']["size"]/1024;

            // File upload configuration 
            $targetDir = "uploads/users/branches/files"; 
            $allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg', 'gif','mp4'); 
            $fileName = basename($_FILES['image']['name']); 
            $targetFilePath = $targetDir."/".$fileName; 

            // Type of file
            $file_type = strstr($fileName,".");


            // Check whether file type is valid 
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to the server 
                if(move_uploaded_file($_FILES['image']['tmp_name'], $targetFilePath) && ($branch_id!='')){ 
                    // Branch data
                    $ImageData = [
                        "educator_id" => $educator_id,
                        "branch_id" => $branch_id,
                        "name" => $fileName,
                        "type" => $file_type,
                        "size" => intval($file_size).' KB'
                    ];
                    $check_branch_data = $this->BranchModel->GetFilesUploaded($ImageData);
                    $response["success"] = true;
                    $response["message"] = "File has uploaded successfully!";
                } 
            } 
        } 
        else{
            $response["success"] = false;
            $response["message"] = "File uploaded unsuccessfully!";
        }
        echo json_encode($response); 
    }

    // Get data of based on branch files
    public function FilesList($branch_id = NULL)
    {
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        //Check branch Courses
        $file_data = $this->BranchModel->GetBranchFileData($educator_id,$branch_id,$data);
        if ($file_data->num_rows > 0) {
            // $fetch_data = $file_data->fetch_assoc();
            foreach($file_data as $row){
                $arr['rows'][] = $row;
            }
            $total_count = $this->BranchModel->CountBranchFileData($educator_id,$branch_id);
            $arr['total'] = $total_count->num_rows();
            $arr['totalNotFiltered'] = $file_data->num_rows;
        } else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);
    }

    public function RenameFile()
    {
        $file_id = $this->input->post("name_id");
        $File_name = $this->input->post("rename");
        $this->BranchModel->UodateFileName($file_id,$File_name);
    }

    public function DeleteFileBranch(){
        $file_id = $this->input->post("file_id");
        $this->BranchModel->DeleteFileBranch($file_id);
        
    }
}   