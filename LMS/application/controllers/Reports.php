<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		


    
	}

        public function Userinfo()
         {
            $this->load->view('reports/userinfo');
        }
        public function User()
         {
            $this->load->view('reports/user');
        }

        public function Branch()
         {
            $this->load->view('reports/branch');
        }

        public function Infographics(){
            $this->load->view('reports/infographics');
        }
        public function Course(){
            $this->load->view('reports/course');
        }

        public function Group(){
            $this->load->view('reports/group');
        }
        public function Reports(){
            $this->load->view('reports/reports');
        } public function Assignment(){
            $this->load->view('reports/assignment');
        }
        public function Ilt(){
            $this->load->view('reports/Ilt');
        }
        public function Custom(){
            $this->load->view('reports/custom_index');
        }
        public function Scorm(){
            $this->load->view('reports/scorm');
        }
         public function Survey(){
            $this->load->view('reports/survey');
        }
        public function Test(){
            $this->load->view('reports/test');
        }
   

    }

   