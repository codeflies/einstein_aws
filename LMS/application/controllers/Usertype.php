<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype extends CI_Controller {

	// educator details
	private $educator_id;

	public function __construct()
	{
		parent::__construct();

		// load essential helper and url etc.
		$this->load->model('UserTypeModel');
		$this->load->library('form_validation');

      // For authentication in every method except index and register
		$class = $this->router->fetch_class();
		$method = $this->router->fetch_method();
		if($class === 'user' && $method !== 'register' && $method !== 'index') {
		   if (!isset($this->session->userdata['user_id'])) {
		       redirect(base_url(),'refresh');
		       $this->session->set_flashdata('message', 'Please login !');
		   }
		   // welcome form condition
		   if ($this->session->userdata['welcome_info'] === "false") {
		      redirect(base_url(),'refresh');
		   }
		}
		// set educator id
		$educator_id = $this->session->userdata["educator_id"];
	}

	// User type create method
	public function Create()
	{
		if($this->input->server('REQUEST_METHOD') == "POST") {

			$this->CreateFormView();
		} else {
			$this->load->view('usertype/create');
		}	
	}

	private function CreateFormView()
	{
		// response object
        $response = [
			'success' => false,
			'message' => '',
			'error' => array(),
			'data' => array(),
			'target' =>  ''
        ];
		// Get All Module 
		$module_data = $this->UserTypeModel->GetModule();
		if ($module_data->num_rows > 0) {

			while ($module_row = $module_data->fetch_assoc()) {
				// Get Module ID
			    $module_id = $module_row["id"];
			    // Add extra array in module row 
				$module_row["category"] = [];
			    // Get all categories of module
				$permission_category = $this->UserTypeModel->GetPermissionCategory($module_id);

				if ($permission_category->num_rows() > 0) {

					$permission_category_row = $permission_category->result();
				
					foreach ($permission_category_row as $permission_data) {
						// Convert stdClass object into array
						$permission_data = json_decode(json_encode($permission_data), true);
						//Get category ID 
						$perm_cate_id = $permission_data["id"];
						// Get parent category ID
						$parent_category_id = $permission_data["parent_category_id"];
						// Store only parent category
						if ($parent_category_id == '0') {
							// Get action of category
							$category_action = $this->UserTypeModel->GetPermissionAction($perm_cate_id);
							if ($category_action->num_rows() > 0) {
								$category_action_row = $category_action->result();
								$permission_data["action"] = $category_action_row;		
							} else {
								$permission_data["action"] = [];
							}
							// Get sub category
							$sub_category = $this->UserTypeModel->GetSubCategory($perm_cate_id);
							if ($sub_category->num_rows() > 0) {
								$sub_category_row = $sub_category->result();	
								foreach ($sub_category_row as $sub_category_data) {
									// Convert std Class object into array
									$sub_category_data = json_decode(json_encode($sub_category_data), true);
									// Get Sub category ID
									$sub_category_id = $sub_category_data["id"];
									// Get Sub Category action
									$sub_category_action = $this->UserTypeModel->GetPermissionAction($sub_category_id);
									if ($sub_category_action->num_rows() > 0) {
										$sub_category_action_row = $sub_category_action->result();
										$sub_category_data["action"] = $sub_category_action_row;		
									}  else {
										$sub_category_data["action"] = [];
									}
									// Store sub category data 
									$permission_data["sub_category"][] = $sub_category_data;	
								}							
							} else {
								$permission_data["sub_category"] = [];
							}
							array_push($module_row["category"],$permission_data);
						}
					}
					//Return Response 
					// response object
			        $response['success'] = true;
			        $response['data'][] = $module_row;
				} 
			}
			echo json_encode($response); die();
		} else {
			$response['message'] = "Some error occured.(Unable to fetch data)";
            echo json_encode($response);
		}
	}

	// User type list
	public function List(){
		$this->load->view('usertype/list');
	}
}

/* End of file UserType.php */
