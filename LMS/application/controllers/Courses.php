<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
class Courses extends CI_Controller {
    
            public function __construct()
            {
            parent::__construct();
            $this->load->helper('form','url');
            $this->load->model('CourseModel');
            $this->load->library('form_validation'); 
            $this->load->helper('date');  
        
            }
    public function Course_list() 
    {
        // $data['result'] = $this->CourseModel->CourseList();
       $this->load->view('courses/course_list');
       
    }

// // Create Group 
    public function Create()
    {
        $educator_id = $this->session->userdata["educator_id"];
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("add_course") !== null)
        {
            $this->CourseCreateUpdate($this->input->post("add_course"));
        } else {
               $data['cat_data'] = $this->CourseModel->Getcategory($educator_id);
              $this->load->view('courses/create', $data);
        }
    }
    // Update Function
    public function Update()
    {
        if($this->input->server('REQUEST_METHOD') == "POST" && $this->input->post("update_course") !== null && $this->input->post("user_id") !== null)
        {
            $this->CourseCreateUpdate($this->input->post("update_course"));
        } else {
            $data['cat_data'] = $this->CourseModel->Getcategory($educator_id);
            $this->load->view('courses/course_list');
        }
    }
// Common method for course add and update
  
    private function CourseCreateUpdate(string $action)
    {   
        if($this->input->server('REQUEST_METHOD') == "POST" && $action != "")
        {
             // response object
            $response = [
                'success' => false,
                'message' => '',
                'error' => array(),
                'data' => array(),
                'target' =>  ''
            ];
            
            // upload directory
            $upload_directory = "uploads/users/course/";
            // alllowed ext.
            $allowed_extension = array("jpg", "png", "jpeg");
            // file validation
            if($_FILES['image']['tmp_name'] != ""){ 
                $file_name_1 = $_FILES['image']["name"];
                $file_size_1 = $_FILES['image']["size"];
                $temp_name_1 = $_FILES['image']["tmp_name"];
                $file_name_1 = $_FILES['image']["name"];

                $file_extension_1 = pathinfo($file_name_1, PATHINFO_EXTENSION);

                if(!in_array($file_extension_1, $allowed_extension)) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['image'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['message'] = "Please choose jpg, png, or jpeg file extension.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                } else if($file_size_1 >  2000000 ) {
                    $err_arr = $this->form_validation->error_array(); 
                    $err_arr['image'] = "Please choose any file less than 2MB.";
                    $response['message'] = "Please choose any file less than 2MB.";
                    $response['error'] = $err_arr;
                    echo json_encode($response);
                    exit;
                }
            }

            //video upload start
            $this->load->helper('url');
            $this->load->library('session');  
            $map=$_FILES['Use_Video']['name'];
            $filename=$this->input->post('Use_Video');

            print_r($filename);die;
            if(is_uploaded_file($_FILES['Use_Video']['tmp_name'])) 
            {
                $filename = $_FILES['Use_Video']['name'];
                //$config['upload_path'] = './assets/video/';
                $config['upload_path'] = 'uploads/users/course/video';
                $config['allowed_types'] = 'mp4|3gp|gif|jpg|png|jpeg|pdf';
                $config['max_size']='';
                $config['max_width']='200000000';
                $config['max_height']='1000000000000';
    
                // $config['image_library']='gd2';
                $this->load->library('upload', $config);
                $img = $this->upload->do_upload('Use_Video');

                print_r($img);die;
            }
            //vedio upload end


            // form validation
            $this->form_validation->set_rules(
                'course_name','course_name','trim|required'
            );

            // $this->form_validation->set_rules(
            //     'category','category'
            // );
            // $this->form_validation->set_rules(
            //     'price','price','trim'
            // );
            // $this->form_validation->set_rules(
            //     'description','description','trim'
            // );
            // $this->form_validation->set_rules(
            //     'course_code','course_code','trim'
            // );
            // $this->form_validation->set_rules(
            //     'Capacity','Capacity'
            // );
            //  $this->form_validation->set_rules(
            //     'You_Tube','You_Tube','trim|required'
            // );
            //   $this->form_validation->set_rules(
            //     'Use_Video','Use_Video','trim|required|min_length[4]|max_length[21]'
            // );
            // $this->form_validation->set_rules(
            //     'expiration_date'
            // );
            //      $this->form_validation->set_rules(
            //     'course_expiration'
            // );
              
            //    $this->form_validation->set_rules(
            //     'certification'
            // );
            //    $this->form_validation->set_rules(
            //     'level','level'
            // );
            //    $this->form_validation->set_rules(
            //     'Use_Video','Use_Video'
            // );
               
           // Setting update category id according to action
            if ($action === "AddCourse") {
                $Course_id_update = "";
            } else {
                // category update id 
                $Course_id_update = $this->input->post("user_id"); 
            }

            // EDUCATOR DETAILS
            $educator_id = $this->session->userdata["educator_id"];

            if($this->form_validation->run() == false)
            {
                $response['error'] = $this->form_validation->error_array();
                $response['message'] = 'Please fill the form correctly...!';
                echo json_encode($response);
                exit;
            } else {
                // Course data
                $CourseData = [
                    "course_name" => $this->input->post("course_name"),
                    "category" => $this->input->post("category"),
                    "description" => $this->input->post("description"),
                    "course_code" => $this->input->post("course_code"),
                    "price" => $this->input->post("price"),
                    "Capacity" => $this->input->post("Capacity"),
                    "course_expiration" => $this->input->post("course_expiration"),
                    "expiration_date" => $this->input->post("expiration_date"),
                    //"certification" => $this->input->post("certification"),
                    "level" => $this->input->post("level"),
                    // "Use_Video" => $this->input->post("Use_Video"),
                     "educator_id" => $educator_id
                ];
                 // ADD Update condtion 
                if ($action === "AddCourse") {
                    // response message
                    $return_message = "Course Added Successfully";

                    // Insert into Users
                    $result = $this->CourseModel->Insert($CourseData);
                      
                } else if ($action === "UpdateCourse") {
                    // response message
                    $return_message = "Course Updated Successfully";
                    // Update User
                    $result = $this->CourseModel->UpdateCourse($CourseData, $Course_id_update);
                }

                if($result === false) { 
                    $response["message"] = "Some error occured.";
                    echo json_encode($response);
                    exit;
                } else {
                    $course_id = $result;

                    if($_FILES['image']['tmp_name'] != ""){ 
                        
                        $new_file_name = $course_id."_course.png";

                        $upload_path = $upload_directory.$new_file_name;
                        
                        $uploaded = move_uploaded_file($temp_name_1, $upload_path);
                        
                        if (!$uploaded) {
                            // Returning Response
                            $response["success"] = true;
                            $response["message"] = $return_message." (But image upload failed...!)";
                            $response["target"] = base_url()."courses/course_list";
                            echo json_encode($response);
                            exit;
                        } else {
                            $data = [
                                "image" => $new_file_name
                            ];
                            
                            $result = $this->CourseModel->UpdateCourse($data, $course_id);
                            if($result) {
                                // Returning Response
                            // $response["success"] = true;
                            // $response["message"] = $return_message;
                            // $response["target"] = base_url()."courses/course_list";
                            // echo json_encode($response);
                               
                            } else {
                                $response["message"] = "Some error occured.";
                                echo json_encode($response);
                                exit;
                            }
                        }
                    }

                    // Returning Response
                    $response["success"] = true;
                    $response["message"] = $return_message;
                    $response["target"] = base_url()."courses/course_list";
                    echo json_encode($response);
                }
            }
        } else {
            $this->load->view('courses/course_list');
        }
    }


// Delete Function
         public function Delete(){
        $delete_course = $this->input->post("course_id");
        $this->db->where('id', $delete_course);
        $this->db->delete('course');
        $err_arr = $this->form_validation->error_array(); 
        $response["success"] = true;
        $response['message'] = "Course Delete Successfully!";
        $response['error'] = $err_arr;
        echo json_encode($response);
    }

    // Edit function
    public function edit($id){
            // var_dump($id); die();
            // EDUCATOR DETAILS
            $educator_id = $this->session->userdata["educator_id"];
            $result = $this->CourseModel->GetCourseById($id, $educator_id);
            $data['cat_data'] = $this->CourseModel->Getcategory($educator_id);
            $data['CourseData'] = $result->result();
            $data['course_id'] = $id;            
            $this->load->view('courses/edit', $data);
    }


    // List 
    public function datalist(){
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];
        $list = $this->CourseModel->GetCourse($data, $educator_id);
        
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }

            $total_count = $this->CourseModel->CountCourse($educator_id);
            $arr['total'] = $total_count->num_rows;
            $arr['totalNotFiltered'] = $list->num_rows;
        } else {
            $arr['rows'] = [];
        }
        
        echo json_encode($arr);
    }


    public function Clone(){
        $clone_id = $this->input->post("id");
        $course_name = $this->input->post("clone_name");
        $educator_id = $this->session->userdata["educator_id"];
        $clone_data = $this->CourseModel->CloneData($clone_id, $course_name, $educator_id);
        // var_dump(CloneData); die();

        if($clone_data !== false){
            $response["success"] = true;
            $response["message"] = "Course clone Successfully!";
            echo json_encode($response); 
        }
        else{
            $response["message"] = "Some error occured.";
            echo json_encode($response);
            exit;
        }
    }
    //  // Video Uploade Methode
    //     public function upload()
    //     {
    //     if ( ! empty($_FILES))
    //     {
    //         $config['upload_path'] = "uploads/users/course/video";
    //         $config['allowed_types'] = 'gif|jpg|png|mp4|ogv|zip';

    //         $this->load->library('upload');

    //         $files           = $_FILES;
    //         $number_of_files = count($_FILES['file']['name']);
    //         $errors = 0;

    //         // codeigniter upload just support one file
    //         // to upload. so we need a litte trick
    //         for ($i = 0; $i < $number_of_files; $i++)
    //         {
    //             $_FILES['file']['name'] = $files['file']['name'][$i];
    //             $_FILES['file']['type'] = $files['file']['type'][$i];
    //             $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
    //             $_FILES['file']['error'] = $files['file']['error'][$i];
    //             $_FILES['file']['size'] = $files['file']['size'][$i];

    //             // we have to initialize before upload
    //             $this->upload->initialize($config);

    //             if (! $this->upload->do_upload("file")) {
    //                 $errors++;
    //             }
    //         }

    //         if ($errors > 0) {
    //             echo $errors . "File(s) cannot be uploaded";
    //         }

    //     }
    //     elseif ($this->input->post('file_to_remove')) 
    //     {
    //         $file_to_remove = $this->input->post('file_to_remove');
    //         unlink("uploads/users/course/video" . $file_to_remove);  
    //     }
    //     else {
    //         $this->listFiles();
    //     }
    // }

    // private function listFiles()
    // {
    //     $this->load->helper('file');
    //     $files = get_filenames("uploads/users/course/video");
    //     echo json_encode($files);
    // }
    // // End Method

    // List of CourseUser function
    public function CourseUser($course_id){
        $check_course_data['result'] = $this->CourseModel->GetCourseData($course_id);
        if($check_course_data['result']){
            $data['course_id'] = $course_id;
            $this->load->view('courses/user',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function Branches($course_id)
    {
        $check_course_data['result'] = $this->CourseModel->GetCourseData($course_id);
        if($check_course_data['result']){
            $data['course_id'] = $course_id;
            $this->load->view('courses/branches',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    // list of branch select base on course
    public function branchdatalist($course_id)
    {
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        $list = $this->CourseModel->GetCourseBranchData($educator_id,$data);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }

            $total_count = $this->CourseModel->CountCourseBranch($educator_id);
            $arr['total'] = $total_count;
            $arr['totalNotFiltered'] = $list->num_rows;
        } else {
            $arr['rows'] = [];
        }
        echo json_encode($arr);    
    }

    public function Group($course_id)
    {
        $check_group_data['result'] = $this->CourseModel->GetCourseData($course_id);
        if($check_group_data['result']){
            $data['course'] = [
                "course_id"=>$course_id,
                "group_member"=>json_decode($check_group_data['result']->group_member)
            ];
            $this->load->view('courses/group',$data);
        }
        else
        {
            echo "Page Not Found";
            die();
        }
    }

    public function Groupslist(){
        $educator_id = $this->session->userdata["educator_id"];
        // Get value
        $data = [
            'sort' => $this->input->get("sort") ?? "",
            'search' => $this->input->get("search") ?? "",
            'order_by' => $this->input->get("order") ?? "",
            'offset' => $this->input->get("offset") ?? "",
            'limit' => $this->input->get("limit") ?? ""
        ];
        $arr['rows'] = [];

        $list = $this->CourseModel->GetCourseGroupData($educator_id,$data);
        if ($list->num_rows > 0) {
            while($row = $list->fetch_assoc()) {
                $arr['rows'][] = $row;
            }

            $total_count = $this->CourseModel->CountCourseGroup($educator_id);
            $arr['total'] = $total_count;
            $arr['totalNotFiltered'] = $list->num_rows;
        } else {
            $arr['rows'] = [];
        }
        echo json_encode($arr); 
    }

    public function AddGroupMember($course_id){
        $educator_id = $this->session->userdata["educator_id"];
        $group_id = $this->input->post('group_id'); 
        $result_id = $this->CourseModel->AddGroupMember($educator_id,$group_id,$course_id);
        if($result_id==true){
            $response["success"] = true;
            $response['message'] = "Add member Successfully!";
            $response["target"] = base_url()."Course/group/".$course_id;
        }
        else{
            $response["success"] = false;
            $response['message'] = "Add member not Successfully!";
        }
        echo json_encode($response);
    }

    public function MinusGroupMember($course_id){
        $educator_id = $this->session->userdata["educator_id"];
        $group_id = $this->input->post('group_id');
        $result_id = $this->CourseModel->MinusGroupMember($educator_id,$group_id,$course_id);
        if($result_id==true){
            $response["success"] = true;
            $response['message'] = "Remove group Successfully!";
            $response["target"] = base_url()."Course/group/".$course_id;
        }
        else{
            $response["success"] = false;
            $response['message'] = "Remove group not Unsuccessfully!";
        }
        echo json_encode($response);
    }
} 