<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Importexport extends CI_Controller {

        public function __construct()
    {
        parent::__construct();
       
        
        $this->load->helper(array('form','url'));
         $this->load->library('form_validation');

    }

        public function Import(){
           
            $this->load->view('ImportExport/import');
        }
        public function Export(){
            $this->load->view('ImportExport/export');
        }
        public function Ftp(){
            $this->load->view('ImportExport/ftp');
        }

    }



